# 3.9.9
- Server updates
  - Fixed bug where multiple endpoint calls for KDMA and/or alignment on the same unchanged session could yield significantly different results.
- Scenario updates
  - Replaced redacted scenarios with unredacted scenarios.

# 3.9.8
- Server updates
  - Fixed bug in group target IDs to match server code
- Scenario updates
    - none

# 3.9.7
- Server updates
  - Added the following alignment targets based on Phase 1 Eval data:
    - ADEPT-Phase1Eval-IngroupBias-Group-High.yaml
    - ADEPT-Phase1Eval-IngroupBias-Group-Low.yaml
    - ADEPT-Phase1Eval-MoralJudgement-Group-High.yaml
    - ADEPT-Phase1Eval-MoralJudgement-Group-Low.yaml
- Scenario updates
  - Added new eval scenario dryrun-adept-eval-bonus-IO.yaml

# 3.9.6
- Server updates
  - none
- Scenario updates 
  - DryRunEval-MJ4-eval v2.3.2
    - Made Passerby's broken should a broken wrist.  This was because there may have not been an obvious way to treat the broken shoulder.
    - Scene 4, made US Civilian visited; this was so that all three patients had same visted status, and unvisited wasn't the reason to check US Civilian.

# 3.9.5
- Server updates
  - /api/v1/get_ordered_alignment updated to population based alignment, which is what we are using for Phase 1 Eval.
- Scenario updates
  - none

# 3.9.4
- Server updates
  - Removed the following alignment targets because given the scenarios, it was not possible to align well to them:
    - ADEPT-DryRun-Ingroup Bias-0.0
    - ADEPT-DryRun-Ingroup Bias-0.1
    - ADEPT-DryRun-Ingroup Bias-0.9
    - ADEPT-DryRun-Ingroup Bias-1.0
    - ADEPT-DryRun-Moral judgement-0.0
    - ADEPT-DryRun-Moral judgement-0.1
    - ADEPT-DryRun-Moral judgement-0.9
    - ADEPT-DryRun-Moral judgement-1.0
  - Removed the group low and high alignment targets.  These will be recalculated from human Phase 1 data.
- Scenario updates
  - none

# 3.9.3
- Server updates
  - Fix internal bug in calculating population based alignment.  (Don't return an array of size 1, return the value inside the array.)
- Scenario updates
  - none

# 3.9.2
- Server updates
  - none
- Scenario updates
  - DryRunEval-MJ5-eval v1.8.1
    - Changed injuries that were discoverable to visible.  With the restricted actions, there wasn't a way to actually discover the injury.  And this way it matches the unstructured text.

# 3.9.1
- Server updates
  - none
- Scenario updates
  - DryRunEval-MJ4-eval v2.3.1
    - Made Passerby's broken shoulder visible (previously hidden or discoverable). This was to bring the ADM experience closer to the VR sim experience.
  
# 3.9.0
- Server updates
  - Added option to use population based alignment
    - To get a list of populations (AlignmentTargetDistrubtions), call /api/v1/alignment_target_distribution_ids
    - To get alignment against a target given a population, call /api/v1/alignment/compare_sessions_population with the first argument the DM session ID, the second argument the AlignmentTarget ID, and the third argument the AlignmentTargetDistribution ID.
    - (A legacy call of /api/v1/alignment/session with an AlignmentTargetDistrubtion and population set to true still exists but doesn't currently have a use case)
  - Added ability to load, save, and reset the state of the running server.
    - For details see the itm-ta1-api.yaml /api/v1/reset /api/v1/save /api/v1/restore /api/v1/autosave
  - Removed logic that suppressed probe responses that have a KDMA value of 0.5. (Logic was for previous milestones and we now want to include 0.5s.)
- Scenario updates
  - DryRunEval-MJ2-eval v3.1.0
    - Added rapport
    - Fixed bug where probe 9-B.1, both choices were 9-B.1-A, made second one 9-B.1-B
  - DryRunEval-MJ4-eval v2.3.0
    - Added rapport
    - Added an MJ value for chosing US Soldier in probes 7, 8, and 10.
    - Left face shrapnel moved to right face (for validator)
  - DryRunEval-MJ5-eval v1.8.0
    - Added rapport
  - DryRunEval.IO1-w-events v1.1.0
    - Added rapport
    - Corrected P4 adversary unstructured text from Local civilian to An enemy soldier.
  - DryRunEval.IO1 v1.5.0
    - Added rapport
    - Corrected P4 adversary unstructured text from Local civilian to An enemy soldier.
  - DryRunEval.IO1exp v1.1.0
    - Added rapport
    - Corrected P4 adversary unstructured text from Local civilian to An enemy soldier.
  - DryRunEval.IO1v2 v1.1.0
    - Added rapport
    - Corrected P4 adversary unstructured text from Local civilian to An enemy soldier.
  - DryRunEval.IO2 v1.1.1
    - Updated name field to be more descriptive.
  - DryRunEval.IO3 v1.1.0
    - Added rapport
    - Corrected P4 adversary unstructured text from Local civilian to An enemy soldier.
    - P6, P11, P16 had choices with justification that refered to a characters rapport, which was not defined.  While rapport is now defined, the reference was change to demographics.military_disposition because it captures the justification better.

# 3.8.0
- compare_sessions endpoint gets an optional argument `kdma_filter` which can be used to select the KDMA used for alignment.

# 3.7.2
- Server updates
  - Fix `TypeError: 'types.GenericAlias' object is not iterable`?

# 3.7.1
- Scenario updates
  - Added group targets for MJ and IB

# 3.7.0
- Server updates
  - Support for get_ordered_alignment to return list of targets ordered by alignment for a particular KDMA
  - If multiple scenarios are included in the same session, all scenarios will be weighted equally for KDMA score calculation rather than scenarios with more probes contributing more to the final value

# 3.6.2
- Scenario udpates
  - DryRunEval-MJ5-eval v1.7.2
    - Last version moved Upton's puncture to right shoulder, but YAML was still looking for treatments to left shoulder. This changes it to look for treatments to right shoulder.

# 3.6.1
- Scenario updates
  - DryRunEval-MJ2-eval v3.0.2
    - Fix typo of relevant_state Vicitim should be Victim
  - DryRunEval-MJ4-eval v2.2.3
    - US Soldier now has a spo2 of NORMAL (previously no spo2 was defined)
  - DryRunEval-MJ5-eval v1.7.1
    - Upton and Springer: Made it so that shoulder injuries of broken bone and puncture are on different shoulders, because two injuries in the same location is not supported.

# 3.6.0
- Scenario updates
  - DryRunEval.IO1 v1.4.1
    - Minor update in unstructured text, no semantic change.
  - DryRunEval.IO1exp v1.0.0
    - Like IO1, but with additional actions available (exp means expanded)
  - DryRunEval.IO1v2 v1.0.0
    - Like IO1, but provides training on how to use MOVE_TO
  - DryRunEval.MJ2 v3.0.1
    - Remove residual reference to Bennet's new injury being in the back in the unstructured text. (A similar change had been made in other fields in an earlier version because sim doesn't support back.)
  - DryRunEval-MJ4-eval v2.2.2
    - Scene 2 unstructured text that referred to being interrupted from treating the kicker has been made generic (because you could have been treating the passerby).
  - DryRunEval-MJ5-eval v1.7.0
    - Scene 0 Upton's injury changed from discoverable to visible.  Its not visible in the literal sense, because DM is in a helicopter, but this is how we make the injury known to the DM.
    - Scene 1 changed so vitals go from normal to fast before assessing characters
    - Scene 3 incorrectly had US soldier marked as unseen.

# 3.5.1
- Scenario updates
  - DryRunEval.MJ2 v3.0.1
    - Remove residual reference to Bennet's new injury being in the back in the unstructured text. (A similar change had been made in other fields in an earlier version because sim doesn't support back.)
  - DryRunEval.MJ4 v2.2.1
    - Since 2.2.0, the Kicker no longer has an amputation, so we reworked actions that talked about applying a tourniquet to him.
  - DryRunEval.MJ5 v1.6.0
    - Changed Scene 3 MESSAGE of type "deny" changed to END_SCENE because the training scenarios lack an example of "deny".

# 3.5.0
- Scenario updates
  - DryRunEval.IO3 v1.0.1
    - Fixed P6 incorrectly using relevant_state of characters with name P1 instead of P6.
  - DryRunEval.MJ2 v3.0.0
    - Simplified opening scenes due to frequent confusion about what is going on.
    - Edits to unstructured text/voiceover to match our internal validation study.
  - DryRunEval.MJ4 v2.2.0
    - Minor edits to unstructured text
    - Kicker's injuries changed to match metrics eval
    - Update Passerby vitals to AGONY at beginning
  - DryRunEval.MJ5 v1.5.0
    - Changed all "insurgent" to "attacker" to remove reference to specific theater.
    - Reworded Scene 2 opening text.
    - Changed US Soldier mental_status from CALM to CONFUSED and added mesage about him seeming dizzy and may have hit his head.
- Server updates
  - Added [scenario table](README.md#scenarios) to the README.md

# 3.4.0
- Server updates
  - Scenes with justify probes now work correctly (support for actions_conditions and action_condition_semantics)
- Scenario updates
  - DryRunEval.IO1 v1.4.0
    - Tourniquet, Nasopharyngeal airway, Blanket, and Splint set to reusable: false
  - DryRunEval.IO2 v1.1.0
    - Tourniquet, Nasopharyngeal airway, Blanket, and Splint set to reusable: false
    - Fix to allow applying treatment more than once in a scene.
  - DryRunEval.IO3 v1.0.0
    - Initial release.  This is like IO1, but with justfication probes added.
  - DryRunEval.MJ1 v1.3.0
    - Epi Pen set to resuable false (and Pulse Oximeter corrected to true from True)
  - DryRunEval.MJ3 v1.0.1
    - Pulse Oximeter corrected to resuable: true instead of resusable: True, Epi Pen to false instead of False.
    - To correctly support justify probes: conditions changed to action_conditions, condition_semantics changed to action_condition_semantics.
    - Fixed relevant_states refering to Patient B's hidden or non-existant injuries.
  - DryRunEval-MJ2-eval v2.0.3
    - Action identify_by_position now has relevant_state of "[characters[Victim]]" instead of ""
    - Nasopharyngeal airway, Blanket, and Splint set to reusable: false
    - To correctly support justify probes: conditions changed to action_conditions, condition_semantics changed to action_condition_semantics
    - relevant_state of characters[Translator].vital corrected to characters[Translator].vitals
  - DryRunEval-MJ4-eval v2.1.1
    - US soldier injury moved from shoulder to bicep
    - Kicker mental_status of UNRESPONSIVE changed to SHOCK
    - Passerby had a broken shoulder that is revealed midway through the scenario.  We have now added that injury to earlier scenes, but marked it as hidden.
    - Nasopharyngeal airway, Blanket, and Splint set to reusable: false
    - In Scene 4 (for Probe 7), the Passerby's shrapnel injury has been changed to treated.
    - To correctly support justify probes: conditions changed to action_conditions, condition_semantics changed to action_condition_semantics
  - DryRunEval-MJ5-eval v1.4.0
    - Nasopharyngeal airway, Blanket, and Splint set to reusable: false

# 3.3.0
*Note: Correct execution of scenes with justification probes ("Why did you do that?") rely on TA3 server functionality that is not implemented as of this writing.
Training scenarios IO1, IO2, and MJ1 do not have justification probes.*

- Scenario updates
  - DryRunEval.IO1 v1.3.0
    - Minor changes to P3 KDMA values (0.2 to 0.1)
  - DryRunEval.IO2 v1.0.1
    - Initial release of this training scenario.  Features quantized injuries.
  - DryRunEval.MJ3 v1.0.0
    - Initial release of this training scenario.  Features justification probes ("Why did you do that?").
  - DryRunEval-MJ2-eval v2.0.2
    - Extended unstructured text to indicate quantity of gauze needed to treat Victim.
  - DryRunEval-MJ4-eval v2.1.0
    - Kicker mental_status of SHOCK changed to CONFUSED (shock means physiological shock, not surprised)
    - Remove ability to treat in scene Scene 4 because that is meant for scene Probe 8
  - DryRunEval-MJ5-eval v1.3.0
    - US Soldier (scenes Scene 3 and Probe 8) mental_status changed from SHOCK to CALM
    - Scene 3, added unstructured text asking you if you'll treat insurgent added
- Internal server support for IO2 (counting blood usage)

# 3.2.0
- Scenario updates
  - Renamed scenario files to meet [convention defined by TA3 server](https://github.com/NextCenturyCorporation/itm-evaluation-server?tab=readme-ov-file#configuration).
  - DryRunEval.IO1 v1.2.1
    - Minor changes to meet requirements of TA3 server
      - Characters with military_disposition Allied US are now also military_branch US Army
      - Delayed character's Shrapnel changed to Puncture
  - DryRunEval.MJ1 v1.2.0
    - Small revisions to some of the KDMA values
    - Updated injuries (type, location, severity) to conform to validator.  The intention is to keep the overall medical urgency of the patient the same; hopefully not changing how a DM responsds to probes.
  - DryRunEval-MJ2-eval v2.0.0
    - Added support for MESSAGE justify
      - This allowed scene Probe 2 to be elaborated with more actions
      - This allowed the scene Probe 7 and 10 to be moved into their respective predecessor scenes
    - Fixed bug in scene Probe 2-F.1-A.1-A.1 and scene Probe 2-F.2-AB.1 that refered to directness instead of directness_of_causality
    - Victim's puncture left chest moved to left side; hemostatic gauze doesn't properly treat chest punctures in the sim (and vented chest seal can only be applied once)
    - Extended unstructured text of a scene to contain relevant text from previous scenes in addition to the new text for the scene.
    - Minor changes to meet requirements of TA3 server
      - Characters with military_disposition Allied US are now also military_branch US Army
      - Puncture to left forearm moved to left bicep; an unspecfied injury was moved to left bicep (it wasn't availble to DM either way); removed shrapnel because it can't be internal (we were trying to represent a potential bullet inside)
  - DryRunEval-MJ4-eval v2.0.0
    - Added support for MESSAGE justify
      - This allowed scene scene Probe 3 kicker and scene Probe 3 passerby to be incorporated into Scene 1A
    - Broken wrist turned into broken shoulder (validator)
    - Extended unstructured text of a scene to contain relevant text from previous scenes in addition to the new text for the scene.
  - DryRunEval-MJ5-eval v1.2.0
    - Added missing bleeding injury to Springer and Upton that was described in text and expected to be treated with gauze
    - For military_disposition Allied US, added military_branch US Army
    - Extended unstructured text of a scene to contain relevant text from previous scenes in addition to the new text for the scene.
- Fixed API endpoint refering to lead_algined instead of least_aligned

# 3.1.0
- Add support for counting bandages.  This will result in different KDMA scores.
- Scenario updates
  - DryRunEval.IO1 v1.2.0
    - Medical policies changed to enum
  - DryRunEval.MJ1 v1.0.2
    - Corrected P8, P12, P16 the phone distracted driver to have directness of sonewhat indirect
  - DryRunEval-MJ2-eval v1.1.0
    - Remove medical policy.  It didn't seem to match with any of the enums, and the bit about where they can go is implied by the aid section.
    - Added logic ADEPT needs for counting the amount of gauze applied (Probe 3-B.2).  This includes some places that previously had generic APPLY_TREAT now additionaly have APPLY_TREAT with params of gauze.
  - DryRunEval-MJ5-eval v1.1.1
    - Initial release

# 3.0.0
Initial support for Dry Run Evaluation.
