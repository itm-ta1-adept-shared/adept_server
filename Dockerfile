# Base image with common dependencies
FROM python:3.12-slim as base
WORKDIR /usr/src/app
COPY requirements.txt /usr/src/app/
RUN pip3 install --no-cache-dir -r requirements.txt

# Testing stage
FROM base as tester
COPY test-requirements.txt /usr/src/app/
RUN pip3 install --no-cache-dir -r test-requirements.txt
COPY . /usr/src/app
CMD ["pytest", "-v", "--junitxml=reports/junit/test-results.xml"]

# Production stage
FROM base as production
COPY ./openapi_server /usr/src/app/openapi_server
EXPOSE 8080
ENTRYPOINT ["python3"]
CMD ["-m", "openapi_server"]
