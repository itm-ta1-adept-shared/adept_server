# ADEPT API

This project holds ADEPT's implementation of the In the Moment Metrics Eval TA1 API for characterization and alignment.

[[_TOC_]]

## About

This is a Python Flask server that implements the characterization and alignment interface, based on [Soartech's proposed MVP1 API specification](https://github.com/ITM-Soartech/itm-api/blob/master/itm-api.yaml) ([SHA-1 de45fad78d113](https://github.com/ITM-Soartech/itm-api/commit/de45fad78d113ae85d227e6ed683f834ea18bbf0)), with some modifications to support new concepts for the Dry Run Eval and Phase 1 Eval.  The application source lives in "openapi_server/", and the rest is supporting files.

### Scenarios

There are two types of [scenarios](openapi_server/data/scenario/dryrun).

The first type (dryrun-adept-train-IO1.yaml, dryrun-adept-train-MJ1.yaml, etc.) are scenarios that ADEPT developed for training purposes.

The second type of scenario is the ADEPT evaluation scenarios (dryrun-adept-MJ2.yaml, dryrun-adept-MJ4.yaml, dryrun-adept-MJ5.yaml). Depending on the version of the server you have, these evaluation scenarios may have had their KDMA values removed.

Further description of scenarios is available [on the ITM confluence](https://nextcentury.atlassian.net/wiki/spaces/ITMC/pages/3206873089/Dry+Run+and+Phase+1+Evaluation#Evaluation-Scenarios).

|       Scenario ID       |                          Name                              |                File                  | Environment |
| ----------------------- | ---------------------------------------------------------- | ------------------------------------ | ----------- |
| _evaluation_                                                                                                                              |
| DryRunEval-MJ2-eval     | Shooter/Victim                                             | dryrun-adept-eval-MJ2.yaml           | urban       |
| DryRunEval-MJ4-eval     | IED                                                        | dryrun-adept-eval-MJ4.yaml           | jungle      |
| DryRunEval-MJ5-eval     | Fistfight                                                  | dryrun-adept-eval-MJ5.yaml           | desert      |
| _training_                                                                                                                                |     
| DryRunEval.IO1          | Multi-Patient Tradeoffs ingroup/lives                      | dryrun-adept-train-IO1.yaml          | desert      |
| DryRunEval.IO1exp       | Multi-Patient Tradeoffs ingroup/lives                      | dryrun-adept-train-IO1exp.yaml       | desert      |
| DryRunEval.IO1v2        | Multi-Patient Tradeoffs ingroup/lives                      | dryrun-adept-train-IO1v2 .yaml       | desert      |
| DryRunEval.IO1-w-events | Multi-Patient Tradeoffs ingroup/lives                      | dryrun-adept-train-IO1-w-events.yaml | desert      |
| DryRunEval.IO2          | Training focused on quantized treatment.                   | dryrun-adept-train-IO2.yaml          | desert      |
| DryRunEval.IO3          | Multi-Patient Tradeoffs ingroup/lives, with justifications | dryrun-adept-train-IO3.yaml          | desert      |
| DryRunEval.MJ1          | Multi-Patient Tradeoffs moral/lives                        | dryrun-adept-train-MJ1.yaml          | desert      |
| DryRunEval.MJ3          | Patient tradeoff, training                                 | dryrun-adept-train-MJ3.yaml          | desert      |

### Alignment targets

There are [alignment targets](openapi_server/data/alignment_target/dryrun).
It is not expected to be able to obtain any particular alignment score against these targets, including 0 and 1.

There are also [alignment target distributions](openapi_server/data/population_data/dryrun).  These represent KDMA profiles seen during the DryRunEval.


## Run locally

Requires Python 3.10 or newer.

From the cloned repository root directory:

### Virtual environment (optional)
```shell
python -m venv --clear --upgrade-deps venv
source venv/bin/activate
```
On some systems, replace "python" with "python3".  
On Windows, the method to activate depends on the shell:
- Git Bash: `source venv/Scripts/activate`
- PowerShell: `venv\Scripts\Activate.ps1`
- cmd.exe: `venv\Scripts\activate.bat`

### Install
```shell
pip install -r requirements.txt
```

### Run
```shell
python -m openapi_server
```
then open browser to http://localhost:8080/ui/  
(You can set a custom port by using --port, e.g. `python -m openapi_server --port 8081`.)

There is also an OpenAPI definition at http://localhost:8080/openapi.json

## Running with Docker

To run the server on a Docker container, please execute the following from the cloned repository root directory:

```shell
# building the image
docker build -t openapi_server .

# starting up a container
docker run -p 8080:8080 openapi_server
```

## References
- [Info on the scenarios](https://nextcentury.atlassian.net/wiki/spaces/ITMC/pages/3206873089/Dry+Run+and+Phase+1+Evaluation#Evaluation-Scenarios)
- [ADEPT client](https://gitlab.com/itm-ta1-adept-shared/adept_client)
- [TA3's server](https://github.com/NextCenturyCorporation/itm-evaluation-server)
- [TA3's client](https://github.com/NextCenturyCorporation/itm-evaluation-client)
