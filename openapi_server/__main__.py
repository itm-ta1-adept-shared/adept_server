#!/usr/bin/env python3
import argparse

import connexion
from flask_cors import CORS

from openapi_server import encoder


def main():
    # Set up argument parser
    parser = argparse.ArgumentParser(description="Run the Flask server")
    parser.add_argument('--port', type=int, default=8080, help='Port to run the Flask server on')
    # Parse arguments
    args = parser.parse_args()

    app = connexion.App(__name__, specification_dir='./openapi/')
    app.app.json_encoder = encoder.JSONEncoder
    app.add_api('openapi.yaml',
                arguments={'title': 'ITM TA1 API'},
                pythonic_params=True)
    
    CORS(app.app, resources={r"/*": {"origins": "*"}})  # requested by one of the other teams

    # Run the app on the specified port
    app.run(port=args.port)


if __name__ == '__main__':
    main()
