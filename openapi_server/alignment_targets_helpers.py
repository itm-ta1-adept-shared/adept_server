"""Script to automatically generate the alignment target files."""
import yaml
from pathlib import Path

from openapi_server.models.alignment_target import AlignmentTarget
from openapi_server.models.kdma import KDMA

def create_aligmnent_targets_for_dryrun() -> tuple[AlignmentTarget]:
    kdma_names = ("Moral judgement", "Ingroup Bias")
    kdma_values = (0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0)
    targets = tuple(AlignmentTarget(id=f"ADEPT-DryRun-{name}-{value}", kdma_values=[KDMA(kdma=name, value=value)]) for name in kdma_names for value in kdma_values)
    return targets


def write_target_to_disk(target: AlignmentTarget) -> None:
    destination = Path("openapi_server") / "data" / "alignment_target" / "dryrun" / f"{target.id}.yaml"
    with open(file=destination, mode="w", encoding="utf-8") as file:
        yaml.dump(target.to_dict(), file, encoding="utf-8")
    return None


def main() -> None:
    targets = create_aligmnent_targets_for_dryrun()
    for target in targets:
        write_target_to_disk(target)
    return None


if __name__ == "__main__":
    main()
