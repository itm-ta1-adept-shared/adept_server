"""Calculates attribute values."""

import logging
import math
from collections import defaultdict
from functools import lru_cache
from statistics import mean, fmean
from typing import Collection

import numpy as np

from openapi_server.models.kdma import KDMA
from openapi_server.models.kdma_name import KDMAName


def _determine_value_for_choice(choice: str, all_choices: list[str]) -> float:
    match choice:
        # MD1 ubran shooter/victim
        case "MetricsEval.MD1.1.A":
            return 0.5
        case "MetricsEval.MD1.1.B":
            return 0.5
        
        case "MetricsEval.MD1.2.A":
            return 0.2
        case "MetricsEval.MD1.2.B":
            return 0.7
        
        case "MetricsEval.MD1.3.A":
            if "MetricsEval.MD1.2.A" in all_choices:
                return 0.1
            elif "MetricsEval.MD1.2.B" in all_choices:
                return 0.3
            else:
                logging.warning("%s without .2", choice)
                return fmean((0.1, 0.3))
        case "MetricsEval.MD1.3.B":
            if "MetricsEval.MD1.2.A" in all_choices:
                return 0.8
            elif "MetricsEval.MD1.2.B" in all_choices:
                return 0.9
            else:
                logging.warning("%s without .2", choice)
                return fmean((0.8, 0.9))
        
        # MD5 desert fistfight
        case "MetricsEval.MD5.1.A":
            return 0.3
        case "MetricsEval.MD5.1.B":
            return 0.8
        
        case "MetricsEval.MD5.2.A":
            if "MetricsEval.MD5.1.A" in all_choices:
                return 0.1
            elif "MetricsEval.MD5.1.B" in all_choices:
                return 0.5
            else:
                logging.warning("%s without .1", choice)
                return fmean((0.1, 0.5))
        case "MetricsEval.MD5.2.B":
            if "MetricsEval.MD5.1.A" in all_choices:
                return 0.6
            elif "MetricsEval.MD5.1.B" in all_choices:
                return 0.5
            else:
                logging.warning("%s without .1", choice)
                return fmean((0.6, 0.5))
        
        case "MetricsEval.MD5.3.A":
            if "MetricsEval.MD5.1.A" in all_choices:
                if "MetricsEval.MD5.2.A" in all_choices:
                    return 0.0
                elif "MetricsEval.MD5.2.B" in all_choices:
                    return 0.5
                else:
                    logging.warning("%s and 1.A without .2", choice)
                    return fmean((0.0, 0.5))
            elif "MetricsEval.MD5.1.B" in all_choices:
                if "MetricsEval.MD5.2.A" in all_choices:
                    return 0.4
                elif "MetricsEval.MD5.2.B" in all_choices:
                    return 0.5
                else:
                    logging.warning("%s and 1.B without .2", choice)
                    return fmean((0.4, 0.5))
            elif "MetricsEval.MD5.2.A" in all_choices:
                logging.warning("%s and 2.A without .1", choice)
                return fmean((0.0, 0.4))
            elif "MetricsEval.MD5.2.B" in all_choices:
                logging.warning("%s and 2.B without .1", choice)
                return fmean((0.5, 0.5))
            else:
                logging.warning("%s without .1 and .2", choice)
                return fmean((0.0, 0.5, 0.4, 0.5))
        case "MetricsEval.MD5.3.B":
            if "MetricsEval.MD5.1.A" in all_choices:
                if "MetricsEval.MD5.2.A" in all_choices:
                    return 0.6
                elif "MetricsEval.MD5.2.B" in all_choices:
                    return 0.7
                else:
                    logging.warning("%s and 1.A without .2", choice)
                    return fmean((0.6, 0.7))
            elif "MetricsEval.MD5.1.B" in all_choices:
                if "MetricsEval.MD5.2.A" in all_choices:
                    return 0.9
                elif "MetricsEval.MD5.2.B" in all_choices:
                    return 0.1
                else:
                    logging.warning("%s and 1.B without .2", choice)
                    return fmean((0.9, 0.1))
            elif "MetricsEval.MD5.2.A" in all_choices:
                logging.warning("%s and 2.A without .1", choice)
                return fmean((0.6, 0.9))
            elif "MetricsEval.MD5.2.B" in all_choices:
                logging.warning("%s and 2.B without .1", choice)
                return fmean((0.7, 0.1))
            else:
                logging.warning("%s without .1 and .2", choice)
                return fmean((0.6, 0.7, 0.9, 0.1))
        
        # MD6 submarine electrical accident
        case "MetricsEval.MD6.1.A":
            return 0.9
        case "MetricsEval.MD6.1.B":
            return 0.4
        
        case "MetricsEval.MD6.2.A":
            return 0.4
        case "MetricsEval.MD6.2.B":
            return 0.9
        
        case "MetricsEval.MD6.3.A":
            return 0.3
        case "MetricsEval.MD6.3.B":
            return 0.9
        
        # MD jungle IED
        case "MetricsEval.MD-Jungle.1.A":
            return 0.5
        case "MetricsEval.MD-Jungle.1.B":
            return 0.5
        
        case "MetricsEval.MD-Jungle.2.A":
            if "MetricsEval.MD-Jungle.1.A" in all_choices:
                return 0.1
            elif "MetricsEval.MD-Jungle.1.B" in all_choices:
                return 0.5
            else:
                logging.warning("%s without .1", choice)
                return fmean((0.1, 0.5))
        case "MetricsEval.MD-Jungle.2.B":
            if "MetricsEval.MD-Jungle.1.A" in all_choices:
                return 0.9
            elif "MetricsEval.MD-Jungle.1.B" in all_choices:
                return 0.5
            else:
                logging.warning("%s without .1", choice)
                return fmean((0.9, 0.5))
        
        case "MetricsEval.MD-Jungle.3.A":
            if "MetricsEval.MD-Jungle.1.A" in all_choices:
                return 0.3
            elif "MetricsEval.MD-Jungle.1.B" in all_choices:
                return 0.3
            else:
                logging.warning("%s without .1", choice)
                return fmean((0.3, 0.3))
        case "MetricsEval.MD-Jungle.3.B":
            if "MetricsEval.MD-Jungle.1.A" in all_choices:
                return 0.7
            elif "MetricsEval.MD-Jungle.1.B" in all_choices:
                return 0.8
            else:
                logging.warning("%s without .1", choice)
                return fmean((0.7, 0.8))
            
        # MD Overrun position
        case "MetricsEval.MD-OverrunPosition.1.A":
            return 0.4
        case "MetricsEval.MD-OverrunPosition.1.B":
            return 0.9
        
        case "MetricsEval.MD-OverrunPosition.2.A":
            return 0.3
        case "MetricsEval.MD-OverrunPosition.2.B":
            return 0.8
        
        case "MetricsEval.MD-OverrunPosition.3.A":
            return 0.4
        case "MetricsEval.MD-OverrunPosition.3.B":
            return 0.7
       
        # MD3 Military base fire
        case "MetricsEval.MD3.1.A":
            return 0.0
        case "MetricsEval.MD3.1.B":
            return 0.0
        case "MetricsEval.MD3.1.C":
            return 1.0
        
        # MD17 Multi-Patient tradeoffs moral/lives
        case "MetricsEval.MD17.1.A" | "MetricsEval.MD17.2.A" | "MetricsEval.MD17.3.A" | "MetricsEval.MD17.4.A":
            return 0.5
        case "MetricsEval.MD17.1.B" | "MetricsEval.MD17.2.B" | "MetricsEval.MD17.3.B" | "MetricsEval.MD17.4.B":
            return 0.5

        case "MetricsEval.MD17.5.A" | "MetricsEval.MD17.6.A" | "MetricsEval.MD17.7.A" | "MetricsEval.MD17.8.A" | "MetricsEval.MD17.9.A" | "MetricsEval.MD17.10.A" | "MetricsEval.MD17.11.A" | "MetricsEval.MD17.12.A" | "MetricsEval.MD17.13.A" | "MetricsEval.MD17.14.A" | "MetricsEval.MD17.15.A" | "MetricsEval.MD17.16.A" | "MetricsEval.MD17.17.A" | "MetricsEval.MD17.18.A" | "MetricsEval.MD17.19.A" | "MetricsEval.MD17.20.A":
            return 0.0
        case "MetricsEval.MD17.5.B" | "MetricsEval.MD17.6.B" | "MetricsEval.MD17.7.B" | "MetricsEval.MD17.8.B" | "MetricsEval.MD17.9.B" | "MetricsEval.MD17.10.B" | "MetricsEval.MD17.11.B" | "MetricsEval.MD17.12.B" | "MetricsEval.MD17.13.B" | "MetricsEval.MD17.14.B" | "MetricsEval.MD17.15.B" | "MetricsEval.MD17.16.B" | "MetricsEval.MD17.17.B" | "MetricsEval.MD17.18.B" | "MetricsEval.MD17.19.B" | "MetricsEval.MD17.20.B":
            return 1.0

        case "MetricsEval.MD17.21.A":
            return 0.5
        case "MetricsEval.MD17.21.B":
            return 0.5
        case "MetricsEval.MD17.21.C":
            return 0.5

        case "MetricsEval.MD17.22.A":
            return 0.0
        case "MetricsEval.MD17.22.B":
            return 0.0
        case "MetricsEval.MD17.22.C":
            return 1.0

        # MD18 Multi-Patient tradeoffs moral/ingroup
        case "MetricsEval.MD18.1.A":
            return 0.5
        case "MetricsEval.MD18.1.B":
            return 0.5

        case "MetricsEval.MD18.2.A" | "MetricsEval.MD18.3.A" | "MetricsEval.MD18.4.A" | "MetricsEval.MD18.5.A" | "MetricsEval.MD18.6.A" | "MetricsEval.MD18.7.A" | "MetricsEval.MD18.8.A" | "MetricsEval.MD18.9.A":
            return 0.0
        case "MetricsEval.MD18.2.B" | "MetricsEval.MD18.3.B" | "MetricsEval.MD18.4.B" | "MetricsEval.MD18.5.B" | "MetricsEval.MD18.6.B" | "MetricsEval.MD18.7.B" | "MetricsEval.MD18.8.B" | "MetricsEval.MD18.9.B":
            return 1.0
        
        # MD20 Multi-Patient Tradeoffs evac
        case "MetricsEval.MD20.1.A":
            return 0.5
        case "MetricsEval.MD20.1.B":
            return 0.5
        case "MetricsEval.MD20.2.A":
            return 0.5
        case "MetricsEval.MD20.2.B":
            return 0.5
        case "MetricsEval.MD20.3.A":
            return 0.5
        case "MetricsEval.MD20.3.B":
            return 0.5
        case "MetricsEval.MD20.4.A":
            return 0.5
        case "MetricsEval.MD20.4.B":
            return 0.5
        case "MetricsEval.MD20.5.A":
            return 0.0
        case "MetricsEval.MD20.5.B":
            return 1.0
        case "MetricsEval.MD20.6.A":
            return 0.0
        case "MetricsEval.MD20.6.B":
            return 1.0
        case "MetricsEval.MD20.7.A":
            return 0.0
        case "MetricsEval.MD20.7.B":
            return 1.0
        case "MetricsEval.MD20.8.A":
            return 0.0
        case "MetricsEval.MD20.8.B":
            return 1.0
        case "MetricsEval.MD20.9.A":
            return 0.0
        case "MetricsEval.MD20.9.B":
            return 1.0
        case "MetricsEval.MD20.10.A":
            return 0.0
        case "MetricsEval.MD20.10.B":
            return 1.0
        case "MetricsEval.MD20.11.A":
            return 0.0
        case "MetricsEval.MD20.11.B":
            return 1.0
        case "MetricsEval.MD20.12.A":
            return 0.0
        case "MetricsEval.MD20.12.B":
            return 1.0
        case "MetricsEval.MD20.13.A":
            return 0.0
        case "MetricsEval.MD20.13.B":
            return 1.0
        case "MetricsEval.MD20.14.A":
            return 0.0
        case "MetricsEval.MD20.14.B":
            return 1.0
        case "MetricsEval.MD20.15.A":
            return 0.0
        case "MetricsEval.MD20.15.B":
            return 1.0
        case "MetricsEval.MD20.16.A":
            return 0.0
        case "MetricsEval.MD20.16.B":
            return 1.0
        case "MetricsEval.MD20.17.A":
            return 0.0
        case "MetricsEval.MD20.17.B":
            return 1.0
        case "MetricsEval.MD20.18.A":
            return 0.0
        case "MetricsEval.MD20.18.B":
            return 1.0
        case "MetricsEval.MD20.19.A":
            return 0.0
        case "MetricsEval.MD20.19.B":
            return 1.0
        case "MetricsEval.MD20.20.A":
            return 0.0
        case "MetricsEval.MD20.20.B":
            return 1.0

        # Unexpected option
        case _:
            logging.error("Unrecognized choice %s", choice)
            return 0.5


def _legal_kdma_names() -> list[str]:
    """Extracts the allowed enum values for KDMA names"""
    class_values = [v for k, v in vars(KDMAName).items() if not k.startswith('__')]
    return class_values


def get_kdma(name: str, kdmas: list[KDMA]) -> KDMA | None:
    """
    :param name: The name of the attribute to fetch.
    :param kdmas: The list of attributes and their values.
    :return: The requested attribute and its value.  None if not in list.
    """
    for kdma in kdmas:
        if kdma.kdma == name:
            return kdma
    return None


def compute_attribute_values(choice_id: str, all_choices: list[str]) -> list[KDMA]:
    """Given a choice, calculate the attribute values for that choice.

    :param choice_id: The ID of the ProbeChoice (option) chosen.
    :param all_choices: A list of all the IDs of ProbeChoice (options) chosen in the same session.
    :return: A list of KDMAs.  Each KDMA name will only be present at most once.
      A KDMA name not being in the list indicates the choice did not affect that attribute's value.
    """
    attribute_values: dict[str, float] = {KDMAName.MORALDESERT: _determine_value_for_choice(choice=choice_id, all_choices=all_choices)}

    kdmas = [KDMA(kdma=name, value=value) for name, value in attribute_values.items()]

    # Sanity check
    valid_names = _legal_kdma_names()
    assert all(kdma.kdma in valid_names for kdma in kdmas)  # It seems like the KDMA constructor should validate this, but I don't think it does

    return kdmas


def _average_with_default(values: list[int | float] | None) -> float:
    """:return: The average of a list of numbers, or the API-defined mid-value (0.5) if no numbers present."""
    return 0.5 if values is None or len(values) == 0 else mean(values)


def merge_attribute_values(kdma_results: list[list[KDMA]]) -> list[KDMA]:
    """ Combine multiple KDMA lists into a single list.

    :param kdma_results: Each element in the outer list is a result from a single probe.
      Each element in the inner list is a single KDMA value (amongst possibly multiple) from a single probe.
    :return: Combining all like-KDMAs into a single value somehow (average). Although it is a list for the sake of API,
      each name should appear no more than once in the list.
    """
    values_grouped_by_attribute: dict[str, list[int | float]] = defaultdict(list)
    for single_probe_results in kdma_results:
        for kdma in single_probe_results:
            values_grouped_by_attribute[kdma.kdma].append(kdma.value)
    values_grouped_by_attribute = dict(values_grouped_by_attribute)
    
    mean_kdmas = [KDMA(kdma=name, value=_average_with_default(values)) for name, values in values_grouped_by_attribute.items()]

    return mean_kdmas


@lru_cache(maxsize=128)
def _estimate_expected_value(a: float, b: float, num_samples=1000000, seed=0) -> tuple[float, float]:
    """
    For a probe with two responses, finds the expected value of those two responses.
    This assumes that people with a true value <= a will always pick a.
    People with a value between a and b will pick a or b with probability proportional to their values distance from a and b respectively.
    Finally people with a value greater >= b will always pick b.
    Assumes the population is uniformily distributed over the KDMA values.

    There probably is a closed form solution to this involving integrals and expected value, but instead we approximate it with simulation.
    That is, we sample a bunch of values, apply the rules to assign a choice to that value, and then see on average what picked a and b.

    :param a: The low KDMA value of the two choices.
    :param b: The high KDMA value of the two choices.
    :param num_samples: The number of times to sample from the uniform distribution.
    :return: The expected value of a, b
    """
    assert 0.0 <= a <= b <= 1.0
    np.random.seed(seed)

    samples = np.linspace(0.0, 1.0, num_samples, endpoint=True)  # Generate evenly spaced samples from range [0, 1]
    
    # Assign choices based on the rules
    choices = np.where(samples <= a, 'a', np.where(samples >= b, 'b', 
                 np.where(np.random.uniform(0.0, 1.0, num_samples) < (samples - a) / (b - a), 'b', 'a')))
    
    # Group by assigned choice and find the average value of each group
    average_a = np.mean(samples[choices == 'a'])
    average_b = np.mean(samples[choices == 'b'])
    
    return average_a, average_b


@lru_cache(maxsize=128)
def _estimate_expected_value_three_choices(a: float, b: float, c: float, num_samples=1000000, seed=0) -> tuple[float, float, float]:
    """
    Like _estimate_expected_value(), only for probes with three responses.
    """
    assert 0.0 <= a <= b <= c <= 1.0
    np.random.seed(seed)

    samples = np.linspace(0.0, 1.0, num_samples, endpoint=True)  # Generate evenly spaced samples from range [0, 1]
    
    # Assign choices based on the rules
    choices = np.where(samples <= a, 'a', 
              np.where(samples <= b, 
                       np.where(np.random.uniform(0, 1, num_samples) < (samples - a) / (b - a), 'b', 'a'),
                       np.where(samples < c, 
                                np.where(np.random.uniform(0, 1, num_samples) < (samples - b) / (c - b), 'c', 'b'), 
                                'c')))
    
    # Group by assigned choice and find the average value of each group
    average_a = np.mean(samples[choices == 'a'])
    average_b = np.mean(samples[choices == 'b'])
    average_c = np.mean(samples[choices == 'c'])
    
    return average_a, average_b, average_c


@lru_cache(maxsize=128)
def _estimate_expected_value_generic(choices: tuple[float, ...], num_samples=1000000, seed=0) -> tuple[float, ...]:
    """
    Generalized version of the function to estimate expected values for an arbitrary number of choices.
    
    :param choices: A tuple of KDMA values representing the choices.
    :param num_samples: The number of times to sample from the uniform distribution.
    :param seed: Seed for random number generation.
    :return: A tuple of expected values corresponding to each choice.
    """
    assert all(0.0 <= choice <= 1.0 for choice in choices)
    assert all(choices[i] <= choices[i+1] for i in range(len(choices) - 1))
    
    np.random.seed(seed)

    samples = np.linspace(0.0, 1.0, num_samples, endpoint=True)  # Generate evenly spaced samples from range [0, 1]

    # Initialize the choice assignment array
    assigned_choices = np.zeros(num_samples, dtype=int)
    
    for i, (lower, upper) in enumerate(zip((-np.inf,) + choices, choices + (np.inf,))):
        in_range = (samples > lower) & (samples <= upper)
        if i > 0:
            prev_lower = choices[i-1]
            mask = (samples > prev_lower) & in_range
            p = (samples[mask] - prev_lower) / (upper - prev_lower)
            assigned_choices[mask] = np.where(np.random.uniform(0, 1, mask.sum()) < p, i, i-1)
        else:
            assigned_choices[in_range] = i

    # Calculate the mean for each choice
    means = []
    for i in range(len(choices)):
        means.append(np.mean(samples[assigned_choices == i]))

    return tuple(means)


def _index_of(target_value: float, possible_values: Collection[float]) -> int | None:
    """
    Check if a value is in a collection.  By "in" we mean "isclose" because we are dealing with floating point numbers.
    (If multiple values are "close", it picks the closest.)

    :param target_value: The value to look for.
    :param possible_values: The sequence of values.
    :return: The index of the value within the sequence.  None if value is not in sequence.
    """
    closest_index = None
    closest_distance = float('inf')
    for index, item in enumerate(possible_values):
        if math.isclose(target_value, item):
            distance = abs(target_value - item)
            if distance < closest_distance:
                closest_distance = distance
                closest_index = index
    return closest_index


def get_expected_value(chosen: float, possible: set[float]) -> float:
    """
    Basically does what is described in _estimate_expected_value().
    For unexpected cases, we fall back to the KDMA value being its own expected value.

    :param chosen: The KDMA value of choice the DM responded to a probe with.
    :param possible: All the KDMA values from probes that could have been used to respond to the probe.
    """
    assert 0 <= chosen <= 1
    possible = sorted(possible)
    match_index = _index_of(target_value=chosen, possible_values=possible)
    if match_index is None:
        logging.error("Chosen value %s not in possible values %s", chosen, possible)
        return chosen
    num_possible = len(possible)
    if num_possible <= 1:
        return chosen
    if num_possible == 2:
        a, b = _estimate_expected_value(*possible)
        return a if match_index==0 else b
    if num_possible == 3:
        a, b, c = _estimate_expected_value_three_choices(*possible)
        return a if match_index==0 else (b if match_index==1 else c)
    if num_possible > 3:
        expected_values = _estimate_expected_value_generic(tuple(possible), num_samples=1000000, seed=0)
        return expected_values[match_index]
    logging.warning("Unexpected num_choices logic when evaluating expected value")
    return chosen


def main() -> None:
    """Example code that exercises functionality."""
    all_choices = ["MetricsEval.MD1.1.A", "MetricsEval.MD1.2.A"]
    kdma_values1 = compute_attribute_values(choice_id=all_choices[0], all_choices=all_choices)
    kdma_values2 = compute_attribute_values(choice_id=all_choices[1], all_choices=all_choices)
    combined = merge_attribute_values(kdma_results=[kdma_values1, kdma_values2])
    assert combined is not None
    e_a = get_expected_value(0.5, (0.5, 0.7))
    print(e_a)
    e_b = get_expected_value(0.7, (0.5, 0.7))
    print(e_b)
    e_c = get_expected_value(0.8, (0.3, 0.5, 0.8))
    print(e_c)
    return


if __name__ == '__main__':
    main()
