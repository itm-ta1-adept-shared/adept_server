import connexion
from typing import Dict
from typing import Tuple
from typing import Union

import openapi_server.data_loader
import openapi_server.server_state
from openapi_server.models.alignment_results import AlignmentResults  # noqa: E501
from openapi_server.models.alignment_target import AlignmentTarget  # noqa: E501
from openapi_server.models.alignment_target_distribution import AlignmentTargetDistribution  # noqa: E501
from openapi_server.models.get_alignment_target_api_v1_alignment_target_target_id_get200_response import GetAlignmentTargetApiV1AlignmentTargetTargetIdGet200Response  # noqa: E501
from openapi_server.models.http_validation_error import HTTPValidationError  # noqa: E501
from openapi_server.models.kdma_name import KDMAName  # noqa: E501
from openapi_server.models.probe_response import ProbeResponse  # noqa: E501
from openapi_server.models.probe_response_batch import ProbeResponseBatch  # noqa: E501
from openapi_server.models.scenario import Scenario  # noqa: E501
from openapi_server.models.http_validation_error import HTTPValidationError
from openapi_server import util


_dev_mode = False  # Set to true for faster development (via cached default empty ServerState)
SERVER_STATE = openapi_server.server_state.ServerState.get_cached_empty_serverstate() if _dev_mode else openapi_server.server_state.ServerState()
"""Represents the current state of the server."""


def compare_sessions(session_id_1, session_id_2, kdma_filter=None):  # noqa: E501
    """Compare Sessions

    Calculate the alignment between the responses found in one sessions with those found in another session. # noqa: E501

    :param session_id_1: 
    :type session_id_1: str
    :param session_id_2: 
    :type session_id_2: str
    :param kdma_filter: Calculate alignment results using only this KDMA (optional).  If missing, the alignment will be calculated over the intersection of KDMAs present in the sessions.
    :type kdma_filter: dict | bytes

    :rtype: Union[AlignmentResults, Tuple[AlignmentResults, int], Tuple[AlignmentResults, int, Dict[str, str]]
    """
    if connexion.request.is_json:
        kdma_filter =  KDMAName.from_dict(connexion.request.get_json())  # noqa: E501
    return SERVER_STATE.get_alignment_sessions(session_id1=session_id_1, session_id2=session_id_2, kdma_filter=kdma_filter)


def compare_sessions_population(session_id_1_or_target_id, session_id_2_or_target_id, target_pop_id, kdma_filter=None):  # noqa: E501
    """Compare Sessions Population

    Calculate the alignment between the responses found in one session with those found in another session, within the context of a target population. # noqa: E501

    :param session_id_1_or_target_id: Either (a) the session that holds the responses or (b) a single-value target ID.
    :type session_id_1_or_target_id: str
    :param session_id_2_or_target_id: Either (a) the session that holds the responses or (b) a single-value target ID.
    :type session_id_2_or_target_id: str
    :param target_pop_id: ID of the target population.
    :type target_pop_id: str
    :param kdma_filter: Calculate alignment results using only this KDMA (optional). If missing, the alignment will be calculated over the intersection of KDMAs present in the sessions or targets.
    :type kdma_filter: dict | bytes

    :rtype: Union[AlignmentResults, Tuple[AlignmentResults, int], Tuple[AlignmentResults, int, Dict[str, str]]
    """
    if connexion.request.is_json:
        kdma_filter =  KDMAName.from_dict(connexion.request.get_json())  # noqa: E501
    return SERVER_STATE.get_alignment_sessions_population(session_id1_or_target_id=session_id_1_or_target_id,
                                                          session_id2_or_target_id=session_id_2_or_target_id,
                                                          target_pop_id=target_pop_id,
                                                          kdma_filter=kdma_filter)


def computed_kdma_profile(session_id):  # noqa: E501
    """Compute KDMA Profile

    Get KDMA profile resulting from all probe responses of the indicated session. # noqa: E501

    :param session_id: 
    :type session_id: str

    :rtype: Union[List[KDMA], Tuple[List[KDMA], int], Tuple[List[KDMA], int, Dict[str, str]]
    """
    return SERVER_STATE.get_kdma_profile(session_id=session_id)[0]


def get_alignment_target_api_v1_alignment_target__target_id__get(target_id, population=None):  # noqa: E501
    """Get Alignment Target

    Retrieve alignment target.  Parameters ---------- target_id: str     id of alignment target population: boolean     if true, treat target as distribution rather than a single set of attribute scores  Returns ---------- tgt: AlignmentTarget object     json-compatible alignment target pydantic object with schema defined in ../schema/. # noqa: E501

    :param target_id: 
    :type target_id: str
    :param population: 
    :type population: bool

    :rtype: Union[GetAlignmentTargetApiV1AlignmentTargetTargetIdGet200Response, Tuple[GetAlignmentTargetApiV1AlignmentTargetTargetIdGet200Response, int], Tuple[GetAlignmentTargetApiV1AlignmentTargetTargetIdGet200Response, int, Dict[str, str]]
    """
    if population:
        return openapi_server.data_loader.load_alignment_target_distribution(identity=target_id)
    else:
        return openapi_server.data_loader.load_object(model_type=AlignmentTarget, identity=target_id)


def get_alignment_target_distribution_api_v1_alignment_target_distribution__target_id__get(target_id):  # noqa: E501
    """Get Alignment Target Distribution

    Retrieve alignment target distribution (which captures the alignment spread of the target population).  Note: This endpoint is optional and may not be implemented by all TA1 servers.  Parameters ---------- target_id: str     id of alignment target  Returns ---------- tgt: AlignmentTargetDistribution object     json-compatible alignment target distribution pydantic object with schema defined in ../schema/. # noqa: E501

    :param target_id: 
    :type target_id: str

    :rtype: Union[AlignmentTargetDistribution, Tuple[AlignmentTargetDistribution, int], Tuple[AlignmentTargetDistribution, int, Dict[str, str]]
    """
    return openapi_server.data_loader.load_alignment_target_distribution(identity=target_id)


def get_alignment_target_distribution_ids_api_v1_alignment_target_distribution_ids_get():  # noqa: E501
    """Get alignment targets distribution IDs

    Retrieve an array of alignment targets distribution IDs.  Each ID is a valid identifier for an AlignmentTargetDistribution. # noqa: E501


    :rtype: Union[List[str], Tuple[List[str], int], Tuple[List[str], int, Dict[str, str]]
    """
    return openapi_server.data_loader.get_known_alignment_target_distriubtion_ids()


def get_alignment_target_ids_api_v1_alignment_target_ids_get():  # noqa: E501
    """Get alignment targets IDs

    Retrieve an array of alignment targets IDs.  Each ID is a valid identifier for an AlignmentTarget. # noqa: E501


    :rtype: Union[List[str], Tuple[List[str], int], Tuple[List[str], int, Dict[str, str]]
    """
    return openapi_server.data_loader.get_known_alignment_target_ids()


def get_api_api_v1__get():  # noqa: E501
    """Get Api

    Return API version, can be used to check connection.  Parameters ----------- None  Returns -------- version: dict     key (str): &#39;api_version&#39;, value (str): version string # noqa: E501


    :rtype: Union[object, Tuple[object, int], Tuple[object, int, Dict[str, str]]
    """
    return {'api_version': 1}


def get_ordered_alignment_and_scores_api_v1_get_ordered_alignment_and_scores_get(session_id, kdma_id):  # noqa: E501
    """Get Ordered List of Alignments and Scores

    Returns an array of mappings of alignment target IDs to alignments scores, ordered from the most to least aligned to the decision maker that completed the specified session_id. Does not consider group targets or any targets created by the &#x60;alignment_target/{target_id}&#x60; call as candidates to match against. Passing in a group target as the &#x60;session_id&#x60; is not a supported use case and will return a 404 error. # noqa: E501

    :param session_id: The session Id of the decision-maker
    :type session_id: str
    :param kdma_id: Name of KDMA
    :type kdma_id: str

    :rtype: Union[List[Dict[str, str]], Tuple[List[Dict[str, str]], int], Tuple[List[Dict[str, str]], int, Dict[str, str]]
    """
    return SERVER_STATE.get_ordered_alignment_and_scores(session_id=session_id, kdma_id=kdma_id)


def _obj_to_bool(value: object) -> bool:
    """
    Raises if value isn't in one of the boolean-like forms expected.

    :param value: The object to convert to a boolean.
    :return: The value as a bool.
    """
    if value is None:
        value = False
    elif isinstance(value, str):
        if value == "True" or value == "true" or value == "1":
            value = True
        elif value == "False" or value == "false" or value == "0":
            value = False
    elif isinstance(value, int):
        if value == 1:
            value = True
        elif value == 0:
            value = False
    if isinstance(value, bool):
        return value
    raise ValueError(f"Unexpected value for value: {value}")

def get_probe_response_alignment_api_v1_alignment_probe_get(session_id, target_id, scenario_id, probe_id, population=None):  # noqa: E501
    """Get Probe Response Alignment

    Get probe-level alignment # noqa: E501

    :param session_id: 
    :type session_id: str
    :param target_id: 
    :type target_id: str
    :param scenario_id: 
    :type scenario_id: str
    :param probe_id: 
    :type probe_id: str
    :param population: 
    :type population: bool

    :rtype: Union[AlignmentResults, Tuple[AlignmentResults, int], Tuple[AlignmentResults, int, Dict[str, str]]
    """
    population = _obj_to_bool(value=population)
    return SERVER_STATE.get_alignment_result_probe(session_id=session_id, target_id=target_id, scenario_id=scenario_id, probe_id=probe_id, population=population)


def get_scenario_api_v1_scenario__scenario_id___get(scenario_id):  # noqa: E501
    """Get Scenario

    Retrieve scenario.  Parameters ----------- scenario_id: str, required     id of scenario  Returns ------------ scenario: Scenario object     json-compatible scenario pydantic object with schema defined in ../schema/. # noqa: E501

    :param scenario_id: 
    :type scenario_id: str

    :rtype: Union[Scenario, Tuple[Scenario, int], Tuple[Scenario, int, Dict[str, str]]
    """
    return openapi_server.data_loader.load_object(model_type=Scenario, identity=scenario_id)


def get_session_alignment_api_v1_alignment_session_get(session_id, target_id, population=None):  # noqa: E501
    """Get Session Alignment

    Get alignment for all probe responses in a session. # noqa: E501

    :param session_id: 
    :type session_id: str
    :param target_id: 
    :type target_id: str
    :param population: 
    :type population: bool

    :rtype: Union[AlignmentResults, Tuple[AlignmentResults, int], Tuple[AlignmentResults, int, Dict[str, str]]
    """
    population = _obj_to_bool(value=population)
    return SERVER_STATE.get_alignment_result_session(session_id=session_id, target_id=target_id, population=population)


def post_new_session_id_api_v1_new_session_post():  # noqa: E501
    """Post New Session Id

    Get unique session id for grouping answers from a collection of scenarios/probes together when computing kdma values/alignment results. # noqa: E501


    :rtype: Union[str, Tuple[str, int], Tuple[str, int, Dict[str, str]]
    """
    return SERVER_STATE.generate_session_id()


def post_probe_response_api_v1_response__post(body):  # noqa: E501    For some reason we need to change the parameter from probe_response to body
    """Post Probe Response

    Send probe response to be stored in database.  Parameters ---------- response: ProbeResponse object (defined in pydantic schema)     Contains session id, probe id, response id  Returns ----------- None # noqa: E501

    :param probe_response: 
    :type probe_response: dict | bytes

    :rtype: Union[object, Tuple[object, int], Tuple[object, int, Dict[str, str]]
    """
    if connexion.request.is_json:
        probe_response = ProbeResponse.from_dict(connexion.request.get_json())  # noqa: E501
        SERVER_STATE.add_probe_response(probe_response=probe_response)
        return {}
    else:
        return HTTPValidationError("Expected body to be a JSON ProbeResponse"), 422


def post_probe_responses_api_v1_responses__post(body):  # noqa: E501    For some reason we need to change the parameter from probe_response_batch to body
    """Post Probe Responses

    Send collection of probe responses to be stored in database.  Parameters ---------- session_id: str     unique id for user session probe_ids: List[str]:     list of id&#39;s corresponding to probes response_ids: List[str]     list of id&#39;s corresponding to responses  Returns ----------- None # noqa: E501

    :param probe_response_batch: 
    :type probe_response_batch: dict | bytes

    :rtype: Union[object, Tuple[object, int], Tuple[object, int, Dict[str, str]]
    """
    if connexion.request.is_json:
        probe_response_batch = ProbeResponseBatch.from_dict(connexion.request.get_json())  # noqa: E501
        SERVER_STATE.add_probe_responses(probe_responses=probe_response_batch)
        return {}
    else:
        return HTTPValidationError("Expected body to be a JSON ProbeResponseBatch"), 422


def reset_server_state_api_v1_reset_post():  # noqa: E501
    """Reset server state

    Resets the server state, clearing all sessions, responses, and turning off autosave.  (This action is not automatically saved.) # noqa: E501


    :rtype: Union[object, Tuple[object, int], Tuple[object, int, Dict[str, str]]
    """
    SERVER_STATE.reset()
    return {}


def restore_server_state_api_v1_restore_post():  # noqa: E501
    """Restore the server state

    Restore the server state (session, responses, autosave). # noqa: E501


    :rtype: Union[object, Tuple[object, int], Tuple[object, int, Dict[str, str]]
    """
    return {}, SERVER_STATE.load_state()


def save_server_state_api_v1_save_post():  # noqa: E501
    """Save the server state

    Save the server state (sessions, responses, autosave). # noqa: E501


    :rtype: Union[object, Tuple[object, int], Tuple[object, int, Dict[str, str]]
    """
    SERVER_STATE.save_state()
    return {}


def set_autosave_api_v1_autosave_post(enabled: bool):  # noqa: E501
    """Set autosave

    Enable or disable the auto-save feature.  Auto-save is off by default. # noqa: E501

    :param enabled: 
    :type enabled: bool

    :rtype: Union[object, Tuple[object, int], Tuple[object, int, Dict[str, str]]
    """
    SERVER_STATE.set_autosave(enabled=enabled)
    return {}
