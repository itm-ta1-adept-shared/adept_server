"""Load from data YAML (or JSON) to Python objects"""

import json
import logging
import typing
from collections import OrderedDict
from functools import lru_cache
from pathlib import Path

import yaml

from openapi_server import portability
from openapi_server.models.alignment_target import AlignmentTarget
from openapi_server.models.alignment_target_distribution import AlignmentTargetDistribution
from openapi_server.models.base_model import Model
from openapi_server.models.probe_response_batch import ProbeResponseBatch
from openapi_server.models.scenario import Scenario

_ROOT_DIR: typing.Final[Path] = portability.repo_root() / "openapi_server" / "data"
"""Absolute Path where data is stored."""

_DIR_LOOKUP: typing.Final[dict[typing.Type[Model], tuple[str, str]]] = {AlignmentTarget: ("alignment_target", "id"),
                                                                        AlignmentTargetDistribution: ("population_data", "id"),
                                                                        Scenario: ("scenario", "id"),
                                                                        ProbeResponseBatch: ("probe_response_batch", "session_id")}
"""Directories that contain data files of a requested type.  And the name of the key that will serve to recognize the instance."""

_DIR_LOOKUP_REVERSE: typing.Final[dict[str, typing.Type[Model]]] = OrderedDict((v[0], k) for k, v in _DIR_LOOKUP.items())
"""A reverse lookup of the directory name to the data type."""


@lru_cache(maxsize=128)
def _find_file(subdirectory: str, key: str, value: typing.Any) -> typing.Optional[dict[str, typing.Any]]:
    """ Find a YAML or JSON file that has a specific key-value pair at its top level.

    :param subdirectory: The subdirectory (of the root dir) to look.
    :param key: The key to look for.
    :param value: The value to look for.
    """
    # Define the directory
    directory = _ROOT_DIR / subdirectory

    # Use rglob to search for YAML, YML, and JSON files recursively
    for file_path in directory.rglob('*'):
        if file_path.suffix in ['.yaml', '.yml', '.json']:
            # Open and load the file
            with open(file_path, 'r', encoding="utf-8") as file:
                try:
                    # Load file content as per file type
                    if file_path.suffix == '.json':
                        data = json.load(file)
                    else:
                        data = yaml.safe_load(file)

                    if key in data and data[key] == value:
                        return data
                except Exception as e:
                    logging.warning(f"Failed to load {file_path}: {e}")
    logging.warning("Failed to find %s, %s, %s", subdirectory, key, value)
    return None


T = typing.TypeVar("T", bound=Model)
""" A generic type variable, constrained to 'Model' and its subclasses. """


@lru_cache(maxsize=128)
def load_object(model_type: typing.Type[T], identity: str) -> T | None:
    """ Get data from a file in its Python object form.

    :param model_type: The type of model to load. This should be a subclass of Model, e.g. AlignmentTarget.
    :param identity: The value of the id field of the data you want to load.
    :return: The data as an object.
      Or None if problem loading data (e.g. couldn't find ID amongst the data files).
    """
    subdir, key = _DIR_LOOKUP[model_type]
    data = _find_file(subdirectory=subdir, key=key, value=identity)
    return None if data is None else model_type.from_dict(data)


@lru_cache(maxsize=128)
def load_object_by_filename(filepath: str) -> Model | None:
    """
    :param filepath: The path of the file that contains the data you want to load.
      It is assumed to live in (or a subdir of) a directory that identifies the data type.
      e.g. "openapi_server/data/probe_response_batch/mvp/kitware/bbn_aligned_responses.json"
      has probe_response_batch, to indicate that it is a ProbeResponseBatch.
    :return: A Python object representation of the data at the indicated path. """
    known_directories = tuple(value[0] for value in _DIR_LOOKUP.values())
    for known_dir in known_directories:
        if known_dir in filepath:
            data_type = _DIR_LOOKUP_REVERSE[known_dir]
            with open(file=filepath, mode='r', encoding="utf-8") as file:
                if filepath.endswith('.json'):
                    data = json.load(file)
                else:
                    data = yaml.safe_load(file)
            return data_type.from_dict(data)
    logging.warning("Failed to find %s", filepath)
    return None


@lru_cache(maxsize=10)
def _get_known_ids_of_type(model_type: typing.Type[T]) -> list[str]:
    """ Get IDs of an indicated type that exist in our data,

    :param model_type: The type of IDs to collect.  Must be one of the types mapped in `_DIR_LOOKUP`.
    :return: The collected IDs.
    """
    subdir, key = _DIR_LOOKUP[model_type]
    assert key == 'id'  # not true for ProbeResponseBatch, but we aren't currently using that
    directory = _ROOT_DIR / subdir
    ids = []
    for file_path in directory.rglob('*.yaml'):
        # Open and load the YAML file
        with open(file_path, 'r', encoding="utf-8") as file:
            try:
                data = yaml.safe_load(file)
                if key in data:
                    assert isinstance(data[key], str), "Unexpected ID type found"
                    ids.append(data[key])
                else:
                    logging.info(f"{file_path.name} doesn't have ID")
            except Exception as e:
                logging.warning(f"Failed to load {file_path} due to: {e}")
    return ids


@lru_cache(maxsize=1)
def get_known_scenario_ids() -> list[str]:
    """ Scenario IDs that exist in our data

    :return: Scenario IDs.
    """
    return _get_known_ids_of_type(Scenario)


@lru_cache(maxsize=1)
def get_known_alignment_target_ids() -> list[str]:
    """ Alignment Target IDs that exist in our data

    :return: Alignment Target IDs.
    """
    return _get_known_ids_of_type(AlignmentTarget)


@lru_cache(maxsize=1)
def get_known_alignment_target_distriubtion_ids() -> list[str]:
    """ Alignment Target Distribution IDs that exist in our data

    :return: Alignment Target Distribution IDs.
    """
    return _get_known_ids_of_type(AlignmentTargetDistribution)


def _collect_values(d: dict | list, key: str) -> typing.Generator[object, None, None]:
    """Recursively collect values from a dictionary or list of dictionaries that match a given key.
    
    :param d: The dictionary or list of dictionaries to search through.
    :param key: The key for which values are being searched.
    :return: A generator yielding values associated with the specified key.
    """
    if isinstance(d, dict):
        for k, v in d.items():
            if k == key:
                yield v
            elif isinstance(v, (dict, list)):
                yield from _collect_values(v, key)
    elif isinstance(d, list):
        for item in d:
            yield from _collect_values(item, key)


@lru_cache(maxsize=1)
def get_known_probe_ids() -> dict[str, list[str]]:
    """ Probe IDs that exist in our data

    :return: {<scenario id>: [probe ids]}
    """
    subdir, key = _DIR_LOOKUP[Scenario]
    assert key == 'id'
    directory = _ROOT_DIR / subdir
    ids = dict()
    # Use rglob to search for YAML files recursively
    for file_path in directory.rglob('*.yaml'):
        # Open and load the YAML file
        with open(file_path, 'r', encoding="utf-8") as file:
            try:
                data = yaml.safe_load(file)
                if key in data:
                    assert isinstance(data[key], str), "Unexpected ID type found"
                    scenario_id = data[key]
                    if scenario_id not in ids:
                        ids[scenario_id] = []
                    probe_ids = set(_collect_values(data, "probe_id"))
                    ids[scenario_id].extend(probe_ids)
                else:
                    logging.info(f"{file_path.name} doesn't have an ID")
            except Exception as e:
                logging.warning(f"Failed to load {file_path} due to: {e}")
    return ids


def _add_pop_to_alignment_target_id(identity: str) -> str | None:
    """
    :param identity: The id to modify
    :return: The id with pop added at the right spot.  None if couldn't find the spot, or if id already has pop.
    """
    if identity.find("alignment-target-") == -1:
        return None  # not of the expected form
    if identity.find("alignment-target-pop-") != -1:
        return None  # pop is already there
    return identity.replace("alignment-target-", "alignment-target-pop-")


def load_alignment_target_distribution(identity: str) -> AlignmentTargetDistribution:
    """Loads an alignment target distribution from population data if it exists.
    It will also attempt to add pop to the name in case the caller gave us the non-population ID of the target.
    
    :param identity: The id of the alignment target [distribution] to load.
    :return: The alignment target distribution.  None if not found.
    """
    a_t_d: AlignmentTargetDistribution = load_object(model_type=AlignmentTargetDistribution, identity=identity)
    if a_t_d is None:
        a_t_d: AlignmentTargetDistribution = load_object(model_type=AlignmentTargetDistribution, identity=_add_pop_to_alignment_target_id(identity=identity))
        if a_t_d is not None:
            logging.info("Loading %s instead of requested %s", _add_pop_to_alignment_target_id(identity=identity), identity)
    if a_t_d is None:
        logging.warning("Didn't find a source for AlignmentTargetDistribution %s", identity)
    return a_t_d


def main() -> None:
    """Example code that exercises functionality."""
    s1 = load_object(model_type=Scenario, identity="MetricsEval.MD4-Jungle")
    at = load_object(model_type=AlignmentTarget, identity="ADEPT-metrics_eval-alignment-target-train-HIGH")
    # Disabled until we port MVP stuff to latest (e.g. 0-10 to 0-1): atd = load_object(model_type=AlignmentTargetDistribution, identity="ADEPT-synthetic-data-1")
    sids = get_known_scenario_ids()
    pids = get_known_probe_ids()
    at_ids = get_known_alignment_target_ids()
    atd_ids = get_known_alignment_target_distriubtion_ids()
    # Disabling these until we can support MVP1 and MVP2 again
    # prb = load_object(model_type=ProbeResponseBatch,
    #                   identity="a381c6d9-3a2f-4a39-ab57-964fe9858278")
    # prb2 = load_object_by_filename("openapi_server/data/probe_response_batch/mvp/kitware/bbn_aligned_responses.json")
    # assert prb2 is not None
    load_alignment_target_distribution("ADEPT-metrics_eval-alignment-target-pop-train-HIGH")
    load_alignment_target_distribution("ADEPT-metrics_eval-alignment-target-train-HIGH")
    return


if __name__ == '__main__':
    import openapi_server.portability
    openapi_server.portability.init_cwd_to_repo_root()
    main()
