from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from openapi_server.models.base_model import Model
from openapi_server import util


class DirectnessEnum(Model):
    """NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).

    Do not edit the class manually.
    """

    """
    allowed enum values
    """
    DIRECT = 'direct'
    SOMEWHAT_DIRECT = 'somewhat direct'
    SOMEWHAT_INDIRECT = 'somewhat indirect'
    INDIRECT = 'indirect'
    NONE = 'none'
    def __init__(self):  # noqa: E501
        """DirectnessEnum - a model defined in OpenAPI

        """
        self.openapi_types = {
        }

        self.attribute_map = {
        }

    @classmethod
    def from_dict(cls, dikt) -> 'DirectnessEnum':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The DirectnessEnum of this DirectnessEnum.  # noqa: E501
        :rtype: DirectnessEnum
        """
        return util.deserialize_model(dikt, cls)
