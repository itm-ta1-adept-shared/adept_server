""" Misc routines for dealing with platform or environment differences. """
import logging
import os
from pathlib import Path


def is_pycharm() -> bool:
    """ :return: True if we are running in the PyCharm IDE.  Else False. """
    return os.environ.get("PYCHARM_HOSTED") == '1'


def repo_root() -> Path:
    """ :return: Root directory of the repo. """
    mydir = Path(__file__).parent
    assert mydir.name == "openapi_server", "This file should be one deeper than the workspace root"
    return mydir.parent


def init_cwd_to_repo_root() -> None:
    """First time it is called, changes the working directory to the repo root."""
    # When I run VSCode it is usually from the repo root, and that dir is used
    # as the current working directory.
    # When I run a file in PyCharm, it uses that file's dir as the current
    # working directory.
    # There has got to be a better way to deal with this...
    if not hasattr(init_cwd_to_repo_root, "inited"):
        init_cwd_to_repo_root.inited = True
        os.chdir(repo_root())
    return


_cached_cwd: list[str] = []
"""A FILO sequence of current working directories."""


def cache_cwd() -> None:
    """Take note of the current working directory.  Can be restored (FILO) with restore_cwd()"""
    _cached_cwd.append(os.getcwd())
    return


def restore_cwd() -> None:
    """Restore the working directory with a previously cached one (FILO)."""
    if len(_cached_cwd) > 0:
        os.chdir(_cached_cwd.pop())
    else:
        logging.warning("Request to restore cwd when none has been cached.")
    return


def cache_and_cwd_to_repo_root() -> None:
    """Caches the current working directory and changes the working directory to the repo root.
    Usually paired with a restore_cwd() later.
    """
    # When I run VSCode it is usually from the repo root, and that dir is used
    # as the current working directory.
    # When I run a file in PyCharm, it uses that file's dir as the current
    # working directory.
    # There has got to be a better way to deal with this...
    cache_cwd()
    os.chdir(repo_root())
    return


def main() -> None:
    """ Demonstrate some functionality """
    ispycharm = is_pycharm()
    print(f"You are{'' if ispycharm else 'nt'} using PyCharm.  Good for you.")
    return


if __name__ == "__main__":
    main()
