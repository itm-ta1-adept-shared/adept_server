"""Save Python objects to disk."""

import logging
import os
import pickle
import re
import sys
import typing

try:
    from google.colab import drive
except ModuleNotFoundError:
    pass


class SaveHelper:
    """
    Save Python objects to disk.
    All I/O happens with paths relative to the "base directory", which is a location hard coded into this class.
    On Windows it is the local folder.
    Example usage, save a list and read it back out.::

        SaveHelper.to_pickle(obj=[1,2,3], path="out/list1.pkl")
        list1 = SaveHelper.from_pickle(path="out/list1.pkl")

    """
    _base_dir = None
    """The root path which paths are relative to.  Including closing slash."""
    _logger = logging.getLogger("SaveHelper")
    """Where to write diagnostic messages to.  Use set_logger to change."""

    read_disabled = False
    """Requests to read will always return None."""
    write_disabled = False
    """Requests to write will be ignored."""

    @classmethod
    def _platform_specific_init(cls) -> str:
        """
        Perform an setup needed before we can do our I/O to/from the base directory.\n
        :return: The base directory being used.
        """
        if cls.read_disabled and cls.write_disabled:
            return ""

        if cls._base_dir is not None:
            return cls._base_dir

        try:
            sys.getwindowsversion()
        except AttributeError:
            is_windows = False
        else:
            is_windows = True

        if is_windows:
            cls._base_dir = "./"
        else:
            if 'drive' in sys.modules:
                drive.mount('/content/drive')
                cls._base_dir = '/content/drive/My Drive/output/'
            else:
                cls._base_dir = './'  # '/tmp/' might be another option
        return cls._base_dir

    @classmethod
    def to_pickle(cls, obj: object, path: str) -> None:
        """
        Save to a Pickle file.\n
        :param obj: Python object to save.
        :param path: Location to save to (relative to base_dir).
        """
        if cls.write_disabled:
            return
        try:
            path = cls.path_cleaner(path)
            path = cls.make_dirs(path)
            with open(path, 'wb') as filehandler:
                pickle.dump(obj=obj, file=filehandler, protocol=pickle.HIGHEST_PROTOCOL)
        except Exception:
            cls._logger.exception("Unable to save to %s.", path)
        return None

    @classmethod
    def from_pickle(cls, path: typing.Union[str, list[str]]) -> object:
        """
        Read from a Pickle file.\n
        :param path: Location to be read from (relative to base_dir).
          Can also be a list of locations to read, in which case we stop at the first success.
        :return: Python object that was read.  None if unable to read file.
        """
        if cls.read_disabled:
            return None

        if isinstance(path, list):
            for p in path:
                result = cls.from_pickle(path=p)
                if result is not None:
                    cls._logger.debug("Loaded %s.", p)
                    return result
            return None
        try:
            path = cls.path_cleaner(path)
            path = cls.make_dirs(path)
            with open(path, 'rb') as filehandler:
                retval = pickle.load(file=filehandler)
            return retval
        except FileNotFoundError:
            pass
        except Exception:
            cls._logger.exception("SaveHelper.from_pickle(%s)", path)
        return None
    
    @classmethod
    def make_dirs(cls, path: str) -> str:
        """
        Create any folders needed to perform the IO.\n
        to_pickle and from_pickle will call this as needed,
        but this is also made available as a public method in case it is useful.\n
        :param path: Path relative to base_dir.
        :return: The absolute version of the path passed in.
        """
        cls._platform_specific_init()
        fullpath = cls._base_dir + path
        dirname, _ = os.path.split(p=fullpath)
        os.makedirs(name=dirname, exist_ok=True)
        return fullpath

    @classmethod
    def concat_to_base_dir(cls, additional_dir: str) -> None:
        """
        A way to modify where things are saved, kind of like a change directory.
        Not typically needed.  Experimental.\n
        :param additional_dir: Don't include opening slash, do include closing slash.
        """
        assert additional_dir[0] != '/'
        assert additional_dir[-1] == '/'
        cls._platform_specific_init()
        cls._base_dir += additional_dir

    @staticmethod
    def path_cleaner(unclean: str) -> str:
        """
        Strip symbols that don't work with the filesystem from the string.\n
        to_pickle and from_pickle use this internally.  It is exposed as a public method in case you want to use this
        with some other persistence function (like plt.savefig()).\n
        :param unclean: The original string.
        :return: The cleaned string.
        """
        return re.sub('[{}" :\']', '', unclean)  # make safe for filename

    @classmethod
    def get_base_dir(cls) -> str:
        """:return: The root path to which paths used in this class are relative to.  Includes slash at end."""
        return cls._base_dir
    
    @classmethod
    def set_logger(cls, logger: typing.Union[logging.Logger, int]) -> None:
        """
        :param logger: The logger to use; or if an int it will use an internal logger at that level.
        """
        if isinstance(logger, int):
            cls._logger = logging.getLogger(cls.__name__)
            cls._logger.setLevel(level=logger)
        else:
            cls._logger = logger
        return None
