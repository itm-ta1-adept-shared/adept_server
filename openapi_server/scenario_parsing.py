"""Logic to extract pieces of info from a Scenario."""
import ast
import logging
from collections import defaultdict
from functools import lru_cache

from openapi_server import data_loader
from openapi_server.models.scenario import Scenario

def check_for_duplicate_probes() -> None:
    """
    Look to see if (within a scenario) the same probe is answered in multiple scenes.
    We did this because we want to automatically be able to extract the "responses not taken" in the same scene.  And the same probe in multiple scenes would muddy that.
    """
    scenario_ids = "DryRunEval.IO1", "DryRunEval.MJ1", "DryRunEval-MJ2-eval", "DryRunEval-MJ4-eval"
    probes_by_scenario = defaultdict(dict[str, set])
    for scenario_id in scenario_ids:
        scenario = data_loader.load_object(Scenario, scenario_id)
        assert scenario.id == scenario_id
        for scene in scenario.scenes:
            for action in scene.action_mapping:
                probes_by_scenario[scenario_id].setdefault(action.probe_id, set())
                probes_by_scenario[scenario_id][action.probe_id].add(scene.id)
    print()
    for scenario_id, scenario_probes in probes_by_scenario.items():
        for probe_id, scenes in scenario_probes.items():
            if len(scenes) > 1:
                print(f"Scenario {scenario_id}: probe {probe_id} scenes: {scenes}")
    # If something gets printed, you might have a duplicate probe issue
    # If nothing printed here, you should be good
    return


@lru_cache(maxsize=128)
def get_kmda_values_for_probe(scenario_id: str, probe_id: str) -> dict[str, set[float]]:
    """
    Get the possible KDMA values for a probe.
    :param scenario_id: The scenario to search.
    :param probe_id: The probe to look up.
    :return: Dictionary, with key of KDMA name and value is the possible values for that KDMA (unsorted).
    """
    scenario: Scenario = data_loader.load_object(Scenario, scenario_id)

    # Special cases
    if scenario.id == "DryRunEval-MJ2-eval" and probe_id == "Probe 3-B.2":
        return {"Moral judgement": {0.2, 0.4, 0.7}}
    if scenario.id == "DryRunEval-MJ5-eval" and probe_id == "Probe 3":
        return {"Moral judgement": {0.1, 0.2, 0.4, 0.6, 0.7, 1.0}}
    if scenario.id == "DryRunEval.IO2":
        if probe_id == "P1":
            return {"Ingroup Bias": {0.8, 0.4, 0.0}}
        if probe_id == "P2":
            return {"Ingroup Bias": {0.75, 0.3, 0.0}}
        if probe_id == "P3":
            return {"Ingroup Bias": {0.75, 0.2, 0.0}}
        if probe_id == "P4":
            return {"Ingroup Bias": {0.7, 0.1, 0.0}}
        if probe_id == "P5":
            return {"Ingroup Bias": {0.55, 0.5, 0.4}}
        if probe_id == "P6":
            return {"Ingroup Bias": {0.5, 0.45, 0.4}}
        if probe_id == "P7":
            return {"Ingroup Bias": {0.5, 0.4, 0.35}}
        if probe_id == "P8":
            return {"Ingroup Bias": {0.5, 0.3, 0.05}}
        if probe_id == "P9":
            return {"Ingroup Bias": {1.0, 0.7, 0.4}}
        if probe_id == "P10":
            return {"Ingroup Bias": {0.9, 0.65, 0.4}}
        if probe_id == "P11":
            return {"Ingroup Bias": {0.8, 0.6, 0.35}}
        if probe_id == "P12":
            return {"Ingroup Bias": {0.65, 0.45, 0.25}}
        if probe_id == "P13":
            return {"Ingroup Bias": {1.0, 0.7, 0.4}}
        if probe_id == "P14":
            return {"Ingroup Bias": {0.9, 0.65, 0.4}}
        if probe_id == "P15":
            return {"Ingroup Bias": {0.8, 0.55, 0.35}}
        if probe_id == "P16":
            return {"Ingroup Bias": {0.7, 0.45, 0.25}}

    kdmas = defaultdict(set[float])
    for scene in scenario.scenes:
        for action in scene.action_mapping:
            if action.probe_id == probe_id and action.kdma_association is not None:  # rare, but some probes don't have KDMAs associated with them
                for kdma_name, kdma_value in action.kdma_association.items():
                    kdmas[kdma_name].add(kdma_value)
    return kdmas

@lru_cache(maxsize=128)
def get_kdma_values_for_response(scenario_id: str, probe_id: str, choice: str) -> dict[str, float] | None:
    """
    Get the KDMA values for a particular response.
    :param scenario_id: The scenario to search.
    :param probe_id: The probe being responsed to.
    :param choice: The choice that was made. (Or special case, KDMA direclty given to this probe.)
    :return: The KDMA values for the specified choice, or None if it wasn't found.
    """
    try:
        # Special case if a KDMA value is directly inserted into the choice (special case for gauze counting and blood-giving)
        dict_attempt = ast.literal_eval(choice)
        if isinstance(dict_attempt, dict):
            return dict_attempt
    except Exception as e:
        logging.info("When trying to literal_eval, %s", e)
    scenario = data_loader.load_object(Scenario, scenario_id)
    for scene in scenario.scenes:
        for action in scene.action_mapping:
            if action.probe_id == probe_id and action.choice == choice:
                return action.kdma_association
    logging.error("Couldn't find chosen value scenario %s, probe_id %s, choice %s", scenario_id, probe_id, choice)
    return None


def main() -> None:

    print(get_kmda_values_for_probe("DryRunEval.IO1", "DryRunEval.IO1.1"))
    print(get_kmda_values_for_probe("DryRunEval-MJ2-eval", "Probe 7"))  # empty
    print(get_kmda_values_for_probe("DryRunEval-MJ2-eval", "Probe 7 victim"))  # duplicate 0s
    print(get_kmda_values_for_probe("DryRunEval-MJ4-eval", "Probe 10"))  # multiple KDMAs

    return


if __name__ == "__main__":
    main()
