"""
Tracks information we have received from users.
The main object of the application.
The endpoint handlers are defined in openapi_server/controllers/default_controller.py, but those
just mostly call into a corresponding function in this class.
"""
import copy
import logging
import math
import typing
import uuid
from collections import defaultdict

from openapi_server import alignment_score
from openapi_server import attribute_values
from openapi_server import data_loader
from openapi_server import scenario_parsing
from openapi_server.models.alignment_results import AlignmentResults
from openapi_server.models.alignment_source import AlignmentSource
from openapi_server.models.alignment_target import AlignmentTarget
from openapi_server.models.kdma import KDMA
from openapi_server.models.kdma_name import KDMAName
from openapi_server.models.probe_response import ProbeResponse
from openapi_server.models.probe_response_batch import ProbeResponseBatch
from openapi_server.models.response import Response
from openapi_server.models.scenario import Scenario
from openapi_server.save_helper import SaveHelper


class ServerState:
    """Represents the state of the server as clients interact with us.
    For example tracks sessions, holds responses, etc.
    """

    SessionIdType = str
    """A typedef for how we represent a session ID.  It is a uuid, but as a string."""

    def __init__(self) -> None:
        self._current_session_id: ServerState.SessionIdType | None = None
        """Current active session"""
        self._session_ids: set[ServerState.SessionIdType] = set()
        """All sessions so far"""
        self._probe_responses_by_session: dict[ServerState.SessionIdType, list[Response]] = dict()
        """{session id: responses given for that session}"""
        self._known_scenario_ids: typing.Final[list[str]] = data_loader.get_known_scenario_ids()
        """All scenario IDs the application supports."""
        self._known_probe_ids: typing.Final[dict[str, list[str]]] = data_loader.get_known_probe_ids()
        """All probe IDs the application supports."""
        self._known_target_ids: typing.Final[list[str]] = data_loader.get_known_alignment_target_ids()
        """All alignment target IDs the application supports."""
        self._scenarios: dict[str, Scenario] = {scenario_id: data_loader.load_object(model_type=Scenario, identity=scenario_id) for scenario_id in self._known_scenario_ids}
        """Cache of the scenario data."""
        self._autosave: bool = False
        """If we should save the state each time there is a change."""
        return None

    def _change_session_id(self, session_id: SessionIdType) -> None:
        """ :param session_id: The session ID to change to. """
        self._session_ids.add(session_id)
        self._current_session_id = session_id
        if self._autosave:
            self.save_state()
        return

    def _handle_session_id(self, session_id: SessionIdType | None) -> SessionIdType:
        """ Helper function for dealing if session ID was or wasn't given.
        If it was given, we'll assume we want to use that session ID.
        If it wasn't, we'll use the current ID, or generate a new one if there isn't a current.

        :param session_id: Proposed session ID.
        :return: Session ID to use.
        """
        if session_id is not None:
            self._change_session_id(session_id=session_id)
            return session_id
        if self._current_session_id is None:
            return self.generate_session_id()
        return self._current_session_id
        
    def reset(self) -> None:
        """Forget responses we have been sent."""
        self._current_session_id: ServerState.SessionIdType | None = None
        self._session_ids: set[ServerState.SessionIdType] = set()
        self._probe_responses_by_session: dict[ServerState.SessionIdType, list[Response]] = dict()
        self._autosave = False
        return None

    def generate_session_id(self) -> str:
        """ Generate a new session ID and set it as the active session.
        
        :return: The new session ID.
        """
        self._change_session_id(session_id=str(uuid.uuid4()))
        return self._current_session_id
    
    def add_probe_response(self, probe_response: ProbeResponse) -> None:
        """Take note of a new response to a single probe."""
        probe_response.session_id = self._handle_session_id(probe_response.session_id)
        assert probe_response.response.scenario_id in self._known_scenario_ids, "Unknown scenario"
        assert probe_response.response.probe_id in self._known_probe_ids[probe_response.response.scenario_id], "Unknown probe"
        if probe_response.session_id not in self._probe_responses_by_session:
            self._probe_responses_by_session[probe_response.session_id] = [probe_response.response]
        else:
            self._probe_responses_by_session[probe_response.session_id].append(probe_response.response)
        if self._autosave:
            self.save_state()
        return
    
    def add_probe_responses(self, probe_responses: ProbeResponseBatch) -> None:
        """Take note of new responses to some number of probes."""
        probe_responses.session_id = self._handle_session_id(probe_responses.session_id)
        if probe_responses.session_id not in self._probe_responses_by_session:
            self._probe_responses_by_session[probe_responses.session_id] = []
        for response in probe_responses.responses:
            assert response.scenario_id in self._known_scenario_ids
            assert response.probe_id in self._known_probe_ids[response.scenario_id]
            self._probe_responses_by_session[probe_responses.session_id].append(response)
        if self._autosave:
            self.save_state()
        return
    
    def get_alignment_result_probe(self, session_id: str, target_id: str, scenario_id: str, probe_id: str, population: bool = False) -> AlignmentResults:
        """Calculates an alignment for a single response previously given.

        :param session_id: The session that holds the response.
        :param target_id: The target we are aligning to.
        :param scenario_id: The scenario that is being processed.
        :param probe_id: The question that the response was to.
        :param population: If False, will treat target as set of attribute values. If True, will treat target as 
          attribute values for an entire population.
        :return: Info about the calculated attribute values and alignment score.
        """
        session_id = self._handle_session_id(session_id)
        responses: list[Response] | None = copy.deepcopy(self._probe_responses_by_session.get(session_id, None))
        if responses is None:
            logging.error("Requested alignment but no responses have been sent for session %s.", session_id)
            return AlignmentResults()
        responses = alignment_score.handle_apply_gauze(responses=responses)
        responses = alignment_score.handle_give_blood(responses=responses)

        # Extract information about responses
        kdma_values: list[KDMA] | None = None
        for response in responses:
            if response.scenario_id == scenario_id and response.probe_id == probe_id:
                if kdma_values is not None:
                    logging.warning("Multiple responses found for this session (%s), scenario (%s), and probe (%s) combo.  Using latest.", session_id, response.scenario_id, response.probe_id)
                kdma_values = []
                # Extract the KDMA values and adjust them per the "expectation" design
                chosen_values = scenario_parsing.get_kdma_values_for_response(scenario_id=response.scenario_id, probe_id=response.probe_id, choice=response.choice)
                if chosen_values is None:
                    continue
                possible_values = scenario_parsing.get_kmda_values_for_probe(scenario_id=response.scenario_id, probe_id=response.probe_id)
                for kdma_name, kdma_value in chosen_values.items():
                    adjusted_value = attribute_values.get_expected_value(chosen=kdma_value, possible=possible_values[kdma_name])
                    kdma_values.append(KDMA(kdma=kdma_name, value=adjusted_value))

        if kdma_values is None:
            logging.error("Requested alignment but no responses for probe %s (scenario %s) have been received in session %s.", probe_id, scenario_id, session_id)
            return AlignmentResults()
        
        if population:
            score = alignment_score.calculate_alignment_to_population(actual_attribute_values=kdma_values, alignment_target_distribution_id=target_id)
        else:
            score = alignment_score.calculate_alignment(actual_attribute_values=kdma_values, alignment_target_id=target_id)
        
        source = AlignmentSource(scenario_id=scenario_id, probes=[probe_id])
        result = AlignmentResults(alignment_source=[source],
                                  score=score)
        return result
    
    def get_alignment_result_session(self, session_id: str, target_id: str, population: bool = False) -> AlignmentResults:
        """Calculates an alignment for a collection of responses previously given against an alignment target.

        :param session_id: The session that holds the responses.
        :param target_id: The target we are aligning to.
        :param population: If False, will treat target as set of attribute values. If True, will treat target as 
          attribute values for an entire population.
        :return: Info about the calculated alignment score.
        """
        session_id = self._handle_session_id(session_id)

        kdma_values, sources = self.get_kdma_profile(session_id=session_id)
        
        if population:
            score = alignment_score.calculate_alignment_to_population(actual_attribute_values=kdma_values, alignment_target_distribution_id=target_id)
        else:
            score = alignment_score.calculate_alignment(actual_attribute_values=kdma_values, alignment_target_id=target_id)

        result = AlignmentResults(alignment_source=sources,
                                  score=score)
        return result
    
    def get_alignment_sessions(self, session_id1: str, session_id2: str, kdma_filter: KDMAName = None) -> AlignmentResults:
        """Calculates an alignment for a collection of responses previously given against another collection of responses previously given.

        :param session_id1: The session that holds the responses.
        :param session_id2: The session that holds the responses.
        :param kdma_filter: Calculate alignment results using only this KDMA.  If None, the alignment will be calculated over the intersection of KDMAs present in the sessions.
        :return: Info about the calculated alignment score.
        """
        kdma_values1, sources1 = self.get_kdma_profile(session_id=session_id1)
        kdma_values2, sources2 = self.get_kdma_profile(session_id=session_id2)
        if kdma_filter is not None:
            kdma_values1 = [kdma_value for kdma_value in kdma_values1 if kdma_value.kdma == kdma_filter]
            kdma_values2 = [kdma_value for kdma_value in kdma_values2 if kdma_value.kdma == kdma_filter]
        score = alignment_score.calculate_alignment_two_kdmas(attribute_values1=kdma_values1, attribute_values2=kdma_values2)
        result = AlignmentResults(alignment_source=sources1 + sources2,
                                  score=score)
        return result

    def get_alignment_sessions_population(self, session_id1_or_target_id: str, session_id2_or_target_id: str, target_pop_id: str, kdma_filter: KDMAName = None) -> AlignmentResults:
        """Calculates an alignment for a collection of responses previously given against another collection of responses previously given within
        the context of an expert population.

        :param session_id1_or_target_id: Either (a) the session that holds the responses or (b) a single-value target ID.
        :param session_id2_or_target_id: Either (a) the session that holds the responses or (b) a single-value target ID.
        :param target_pop_id: ID of the target population.
        :param kdma_filter: Calculate alignment results using only this KDMA.  If None, the alignment will be calculated over the intersection of KDMAs present in the sessions.
        :return: Info about the calculated alignment score.
        """
        # Can compute alignment between two session IDs, a session ID and a single-value KDMA target, or two KDMA targets
        if session_id1_or_target_id in self._known_target_ids:
            at = data_loader.load_object(model_type=AlignmentTarget, identity=session_id1_or_target_id)
            kdma_values1 = at.kdma_values
            sources1 = [] # no sources if KDMA value coming from alignment target instead of a set of responses
        else:
            kdma_values1, sources1 = self.get_kdma_profile(session_id=session_id1_or_target_id)
        if session_id2_or_target_id in self._known_target_ids:
            at = data_loader.load_object(model_type=AlignmentTarget, identity=session_id2_or_target_id)
            kdma_values2 = at.kdma_values
            sources2 = [] # no sources if KDMA value coming from alignment target instead of a set of responses
        else:
            kdma_values2, sources2 = self.get_kdma_profile(session_id=session_id2_or_target_id)
        if kdma_filter is not None:
            kdma_values1 = [kdma_value for kdma_value in kdma_values1 if kdma_value.kdma == kdma_filter]
            kdma_values2 = [kdma_value for kdma_value in kdma_values2 if kdma_value.kdma == kdma_filter]
        score = alignment_score.calculate_alignment_two_kdmas_pop(attribute_values1=kdma_values1, attribute_values2=kdma_values2, target_id=target_pop_id)
        result = AlignmentResults(alignment_source=sources1 + sources2,
                                  score=score)
        return result

    def get_kdma_profile(self, session_id: str) -> tuple[list[KDMA], list[AlignmentSource]]:
        """
        :param session_id: The session being profiled.
        :return: (The KDMA values of the sessions, the scenarios and probes used to make the calculation)
        """
        responses: list[Response] | None = copy.deepcopy(self._probe_responses_by_session.get(session_id, None))
        if responses is None:
            logging.warning("Requested profile, but no responses have been sent for session %s", session_id)
        responses = alignment_score.handle_apply_gauze(responses=responses)
        responses = alignment_score.handle_give_blood(responses=responses)
        # Extract information about responses
        all_kdma_values = defaultdict(list[list[KDMA]])  # holds all the kdma values resulting from choices
        scenario_and_probes = defaultdict(list)  # for each scenario used in the session, records all probes responded to
        for response in responses:
            scenario_and_probes[response.scenario_id].append(response.probe_id)  # take note of probe (needed for AlignmentSource)
            # Extract the KDMA values and adjust them per the "expectation" design
            chosen_values = scenario_parsing.get_kdma_values_for_response(scenario_id=response.scenario_id, probe_id=response.probe_id, choice=response.choice)
            if chosen_values is None:
                continue
            possible_values = scenario_parsing.get_kmda_values_for_probe(scenario_id=response.scenario_id, probe_id=response.probe_id)
            kdma_values_for_this_response: list[KDMA] = []
            for kdma_name, kdma_value in chosen_values.items():
                adjusted_value = attribute_values.get_expected_value(chosen=kdma_value, possible=possible_values[kdma_name])
                kdma_values_for_this_response.append(KDMA(kdma=kdma_name, value=adjusted_value))
            all_kdma_values[response.scenario_id].append(kdma_values_for_this_response)

        cross_scenario_kdmas: list[list[KDMA]] = [] # concatenation of all KDMA values for all scenarios
        for scenario, kdmas in all_kdma_values.items():
            kdma_values = attribute_values.merge_attribute_values(kdma_results=kdmas)
            cross_scenario_kdmas.extend(kdma_values)

        # Get average of KDMAs if they appear multiple times
        sums_counts = defaultdict(lambda: [0, 0])  # Dictionary to store [sum, count] for each string
        for kdma_obj in cross_scenario_kdmas:
            kdma = kdma_obj.kdma
            value = kdma_obj.value
            sums_counts[kdma][0] += value  # Accumulate sum
            sums_counts[kdma][1] += 1      # Increment count
        averaged_cross_scenario_kdmas = [KDMA(kdma=name, value=(total / count)) for name, (total, count) in sums_counts.items()]
        sources = [AlignmentSource(scenario_id=scenario_id, probes=list(probe_ids)) for scenario_id, probe_ids in scenario_and_probes.items()]
        return averaged_cross_scenario_kdmas, sources

    
    # Note: This function is deprecated in favor of get_ordered_alignment_and_scores
    def get_least_most_aligned_targets(self, session_id: str) -> dict[str, str]:
        alignments = [
            (target_id, self.get_alignment_result_session(session_id=session_id, target_id=target_id, population=False).score)
            for target_id in self._known_target_ids
        ]

        valid_alignments = [(target_id, score) for target_id, score in alignments if math.isfinite(score)]  # Filter out NaN values for finding min and max
        if not valid_alignments:
            logging.warning("Couldn't properly find the least and most aligned.")
            return {"most_aligned": None, "least_aligned": None}

        low_id, _ = min(valid_alignments, key=lambda x: x[1])
        high_id, _ = max(valid_alignments, key=lambda x: x[1])

        return {"most_aligned": high_id, "least_aligned": low_id}

    @staticmethod
    def _alignmenttarget_id_to_alignmenttargetdistribution_id(target_id: str) -> str:
        """
        :param target_id: The AlignmentTarget being used.
        :return: The AlignmentTargetDistribution (i.e. population) associated with the AlignmentTarget.
        """
        if KDMAName.INGROUP_BIAS in target_id:
            return "ADEPT-DryRun-Ingroup Bias-Population-All"
        if KDMAName.MORAL_JUDGEMENT in target_id:
            return "ADEPT-DryRun-Moral judgement-Population-All"
        raise ValueError(f"Don't know of an AlignmentTargetDistribution for {target_id}")

    def get_ordered_alignment_and_scores(self, session_id: str, kdma_id: str) -> list[dict[str, str]]:
        """
        :param session_id: The session to get the alignment of.
        :param kdma_id: The name of the KDMA that we are taking alignment of (currently only 1D alignment is supported so this is required).
        :return: A list with an entry for each target.
          The order of the entries is in decreasing order of alignment score.
          Each entry consits of a dict, where the key is the alignment target name and the value is the alignment score.
        """
        target_ids = self._known_target_ids
        filtered_target_ids = [t for t in target_ids if kdma_id in [k.kdma for k in data_loader.load_object(model_type=AlignmentTarget, identity=t).kdma_values]]

        alignments = [
            (target_id, self.get_alignment_sessions_population(session_id1_or_target_id=session_id, session_id2_or_target_id=target_id, target_pop_id=ServerState._alignmenttarget_id_to_alignmenttargetdistribution_id(target_id), kdma_filter=kdma_id).score)
            for target_id in filtered_target_ids
        ]

        valid_alignments = [{target_id: score} for target_id, score in alignments if math.isfinite(score)]  # Filter out NaN values for finding min and max
        if not valid_alignments:
            logging.warning("Couldn't find any valid alignments.")
            return [None]

        sorted_alignments = sorted(valid_alignments, key=lambda x: list(x.values())[0], reverse=True) # sort dictionary in descending order by score

        return sorted_alignments
    
    _SAVE_DIR: typing.Final[str] = "saved_state/"
    _CURRENT_SESSION_SAVE_NAME: typing.Final[str] = _SAVE_DIR + "current_session_id.pkl"
    _ALL_SESSIONS_SAVE_NAME: typing.Final[str] = _SAVE_DIR + "session_ids.pkl"
    _RESPONSES_SAVE_NAME: typing.Final[str] = _SAVE_DIR + "probe_responses_by_session.pkl"
    _AUTOSAVE_SAVE_NAME: typing.Final[str] = _SAVE_DIR + "autosave.pkl"
    
    def save_state(self) -> None:
        """Saves (some of) the current server state."""
        SaveHelper.to_pickle(obj=self._current_session_id, path=ServerState._CURRENT_SESSION_SAVE_NAME)
        SaveHelper.to_pickle(obj=self._session_ids, path=ServerState._ALL_SESSIONS_SAVE_NAME)
        SaveHelper.to_pickle(obj=self._probe_responses_by_session, path=ServerState._RESPONSES_SAVE_NAME)
        SaveHelper.to_pickle(obj=self._autosave, path=ServerState._AUTOSAVE_SAVE_NAME)
        return None
    
    def load_state(self) -> int:
        """
        Loads (some of) the server state from the previously persisted files.

        :return 200 on success loaded state.  204 if no state to load.  500 on error.
        """
        current_session_id = SaveHelper.from_pickle(path=ServerState._CURRENT_SESSION_SAVE_NAME)
        session_ids = SaveHelper.from_pickle(path=ServerState._ALL_SESSIONS_SAVE_NAME)
        probe_responses_by_session = SaveHelper.from_pickle(path=ServerState._RESPONSES_SAVE_NAME)
        autosave = SaveHelper.from_pickle(path=ServerState._AUTOSAVE_SAVE_NAME)

        components = (session_ids, probe_responses_by_session, autosave)  # A current_session_id of None could be the actual session ID so we won't check for it
        if all(component is not None for component in components):
            self._current_session_id = current_session_id
            self._session_ids = session_ids
            self._probe_responses_by_session = probe_responses_by_session
            self._autosave = autosave
            logging.warning("Loaded previous ServerState.")  # Usally this is benign, but set to warning to increase visibility
            return 200
        if any(component is not None for component in components):
            logging.error("Failed to find all saved states of server, aborting load.")
            return 500
        logging.warning("No saved server state, state not updated.")  # Usally this is benign, but set to warning to increase visibility
        return 204
    
    def set_autosave(self, enabled: bool) -> None:
        self._autosave = enabled
        if enabled:
            self.save_state()
        return None
    
    @classmethod
    def get_cached_empty_serverstate(cls) -> "ServerState":
        """
        This is included mostly to speed up development so you don't have to generate a ServerState each time.
        Note, if you are making changes to the server, the cached copy might be obsolete, in which case delete the cache directory.
        
        :return: An empty ServerState.  Loads from cached example if available.
        """
        cache = "cache/empty_server_state.pkl"
        if (server_state := SaveHelper.from_pickle(path=cache)) is None:
            server_state = ServerState()
            SaveHelper.to_pickle(obj=server_state, path=cache)
        return server_state


def main() -> None:
    """Example code exercising functionality."""
    server = ServerState.get_cached_empty_serverstate()
#    session_id = server.generate_session_id()
#    scenario_id: typing.Final[str] = "DryRunEval.IO1"
#    target_id: typing.Final[str] = "ADEPT-DryRun-Ingroup Bias-0.9"
#
#    r1 = Response(scenario_id=scenario_id,
#                  probe_id="DryRunEval.IO1.1",
#                  choice="DryRunEval.IO1.1.A")
#    pr = ProbeResponse(session_id=session_id,
#                       response=r1)
#    server.add_probe_response(pr)
#
#    r2 = Response(scenario_id=scenario_id,
#                  probe_id="DryRunEval.IO1.2",
#                  choice="DryRunEval.IO1.2.A")
#    r3 = Response(scenario_id=scenario_id,
#                  probe_id="DryRunEval.IO1.3",
#                  choice="DryRunEval.IO1.3.A")
#    prb = ProbeResponseBatch(session_id=session_id,
#                             responses=[r2, r3])
#    server.add_probe_responses(probe_responses=prb)
#
#    ar1 = server.get_alignment_result_probe(session_id=session_id,
#                                            target_id=target_id,
#                                            scenario_id=scenario_id,
#                                            probe_id="DryRunEval.IO1.1",
#                                            population=False)
#    ar2 = server.get_alignment_result_probe(session_id=session_id,
#                                            target_id=target_id,
#                                            scenario_id=scenario_id,
#                                            probe_id="DryRunEval.IO1.2",
#                                            population=False)
#    ar3 = server.get_alignment_result_probe(session_id=session_id,
#                                            target_id=target_id,
#                                            scenario_id=scenario_id,
#                                            probe_id="DryRunEval.IO1.3",
#                                            population=False)
#
#    ar4 = server.get_alignment_result_session(session_id=session_id,
#                                              target_id=target_id,
#                                              population=False)
#
#    # New session, same results
#    session_idB = server.generate_session_id()
#    prbB = ProbeResponseBatch(session_id=session_idB,
#                              responses=[r1, r2, r3])
#    server.add_probe_responses(probe_responses=prbB)
#    s_kdma_profile = server.get_kdma_profile(session_id=session_idB)
#    ss_alignment = server.get_alignment_sessions(session_id1=session_id, session_id2=session_idB)
#    assert math.isclose(ss_alignment.score, 1.0, abs_tol=0.0001)
#
#    # New session, opposite results (I didn't actually check that B answer was opposite)
#    session_idC = server.generate_session_id()
#    r1C = Response(scenario_id=scenario_id,
#                   probe_id="DryRunEval.IO1.1",
#                   choice="DryRunEval.IO1.1.B")
#    r2C = Response(scenario_id=scenario_id,
#                   probe_id="DryRunEval.IO1.2",
#                   choice="DryRunEval.IO1.2.B")
#    r3C = Response(scenario_id=scenario_id,
#                   probe_id="DryRunEval.IO1.3",
#                   choice="DryRunEval.IO1.3.B")
#    prbC = ProbeResponseBatch(session_id=session_idC,
#                              responses=[r1C, r2C, r3C])
#    server.add_probe_responses(probe_responses=prbC)
#
#    ssC_alignment = server.get_alignment_sessions(session_id1=session_id, session_id2=session_idC)
#    assert ssC_alignment.score < ss_alignment.score
#
##    m_l_at = server.get_least_most_aligned_targets(session_id=session_id)
#
#    # Test scoring responses from multiple scenarios and multiple KDMAs
#    session_idD = server.generate_session_id()
#    scenario_id_2: typing.Final[str] = "DryRunEval.MJ1"
#    r1D = Response(scenario_id=scenario_id_2,
#                  probe_id="DryRunEval.MJ1.1",
#                  choice="DryRunEval.MJ1.1.B")
#    r2D = Response(scenario_id=scenario_id_2,
#                  probe_id="DryRunEval.MJ1.2",
#                  choice="DryRunEval.MJ1.2.B")
#    r3D = Response(scenario_id=scenario_id_2,
#                  probe_id="DryRunEval.MJ1.3",
#                  choice="DryRunEval.MJ1.3.B")
#
#    scenario_id_3: typing.Final[str] = "DryRunEval-MJ2-eval"
#    r4D = Response(scenario_id=scenario_id_3,
#                  probe_id="Probe 2",
#                  choice="Response 2A")
#    r5D = Response(scenario_id=scenario_id_3,
#                  probe_id="Probe 2A-1",
#                  choice="Response 2A-1B")
#    r6D = Response(scenario_id=scenario_id_3,
#                  probe_id="Probe 3-B.2",
#                  choice="Response 3-B.2-B-gauze-v")
#    r7D = Response(scenario_id=scenario_id_3,
#                  probe_id="Probe 4",
#                  choice="Response 4-A")
#
#    prbD = ProbeResponseBatch(session_id=session_idD,
#                             responses=[r1, r2, r3, r1D, r2D, r3D, r4D, r5D, r6D, r7D])
#    server.add_probe_responses(probe_responses=prbD)
#
#    prbD_kdma_profile = server.get_kdma_profile(session_id=session_idD)
##    print("KDMA profile: ", prbD_kdma_profile)
#
#    ord_atD_ingroup = server.get_ordered_alignment_and_scores(session_id=session_idD, kdma_id="Ingroup Bias")
#    ord_atD_mj = server.get_ordered_alignment_and_scores(session_id=session_idD, kdma_id="Moral judgement")
#
##    print("ingroup: ", ord_atD_ingroup)
##    print("mj: ", ord_atD_mj)
#

    # Sample responses for alignment scoring (copied from Kitware baseline MJ low)
    session_id = server.generate_session_id()
    scenario_id: typing.Final[str] = "DryRunEval-MJ2-eval"
    target_id: typing.Final[str] = "ADEPT-DryRun-Moral judgement-Group-All"
    pop_target_id: typing.Final[str] = "ADEPT-DryRun-Moral judgement-Population-All"

    r1 = Response(scenario_id=scenario_id,
                  probe_id="Probe 2",
                  choice="Response 2B")
    r2 = Response(scenario_id=scenario_id,
                  probe_id="Probe 2B-1",
                  choice="Response 2B-1B")
    r3 = Response(scenario_id=scenario_id,
                  probe_id="Probe 3-B.2",
                  choice="Response 3-B.2-B-gauze-v")
    r4 = Response(scenario_id=scenario_id,
                  probe_id="Probe 3-B.2",
                  choice="Response 3-B.2-B-gauze-v")
    r5 = Response(scenario_id=scenario_id,
                  probe_id="Probe 3-B.2",
                  choice="Response 3-B.2-A-gauze-s")
    r6 = Response(scenario_id=scenario_id,
                  probe_id="Probe 3-B.2",
                  choice="Response 3-B.2-B-gauze-v")
    r7 = Response(scenario_id=scenario_id,
                  probe_id="Probe 3-B.2",
                  choice="Response 3-B.2-B-gauze-v")
    r8 = Response(scenario_id=scenario_id,
                  probe_id="Probe 3-B.2",
                  choice="Response 3-B.2-C")
    r9 = Response(scenario_id=scenario_id,
                  probe_id="Probe 4",
                  choice="Response 4-B")
    r9b = Response(scenario_id=scenario_id,
                  probe_id="Probe 4-B.1",
                  choice="Response 4-B.1-A")
    r10 = Response(scenario_id=scenario_id,
                  probe_id="Probe 5",
                  choice="Response 5-B")
    r11 = Response(scenario_id=scenario_id,
                  probe_id="Probe 5-B.1",
                  choice="Response 5-B.1-B")
    r12 = Response(scenario_id=scenario_id,
                  probe_id="Probe 6",
                  choice="Response 6-B")
    r13 = Response(scenario_id=scenario_id,
                  probe_id="Probe 7",
                  choice="Response 7-B")
    r14 = Response(scenario_id=scenario_id,
                  probe_id="Probe 8",
                  choice="Response 8-B")
    r15 = Response(scenario_id=scenario_id,
                  probe_id="Probe 9",
                  choice="Response 9-A")
    r16 = Response(scenario_id=scenario_id,
                  probe_id="Probe 9-A.1",
                  choice="Response 9-A.1-A")
    r17 = Response(scenario_id=scenario_id,
                  probe_id="Probe 10",
                  choice="Response 10-A")

    prb = ProbeResponseBatch(session_id=session_id,
                             responses=[r1, r2, r3, r4, r5, r6, r7, r8,
                                 r9, r9b, r10, r11, r12, r13, r14,
                                 r15, r16, r17])
    server.add_probe_responses(probe_responses=prb)

    # Kitware aligned MJ low
    session_id_2 = server.generate_session_id()

    r1 = Response(scenario_id=scenario_id,
                  probe_id="Probe 2",
                  choice="Response 2A")
    r2 = Response(scenario_id=scenario_id,
                  probe_id="Probe 2A-1",
                  choice="Response 2A-1A")
    r3 = Response(scenario_id=scenario_id,
                  probe_id="Probe 3-B.2",
                  choice="Response 3-B.2-A-gauze-s")
    r4 = Response(scenario_id=scenario_id,
                  probe_id="Probe 3-B.2",
                  choice="Response 3-B.2-A-gauze-s")
    r5 = Response(scenario_id=scenario_id,
                  probe_id="Probe 3-B.2",
                  choice="Response 3-B.2-A-gauze-s")
    r8 = Response(scenario_id=scenario_id,
                  probe_id="Probe 3-B.2",
                  choice="Response 3-B.2-C")
    r9 = Response(scenario_id=scenario_id,
                  probe_id="Probe 4",
                  choice="Response 4-B")
    r9b = Response(scenario_id=scenario_id,
                  probe_id="Probe 4-B.1",
                  choice="Response 4-B.1-B")
    r9bb = Response(scenario_id=scenario_id,
                  probe_id="Probe 4-B.1-B.1",
                  choice="Response 4-B.1-B.1-B")
    r10 = Response(scenario_id=scenario_id,
                  probe_id="Probe 5",
                  choice="Response 5-A")
    r11 = Response(scenario_id=scenario_id,
                  probe_id="Probe 5-A.1",
                  choice="Response 5-A.1-A")
    r12 = Response(scenario_id=scenario_id,
                  probe_id="Probe 6",
                  choice="Response 6-A")
    r13 = Response(scenario_id=scenario_id,
                  probe_id="Probe 7",
                  choice="Response 7-C")
    r14 = Response(scenario_id=scenario_id,
                  probe_id="Probe 8",
                  choice="Response 8-A")
    r15 = Response(scenario_id=scenario_id,
                  probe_id="Probe 9",
                  choice="Response 9-A")
    r16 = Response(scenario_id=scenario_id,
                  probe_id="Probe 9-A.1",
                  choice="Response 9-A.1-A")
    r17 = Response(scenario_id=scenario_id,
                  probe_id="Probe 10",
                  choice="Response 10-B")

    prb_2 = ProbeResponseBatch(session_id=session_id_2,
                             responses=[r1, r2, r3, r4, r5, r8, r9,
                                 r9b, r9bb, r10, r11, r12, r13, r14,
                                 r15, r16, r17])
    server.add_probe_responses(probe_responses=prb_2)


    kdma_profile = server.get_kdma_profile(session_id=session_id)
    print("kdma #1: ", kdma_profile)
    ar = server.get_alignment_result_session(session_id=session_id,
                                             target_id=target_id,
                                             population=False)
    print("alignment #1: ", ar)

    ar_pop = server.get_alignment_result_session(session_id=session_id,
                                                 target_id=pop_target_id,
                                                 population=True)
    print("pop alignment #1: ", ar_pop)

    kdma_profile_2 = server.get_kdma_profile(session_id=session_id_2)
    print("kdma #2: ", kdma_profile_2)
    ar_2 = server.get_alignment_result_session(session_id=session_id_2,
                                              target_id=target_id,
                                              population=False)
    print("alignment #2: ", ar_2)

    ar_pop_2 = server.get_alignment_result_session(session_id=session_id_2,
                                                   target_id=pop_target_id,
                                                   population=True)
    print("pop alignment #2: ", ar_pop_2)

    # New alignment between sessions
#    alignment_btwn_profiles = server.get_alignment_sessions_population(session_id1=session_id, session_id2_or_target_id=session_id_2, target_pop_id=pop_target_id)
    alignment_btwn_profiles = server.get_alignment_sessions_population(session_id1_or_target_id=session_id, session_id2_or_target_id="ADEPT-DryRun-Moral judgement-0.8", target_pop_id=pop_target_id)
#    alignment_btwn_profiles = server.get_alignment_sessions_population(session_id1_or_target_id="ADEPT-DryRun-Moral judgement-0.6", session_id2_or_target_id="ADEPT-DryRun-Moral judgement-0.8", target_pop_id=pop_target_id)
    print("alignment between profiles: ", alignment_btwn_profiles)

    return

if __name__ == '__main__':
    main()
