import logging
import random
import typing

import openapi_server.data_loader
import openapi_server.server_state
from openapi_server.models.action_mapping import ActionMapping
from openapi_server.models.conditions import Conditions
from openapi_server.models.probe_response import ProbeResponse
from openapi_server.models.response import Response
from openapi_server.models.semantic_type_enum import SemanticTypeEnum
from openapi_server.models.scenario import Scenario
from openapi_server.models.scene import Scene

def random_walk_scenario(scenario: typing.Union[Scenario, str]) -> list[tuple[str, str]]:
    
    def action_meets_conditions(action: ActionMapping, state: dict[str, object]) -> bool:
        conditions: Conditions = action.action_conditions
        semantics: SemanticTypeEnum = action.action_condition_semantics if hasattr(action, 'action_condition_semantics') else SemanticTypeEnum.AND
        if not conditions:
            return True
        return evaluate_conditions(conditions, semantics, state)

    def scene_meets_conditions(scene: Scene, state: dict[str, object]) -> bool:
        semantics = scene.transition_semantics
        conditions = scene.transitions
        return evaluate_conditions(conditions, semantics, state)

    def evaluate_conditions(conditions: Conditions | None, semantics: SemanticTypeEnum, state: dict[str, object]) -> bool:
        if conditions is None:
            return True
        condition_checks = []
        if conditions.elapsed_time_lt is not None:
            condition_checks.append(state['elapsed_time'] < conditions.elapsed_time_lt)
        if conditions.elapsed_time_gt is not None:
            condition_checks.append(state['elapsed_time'] > conditions.elapsed_time_gt)
        if conditions.actions:
            condition_checks.append(
                any(all(act in state['actions_taken'] for act in action_list) for action_list in conditions.actions)
            )
        if conditions.probes:
            condition_checks.append(
                all(probe in state['probes_answered'] for probe in conditions.probes)
            )
        if conditions.probe_responses:
            condition_checks.append(
                all(response in state['probe_responses'] for response in conditions.probe_responses)
            )
        if conditions.character_vitals:
            for vital_condition in conditions.character_vitals:
                character_id = vital_condition.character_id
                condition_checks.append(
                    all(vital in state['character_vitals'].get(character_id, {}) for vital in vital_condition.vitals)
                )
        if conditions.supplies:
            for supply_condition in conditions.supplies:
                supply_id = supply_condition.type
                condition_checks.append(
                    state['supplies'].get(supply_id, 0) <= supply_condition.quantity
                )
        
        if semantics == SemanticTypeEnum.AND:
            return all(condition_checks)
        elif semantics == SemanticTypeEnum.OR:
            return any(condition_checks)
        elif semantics == SemanticTypeEnum.NOT:
            assert len(condition_checks) == 1
            return not condition_checks[0]
        else:
            msg = f"Unknown semantics: {semantics}"
            logging.error(msg)
            raise ValueError(msg)
    
    if isinstance(scenario, str):
        scenario: Scenario = openapi_server.data_loader.load_object(Scenario, scenario)

    current_scene_id: str = scenario.first_scene
    walk_log: list[tuple[str, str]] = []
    state = {
        'elapsed_time': 0,  # Initialize elapsed time
        'actions_taken': [],
        'probes_answered': [],
        'probe_responses': [],
        'character_vitals': {},
        'supplies': {}
    }
    
    while True:
        current_scene: Scene = next((scene for scene in scenario.scenes if scene.id == current_scene_id), None)
        if not current_scene:
            logging.error("Couldn't find scene %s", current_scene_id)
            break
        
        valid_actions: list[ActionMapping] = [action for action in current_scene.action_mapping if action_meets_conditions(action, state)]
        if not valid_actions:
            logging.error("No legal action found, %s, %s", current_scene_id, state)
            break
        
        action: ActionMapping = random.choice(valid_actions)
        walk_log.append((action.probe_id, action.choice))
        
        # Update state based on action taken
        state['actions_taken'].append(action.action_id)
        state['probes_answered'].append(action.probe_id)
        state['probe_responses'].append(action.choice)
        
        # Assuming action has attributes that affect the state, like affecting supplies or character vitals
        # Update state['supplies'] and state['character_vitals'] accordingly
        
        if scene_meets_conditions(current_scene, state):
            if action.next_scene is not None:
                current_scene_id = action.next_scene
            elif current_scene.next_scene is not None:
                current_scene_id = current_scene.next_scene
            else:
                break  # If transitions are met but there is no next scene, then we are done with the scenario
    
    return walk_log


def just_some_choices() -> None:
    """A hard coded set of choices."""
    ss = openapi_server.server_state.ServerState()
    session_id = ss.generate_session_id()
    scenario_id = "DryRunEval-MJ2-eval"
    target_id = "ADEPT-DryRun-Moral judgement-0.2"
    r = Response(scenario_id=scenario_id,
                 probe_id="n/a",
                 choice="n/a",
                 justification="")
    pr = ProbeResponse(session_id=session_id,
                       response=r)
    ss.add_probe_response(probe_response=pr)
    ar1 = ss.get_alignment_result_probe(session_id=session_id,
                                        target_id=target_id,
                                        scenario_id=scenario_id,
                                        probe_id="n/a",
                                        population=False)
    ar2 = ss.get_alignment_result_session(session_id=session_id,
                                          target_id=target_id,
                                          population=False)
    
    probe_id = "Probe 3-B.2"
    r = Response(scenario_id=scenario_id,
                 probe_id=probe_id,
                 choice="Response Probe 3-B.2-B",
                 justification="")
    pr = ProbeResponse(session_id=session_id,
                       response=r)
    ss.add_probe_response(probe_response=pr)
    ar3 = ss.get_alignment_result_probe(session_id=session_id,
                                        target_id=target_id,
                                        scenario_id=scenario_id,
                                        probe_id="n/a",
                                        population=False)
    ar4 = ss.get_alignment_result_probe(session_id=session_id,
                                        target_id=target_id,
                                        scenario_id=scenario_id,
                                        probe_id=probe_id,
                                        population=False)
    ar5 = ss.get_alignment_result_session(session_id=session_id,
                                          target_id=target_id,
                                          population=False)
    return None


def main() -> None:
    random_walk_scenario(scenario="DryRunEval-MJ2-eval")
    return None


if __name__ == "__main__":
    main()
