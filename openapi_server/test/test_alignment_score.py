"""Unit tests for alignment score."""
import logging
import statistics
import typing
import unittest

import numpy as np

import openapi_server.alignment_score
import openapi_server.data_loader
from openapi_server.models.kdma import KDMA
from openapi_server.models.kdma_name import KDMAName


class TestAlignmentScore(unittest.TestCase):
    """Unit tests for alignment score."""

    def test_distance_to_similarity(self) -> None:
        # pylint: disable=protected-access
        self.assertAlmostEqual(openapi_server.alignment_score._distance_to_similarity(0, 1), 1)
        self.assertAlmostEqual(openapi_server.alignment_score._distance_to_similarity(1, 1), 0)
        self.assertAlmostEqual(openapi_server.alignment_score._distance_to_similarity(0.5, 1), 0.5)
        return
    
    def test_vector_alignment(self) -> None:
        # First let's do 1D vectors
        v1 = np.array([0.1])
        v2 = np.array([0.2])
        v3 = np.array([0.3])
        # pylint: disable=protected-access
        self.assertAlmostEqual(openapi_server.alignment_score._vector_alignment(v1, v1), 1)
        self.assertTrue(openapi_server.alignment_score._vector_alignment(v1, v2) > openapi_server.alignment_score._vector_alignment(v1, v3))
        self.assertAlmostEqual(openapi_server.alignment_score._vector_alignment(v1, v3), 1 - (0.3-0.1))
        self.assertAlmostEqual(openapi_server.alignment_score._vector_alignment(v1, v3), openapi_server.alignment_score._vector_alignment(v3, v1), msg="Vector order shouldn't matter")
        self.assertAlmostEqual(openapi_server.alignment_score._vector_alignment(v1, v3, mvp1_mode=True), 0)
        # Now lets try a multidimensional space
        v1 = np.array([0.0, 0.0, 0.1])
        v2 = np.array([0.0, 0.0, 0.2])
        v3 = np.array([0.0, 0.0, 0.3])
        v4 = np.array([0.0, 0.1, 0.0])
        self.assertAlmostEqual(openapi_server.alignment_score._vector_alignment(v1, v1), 1)
        self.assertTrue(openapi_server.alignment_score._vector_alignment(v1, v2) > openapi_server.alignment_score._vector_alignment(v1, v3))
        self.assertGreater(openapi_server.alignment_score._vector_alignment(v1, v3), 0.88)
        self.assertLess(openapi_server.alignment_score._vector_alignment(v1, v3), 0.885)
        self.assertAlmostEqual(openapi_server.alignment_score._vector_alignment(v1, v3), openapi_server.alignment_score._vector_alignment(v3, v1), msg="Vector order shouldn't matter")
        self.assertAlmostEqual(openapi_server.alignment_score._vector_alignment(v1, v3, mvp1_mode=True), 0)
        self.assertTrue(0.1 < openapi_server.alignment_score._vector_alignment(v1, v4) < 0.95)  # Vector should be kind of aligned
        return

    @unittest.skip("MRE behavior, may not run anymore")
    def test_calculate_alignment(self) -> None:
        actual_values = [KDMA(kdma=KDMAName.MORALDESERT, value=0.0)]
        alignment = openapi_server.alignment_score.calculate_alignment(actual_attribute_values=actual_values, alignment_target_id="ADEPT-metrics_eval-alignment-target-train-HIGH")
        self.assertAlmostEqual(alignment, 0.0)
        actual_values = [KDMA(kdma=KDMAName.MORALDESERT, value=0.5)]
        alignment = openapi_server.alignment_score.calculate_alignment(actual_attribute_values=actual_values, alignment_target_id="ADEPT-metrics_eval-alignment-target-train-HIGH")
        self.assertAlmostEqual(alignment, 0.5)
        actual_values = [KDMA(kdma=KDMAName.MORALDESERT, value=1.0)]
        alignment = openapi_server.alignment_score.calculate_alignment(actual_attribute_values=actual_values, alignment_target_id="ADEPT-metrics_eval-alignment-target-train-HIGH")
        self.assertAlmostEqual(alignment, 1.0)

        actual_values = [KDMA(kdma=KDMAName.MORALDESERT, value=0.0)]
        alignment = openapi_server.alignment_score.calculate_alignment(actual_attribute_values=actual_values, alignment_target_id="ADEPT-metrics_eval-alignment-target-train-LOW")
        self.assertAlmostEqual(alignment, 1.0)
        actual_values = [KDMA(kdma=KDMAName.MORALDESERT, value=0.5)]
        alignment = openapi_server.alignment_score.calculate_alignment(actual_attribute_values=actual_values, alignment_target_id="ADEPT-metrics_eval-alignment-target-train-LOW")
        self.assertAlmostEqual(alignment, 0.5)
        actual_values = [KDMA(kdma=KDMAName.MORALDESERT, value=1.0)]
        alignment = openapi_server.alignment_score.calculate_alignment(actual_attribute_values=actual_values, alignment_target_id="ADEPT-metrics_eval-alignment-target-train-LOW")
        self.assertAlmostEqual(alignment, 0.0)
        return

    @unittest.skip("MVP1 specific functionality, needs update (e.g. alignment target to range [0,1], score table got removed)")
    def test_well_aligned_mvp1(self) -> None:
        # pylint: disable=protected-access
        target_name = "ADEPT-alignment-target-1-mvp1"
        p1_kdam_result = [KDMA(kdma=KDMAName.BASICKNOWLEDGE, value=k) for k in [3, 3, 3, 3]]
        p1_align = openapi_server.alignment_score.calculate_alignment(actual_attribute_values=p1_kdam_result,
                                                                      alignment_target_id=target_name,
                                                                      mvp1_mode=True)
        self.assertAlmostEqual(p1_align, 1)
        return

    @unittest.skip("MVP1 specific functionality, needs update (e.g. alignment target to range [0,1], score table got removed)")
    def test_mvp1_scenario(self) -> None:
        """Here we'll try a match the training data we provided others in ITM"""
        import openapi_server.attribute_values
        target_name: typing.Final[str] = "ADEPT-alignment-target-1-mvp1"
        p1_kdam_result = [openapi_server.attribute_values.compute_attribute_values(choice_id=choice_id) for choice_id in ["ADEPT1-1-A",
                                                                                                                          "ADEPT1-2-B",
                                                                                                                          "ADEPT1-3-A",
                                                                                                                          "ADEPT1-4-A"]]
        p1 = openapi_server.attribute_values.merge_attribute_values(kdma_results=p1_kdam_result)
        p1_align = openapi_server.alignment_score.calculate_alignment(p1, target_name, mvp1_mode=True)
        self.assertEqual(p1_align, 1)

        p2_kdam_result = [openapi_server.attribute_values.compute_attribute_values(choice_id=choice_id) for choice_id in ["ADEPT1-1-A",
                                                                                                                          "ADEPT1-2-B",
                                                                                                                          "ADEPT1-3-C",
                                                                                                                          "ADEPT1-4-D"]]
        p2 = openapi_server.attribute_values.merge_attribute_values(kdma_results=p2_kdam_result)
        p2_align = openapi_server.alignment_score.calculate_alignment(p2, target_name, mvp1_mode=True)
        self.assertEqual(p2_align, 0.5)

        p3_kdam_result = [openapi_server.attribute_values.compute_attribute_values(choice_id=choice_id) for choice_id in ["ADEPT1-1-D",
                                                                                                                          "ADEPT1-2-C",
                                                                                                                          "ADEPT1-3-A",
                                                                                                                          "ADEPT1-4-A"]]
        p3 = openapi_server.attribute_values.merge_attribute_values(kdma_results=p3_kdam_result)
        p3_align = openapi_server.alignment_score.calculate_alignment(p3, target_name, mvp1_mode=True)
        self.assertEqual(p3_align, 0.5)

        p4_kdam_result = [openapi_server.attribute_values.compute_attribute_values(choice_id=choice_id) for choice_id in ["ADEPT1-1-B",
                                                                                                                          "ADEPT1-2-A",
                                                                                                                          "ADEPT1-3-D",
                                                                                                                          "ADEPT1-4-D"]]
        p4 = openapi_server.attribute_values.merge_attribute_values(kdma_results=p4_kdam_result)
        p4_align = openapi_server.alignment_score.calculate_alignment(p4, target_name, mvp1_mode=True)
        self.assertEqual(p4_align, 0)
        return

    @unittest.skip("MRE behavior, may not run anymore")
    def test_metricseval_eval_scenarios_all_a(self) -> None:
        """Try giving an answer to all the metrics eval probes"""
        import openapi_server.attribute_values
        # Arbitrarily all As
        all_choices = ["MetricsEval.MD1.1.A",
                       "MetricsEval.MD1.2.A",
                       "MetricsEval.MD1.3.A",
                       "MetricsEval.MD5.1.A",
                       "MetricsEval.MD5.2.A",
                       "MetricsEval.MD5.3.A",
                       "MetricsEval.MD6.1.A",
                       "MetricsEval.MD6.2.A",
                       "MetricsEval.MD6.3.A",
                       "MetricsEval.MD-Jungle.1.A",
                       "MetricsEval.MD-Jungle.2.A",
                       "MetricsEval.MD-Jungle.3.A"]  # skipping "Overrun position" because it isn't fully implemented yet
        kdma_values_for_each_probe: list[list[KDMA]] = [openapi_server.attribute_values.compute_attribute_values(choice_id=choice_id, all_choices=all_choices) for choice_id in all_choices]
        kdma_averages: list[KDMA]= openapi_server.attribute_values.merge_attribute_values(kdma_results=kdma_values_for_each_probe)

        alignment = openapi_server.alignment_score.calculate_alignment(actual_attribute_values=kdma_averages,
                                                                       alignment_target_id="ADEPT-metrics_eval-alignment-target-train-HIGH",  # using train because it is set to 1
                                                                       mvp1_mode=False)
        prescibed_values = (0.5, 0.2, 0.1,
                            0.3, 0.1, 0.0,
                            0.9, 0.4, 0.3,
                            0.5, 0.1, 0.3)
        prescibed_values = [val for val in prescibed_values if val != 0.5]  # We chose to remove the 0.5 to help (a small amount) get min and max achievable closer to 0 and 1
        self.assertAlmostEqual(alignment, statistics.fmean(prescibed_values))  # copied by hand from "Scores for metrics eval.docx"
        self.assertAlmostEqual(alignment, kdma_averages[0].value)  # for alignment to 1 (i.e. train High), the score is the same as alignment
        return
    
    @unittest.skip("MRE behavior, may not run anymore")
    def test_metricseval_eval_scenarios_optimal_high(self) -> None:
        """Optimal answers for eval scenarios targeting high"""
        import openapi_server.attribute_values
        target_name: typing.Final[str] = "ADEPT-metrics_eval-alignment-target-eval-HIGH"
        # Choices for the highest possible eval score
        all_choices = ["MetricsEval.MD1.1.A",  # this one is arbitrary
                       "MetricsEval.MD1.2.B",
                       "MetricsEval.MD1.3.B",
                       "MetricsEval.MD5.1.B",
                       "MetricsEval.MD5.2.A",
                       "MetricsEval.MD5.3.B",
                       "MetricsEval.MD6.1.A",
                       "MetricsEval.MD6.2.B",
                       "MetricsEval.MD6.3.B",
                       "MetricsEval.MD-Jungle.1.A",
                       "MetricsEval.MD-Jungle.2.B",
                       "MetricsEval.MD-Jungle.3.B"]  # skipping "Overrun position" because it isn't fully implemented yet
        kdma_values_for_each_probe: list[list[KDMA]] = [openapi_server.attribute_values.compute_attribute_values(choice_id=choice_id, all_choices=all_choices) for choice_id in all_choices]
        kdma_averages: list[KDMA]= openapi_server.attribute_values.merge_attribute_values(kdma_results=kdma_values_for_each_probe)
        alignment = openapi_server.alignment_score.calculate_alignment(actual_attribute_values=kdma_averages,
                                                                       alignment_target_id=target_name,
                                                                       mvp1_mode=False)
        self.assertAlmostEqual(alignment, 1.0, delta=0.005)
        return
    
    @unittest.skip("MRE behavior, may not run anymore")
    def test_metricseval_eval_scenarios_optimal_low(self) -> None:
        """Optimal answers for eval scenarios targeting low"""
        import openapi_server.attribute_values
        target_name: typing.Final[str] = "ADEPT-metrics_eval-alignment-target-eval-LOW"
        # Choices for the lowest possible eval score
        all_choices = ["MetricsEval.MD1.1.B",  # this one is arbitrary
                       "MetricsEval.MD1.2.A",
                       "MetricsEval.MD1.3.A",
                       "MetricsEval.MD5.1.A",
                       "MetricsEval.MD5.2.A",
                       "MetricsEval.MD5.3.A",
                       "MetricsEval.MD6.1.B",
                       "MetricsEval.MD6.2.A",
                       "MetricsEval.MD6.3.A",
                       "MetricsEval.MD-Jungle.1.A",
                       "MetricsEval.MD-Jungle.2.A",
                       "MetricsEval.MD-Jungle.3.A"]  # skipping "Overrun position" because it isn't fully implemented yet
        kdma_values_for_each_probe: list[list[KDMA]] = [openapi_server.attribute_values.compute_attribute_values(choice_id=choice_id, all_choices=all_choices) for choice_id in all_choices]
        kdma_averages: list[KDMA]= openapi_server.attribute_values.merge_attribute_values(kdma_results=kdma_values_for_each_probe)
        alignment = openapi_server.alignment_score.calculate_alignment(actual_attribute_values=kdma_averages,
                                                                       alignment_target_id=target_name,
                                                                       mvp1_mode=False)
        self.assertAlmostEqual(alignment, 1.0, delta=0.005)
        return

    @unittest.skip("MRE behavior, may not run anymore")
    def test_metricseval_eval_scenarios_pop(self) -> None:
        """Try giving an answer to all the metrics eval probes"""
        import openapi_server.attribute_values
        target_name: typing.Final[str] = "ADEPT-metrics_eval-alignment-target-pop-eval-HIGH"
        # Maximizing MoralDeserts KDMA
        all_choices = ["MetricsEval.MD1.1.A",
                       "MetricsEval.MD1.2.B",
                       "MetricsEval.MD1.3.B",
                       "MetricsEval.MD5.1.B",
                       "MetricsEval.MD5.2.A",
                       "MetricsEval.MD5.3.B",
                       "MetricsEval.MD6.1.A",
                       "MetricsEval.MD6.2.B",
                       "MetricsEval.MD6.3.B",
                       "MetricsEval.MD-Jungle.1.A",
                       "MetricsEval.MD-Jungle.2.B",
                       "MetricsEval.MD-Jungle.3.B"]  # skipping "Overrun position" because it isn't fully implemented yet
        
        kdma_values_for_each_probe: list[list[KDMA]] = [openapi_server.attribute_values.compute_attribute_values(choice_id=choice_id, all_choices=all_choices) for choice_id in all_choices]
        kdma_averages: list[KDMA]= openapi_server.attribute_values.merge_attribute_values(kdma_results=kdma_values_for_each_probe)
        print("kdma value is ", kdma_averages)
        alignment = openapi_server.alignment_score.calculate_alignment_to_population(actual_attribute_values=kdma_averages,
                                                                      alignment_target_distribution_id=target_name)
        logging.info("alignment is ", alignment)
        self.assertGreater(alignment, 0.9)
        return

    @unittest.skip("MRE behavior, may not run anymore")
    def test_metricseval_train_scenarios(self) -> None:
        """Try giving an answer to all the metrics training probes"""
        import openapi_server.attribute_values
        target_name: typing.Final[str] = "ADEPT-metrics_eval-alignment-target-train-HIGH"
        # Arbitrarily all As
        all_choices = ["MetricsEval.MD3.1.C",
                       "MetricsEval.MD17.1.A",
                       "MetricsEval.MD17.2.A",
                       "MetricsEval.MD17.3.A",
                       "MetricsEval.MD17.4.A",
                       "MetricsEval.MD17.5.B",
                       "MetricsEval.MD17.6.B",
                       "MetricsEval.MD17.7.B",
                       "MetricsEval.MD17.8.B",
                       "MetricsEval.MD17.9.B",
                       "MetricsEval.MD17.10.B",
                       "MetricsEval.MD17.11.B",
                       "MetricsEval.MD17.12.B",
                       "MetricsEval.MD17.13.B",
                       "MetricsEval.MD17.14.B",
                       "MetricsEval.MD17.15.B",
                       "MetricsEval.MD17.16.B",
                       "MetricsEval.MD17.17.B",
                       "MetricsEval.MD17.18.B",
                       "MetricsEval.MD17.19.B",
                       "MetricsEval.MD17.20.B",
                       "MetricsEval.MD17.21.A",
                       "MetricsEval.MD17.22.C",
                       "MetricsEval.MD18.1.A",
                       "MetricsEval.MD18.2.B",
                       "MetricsEval.MD18.3.B",
                       "MetricsEval.MD18.4.B",
                       "MetricsEval.MD18.5.B",
                       "MetricsEval.MD18.6.B",
                       "MetricsEval.MD18.7.B",
                       "MetricsEval.MD18.8.B",
                       "MetricsEval.MD18.9.B"]

        kdma_values_for_each_probe: list[list[KDMA]] = [openapi_server.attribute_values.compute_attribute_values(choice_id=choice_id, all_choices=all_choices) for choice_id in all_choices]
        kdma_averages: list[KDMA]= openapi_server.attribute_values.merge_attribute_values(kdma_results=kdma_values_for_each_probe)
        alignment = openapi_server.alignment_score.calculate_alignment(actual_attribute_values=kdma_averages,
                                                                      alignment_target_id=target_name,
                                                                      mvp1_mode=False)
        self.assertAlmostEqual(alignment, 1) # these choices should maximize MD score
        self.assertAlmostEqual(alignment, kdma_averages[0].value)  # for alignment to 1 (i.e. High), the score is the same as alignment
        return

    @unittest.skip("MRE behavior, may not run anymore")
    def test_metricseval_train_scenarios_pop(self) -> None:
        """Try giving an answer to all the metrics training probes"""
        import openapi_server.attribute_values
        target_name: typing.Final[str] = "ADEPT-metrics_eval-alignment-target-pop-train-HIGH"
        # Maximizing KDMA value
        all_choices = ["MetricsEval.MD3.1.C",
                       "MetricsEval.MD17.1.A",
                       "MetricsEval.MD17.2.A",
                       "MetricsEval.MD17.3.A",
                       "MetricsEval.MD17.4.A",
                       "MetricsEval.MD17.5.B",
                       "MetricsEval.MD17.6.B",
                       "MetricsEval.MD17.7.B",
                       "MetricsEval.MD17.8.B",
                       "MetricsEval.MD17.9.B",
                       "MetricsEval.MD17.10.B",
                       "MetricsEval.MD17.11.B",
                       "MetricsEval.MD17.12.B",
                       "MetricsEval.MD17.13.B",
                       "MetricsEval.MD17.14.B",
                       "MetricsEval.MD17.15.B",
                       "MetricsEval.MD17.16.B",
                       "MetricsEval.MD17.17.B",
                       "MetricsEval.MD17.18.B",
                       "MetricsEval.MD17.19.B",
                       "MetricsEval.MD17.20.B",
                       "MetricsEval.MD17.21.A",
                       "MetricsEval.MD17.22.C",
                       "MetricsEval.MD18.1.A",
                       "MetricsEval.MD18.2.B",
                       "MetricsEval.MD18.3.B",
                       "MetricsEval.MD18.4.B",
                       "MetricsEval.MD18.5.B",
                       "MetricsEval.MD18.6.B",
                       "MetricsEval.MD18.7.B",
                       "MetricsEval.MD18.8.B",
                       "MetricsEval.MD18.9.B"]

        kdma_values_for_each_probe: list[list[KDMA]] = [openapi_server.attribute_values.compute_attribute_values(choice_id=choice_id, all_choices=all_choices) for choice_id in all_choices]
        kdma_averages: list[KDMA]= openapi_server.attribute_values.merge_attribute_values(kdma_results=kdma_values_for_each_probe)
        print("kdma value is ", kdma_averages)
        alignment = openapi_server.alignment_score.calculate_alignment_to_population(actual_attribute_values=kdma_averages,
                                                                      alignment_target_distribution_id=target_name)
        print("alignment is ", alignment)
        self.assertAlmostEqual(alignment, 1) # these choices should maximize MD score
        self.assertAlmostEqual(alignment, kdma_averages[0].value)  # for alignment to 1 (i.e. High), the score is the same as alignment
        return
    

if __name__ == '__main__':
    unittest.main()
