import logging

import pytest

import openapi_server.models
import openapi_server.models.probe_response
import openapi_server.models.probe_response_batch
import openapi_server.models.response
import openapi_server.server_state

# Configure logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


@pytest.fixture
def server_state():
    return openapi_server.server_state.ServerState.get_cached_empty_serverstate()


def test_simple_call_twice(server_state):
    s = server_state
    sid = s.generate_session_id()
    
    choices = (("Probe 1", "Response 1-A"),
               ("Probe 1-A.1", "Response 1-A.1-A"),
               ("Probe 2", "Response 2-A"),
               ("Probe 2-A.1", "Response 2-A.1-A"),
               ("Probe 2-A.1-A.1", "Response 2-A.1-A.1-A"),
               ("Probe 3", "Response 3-A-gauze-sp"),
               ("Probe 3", "Response 3-C"),
               ("Probe 4", "Response 4-A"),
               ("Probe 4.5", "Response 4.5-A"),
               ("Probe 7", "Response 7-A"),
               ("Probe 8", "Response 8-A"),
               ("Probe 8-A.1", "Response 8-A.1-A"),
               ("Probe 8-A.1-A.1", "Response 8-A.1-A.1-A"),
               ("Probe 9", "Response 9-A"),
               ("Probe 9-A.1", "Response 9-A.1-A"),
    )

    responses = [
        openapi_server.models.response.Response(
            scenario_id="DryRunEval-MJ5-eval", probe_id=p, choice=c
        )
        for p, c in choices
    ]

    prb = openapi_server.models.probe_response_batch.ProbeResponseBatch(
        session_id=sid, responses=responses
    )
    s.add_probe_responses(probe_responses=prb)

    kdmas1 = s.get_kdma_profile(session_id=sid)
    assert kdmas1 is not None
    logger.info("kdmas1:")
    for kdma in kdmas1[0]:
        logger.info(f"  {kdma.kdma}: {kdma.value}")

    kdmas2 = s.get_kdma_profile(session_id=sid)
    logger.info("kdmas2:")
    for kdma in kdmas2[0]:
        logger.info(f"  {kdma.kdma}: {kdma.value}")

    assert kdmas1 == kdmas2

    result_mj = s.get_ordered_alignment_and_scores(session_id=sid, kdma_id=openapi_server.models.KDMAName.MORAL_JUDGEMENT)
    assert result_mj is not None
    logger.info(f"alignment mj: {result_mj}\n")
    result_io = s.get_ordered_alignment_and_scores(session_id=sid, kdma_id=openapi_server.models.KDMAName.INGROUP_BIAS)
    assert result_io is not None
    logger.info(f"alignment io: {result_io}\n")
    return None


def test_ta3_example_choices1(server_state) -> None:
    choices = ({'choice': 'Response 1-A', 'justification': 'justification', 'probe_id': 'Probe 1', 'scenario_id': 'DryRunEval-MJ5-eval'}, {'choice': 'Response 1-A.1-A', 'justification': 'justification', 'probe_id': 'Probe 1-A.1', 'scenario_id': 'DryRunEval-MJ5-eval'}, {'choice': 'Response 2-A', 'justification': 'justification', 'probe_id': 'Probe 2', 'scenario_id': 'DryRunEval-MJ5-eval'}, {'choice': 'Response 2-A.1-A', 'justification': 'justification', 'probe_id': 'Probe 2-A.1', 'scenario_id': 'DryRunEval-MJ5-eval'}, {'choice': 'Response 2-A.1-A.1-B', 'justification': 'justification', 'probe_id': 'Probe 2-A.1-A.1', 'scenario_id': 'DryRunEval-MJ5-eval'}, {'choice': 'Response 3-B-gauze-u', 'justification': 'justification', 'probe_id': 'Probe 3', 'scenario_id': 'DryRunEval-MJ5-eval'}, {'choice': 'Response 4-B', 'justification': 'justification', 'probe_id': 'Probe 4', 'scenario_id': 'DryRunEval-MJ5-eval'}, {'choice': 'Response 7-B', 'justification': 'justification', 'probe_id': 'Probe 7', 'scenario_id': 'DryRunEval-MJ5-eval'}, {'choice': 'Response 8-B', 'justification': 'justification', 'probe_id': 'Probe 8', 'scenario_id': 'DryRunEval-MJ5-eval'}, {'choice': 'Response 9-C', 'justification': 'justification', 'probe_id': 'Probe 9', 'scenario_id': 'DryRunEval-MJ5-eval'}, {'choice': 'Response 9-C.1-B', 'justification': 'justification', 'probe_id': 'Probe 9-C.1', 'scenario_id': 'DryRunEval-MJ5-eval'}, {'choice': 'DryRunEval.MJ1.5.B', 'justification': 'justification', 'probe_id': 'DryRunEval.MJ1.5', 'scenario_id': 'DryRunEval.MJ1'}, {'choice': 'DryRunEval.MJ1.6.B', 'justification': 'justification', 'probe_id': 'DryRunEval.MJ1.6', 'scenario_id': 'DryRunEval.MJ1'}, {'choice': 'DryRunEval.MJ1.7.B', 'justification': 'justification', 'probe_id': 'DryRunEval.MJ1.7', 'scenario_id': 'DryRunEval.MJ1'}, {'choice': 'DryRunEval.MJ1.8.B', 'justification': 'justification', 'probe_id': 'DryRunEval.MJ1.8', 'scenario_id': 'DryRunEval.MJ1'}, {'choice': 'DryRunEval.MJ1.9.B', 'justification': 'justification', 'probe_id': 'DryRunEval.MJ1.9', 'scenario_id': 'DryRunEval.MJ1'}, {'choice': 'DryRunEval.MJ1.10.B', 'justification': 'justification', 'probe_id': 'DryRunEval.MJ1.10', 'scenario_id': 'DryRunEval.MJ1'}, {'choice': 'DryRunEval.MJ1.11.B', 'justification': 'justification', 'probe_id': 'DryRunEval.MJ1.11', 'scenario_id': 'DryRunEval.MJ1'}, {'choice': 'DryRunEval.MJ1.12.B', 'justification': 'justification', 'probe_id': 'DryRunEval.MJ1.12', 'scenario_id': 'DryRunEval.MJ1'}, {'choice': 'DryRunEval.MJ1.13.A', 'justification': 'justification', 'probe_id': 'DryRunEval.MJ1.13', 'scenario_id': 'DryRunEval.MJ1'}, {'choice': 'DryRunEval.IO1.1.A', 'justification': 'justification', 'probe_id': 'DryRunEval.IO1.1', 'scenario_id': 'DryRunEval.IO1'}, {'choice': 'DryRunEval.IO1.5.A', 'justification': 'justification', 'probe_id': 'DryRunEval.IO1.5', 'scenario_id': 'DryRunEval.IO1'}, {'choice': 'DryRunEval.IO1.6.A', 'justification': 'justification', 'probe_id': 'DryRunEval.IO1.6', 'scenario_id': 'DryRunEval.IO1'}, {'choice': 'DryRunEval.IO1.7.A', 'justification': 'justification', 'probe_id': 'DryRunEval.IO1.7', 'scenario_id': 'DryRunEval.IO1'}, {'choice': 'DryRunEval.IO1.11.A', 'justification': 'justification', 'probe_id': 'DryRunEval.IO1.11', 'scenario_id': 'DryRunEval.IO1'}, {'choice': 'DryRunEval.IO1.12.B', 'justification': 'justification', 'probe_id': 'DryRunEval.IO1.12', 'scenario_id': 'DryRunEval.IO1'}, {'choice': 'DryRunEval.IO1.15.B', 'justification': 'justification', 'probe_id': 'DryRunEval.IO1.15', 'scenario_id': 'DryRunEval.IO1'}, {'choice': 'DryRunEval.IO1.16.B', 'justification': 'justification', 'probe_id': 'DryRunEval.IO1.16', 'scenario_id': 'DryRunEval.IO1'})
    _run_choices(server_state, choices)
    return None


def test_ta3_example_choices2(server_state) -> None:
    choices = ({'choice': 'Response 2B', 'justification': 'justification', 'probe_id': 'Probe 2', 'scenario_id': 'DryRunEval-MJ2-eval'}, {'choice': 'Response 2B-1A', 'justification': 'justification', 'probe_id': 'Probe 2B-1', 'scenario_id': 'DryRunEval-MJ2-eval'}, {'choice': 'Response 3-B.2-B-gauze-v', 'justification': 'justification', 'probe_id': 'Probe 3-B.2', 'scenario_id': 'DryRunEval-MJ2-eval'}, {'choice': 'Response 3-B.2-B-gauze-v', 'justification': 'justification', 'probe_id': 'Probe 3-B.2', 'scenario_id': 'DryRunEval-MJ2-eval'}, {'choice': 'Response 3-B.2-B-gauze-v', 'justification': 'justification', 'probe_id': 'Probe 3-B.2', 'scenario_id': 'DryRunEval-MJ2-eval'}, {'choice': 'Response 3-B.2-B-gauze-v', 'justification': 'justification', 'probe_id': 'Probe 3-B.2', 'scenario_id': 'DryRunEval-MJ2-eval'}, {'choice': 'Response 3-B.2-A-gauze-s', 'justification': 'justification', 'probe_id': 'Probe 3-B.2', 'scenario_id': 'DryRunEval-MJ2-eval'}, {'choice': 'Response 4-A', 'justification': 'justification', 'probe_id': 'Probe 4', 'scenario_id': 'DryRunEval-MJ2-eval'}, {'choice': 'Response 5-A', 'justification': 'justification', 'probe_id': 'Probe 5', 'scenario_id': 'DryRunEval-MJ2-eval'}, {'choice': 'Response 5-A.1-B', 'justification': 'justification', 'probe_id': 'Probe 5-A.1', 'scenario_id': 'DryRunEval-MJ2-eval'}, {'choice': 'Response 6-B', 'justification': 'justification', 'probe_id': 'Probe 6', 'scenario_id': 'DryRunEval-MJ2-eval'}, {'choice': 'Response 8-A', 'justification': 'justification', 'probe_id': 'Probe 8', 'scenario_id': 'DryRunEval-MJ2-eval'}, {'choice': 'Response 9-B', 'justification': 'justification', 'probe_id': 'Probe 9', 'scenario_id': 'DryRunEval-MJ2-eval'}, {'choice': 'Response 9-B.1-B', 'justification': 'justification', 'probe_id': 'Probe 9-B.1', 'scenario_id': 'DryRunEval-MJ2-eval'}, {'choice': 'DryRunEval.MJ1.5.B', 'justification': 'justification', 'probe_id': 'DryRunEval.MJ1.5', 'scenario_id': 'DryRunEval.MJ1'}, {'choice': 'DryRunEval.MJ1.6.B', 'justification': 'justification', 'probe_id': 'DryRunEval.MJ1.6', 'scenario_id': 'DryRunEval.MJ1'}, {'choice': 'DryRunEval.MJ1.7.B', 'justification': 'justification', 'probe_id': 'DryRunEval.MJ1.7', 'scenario_id': 'DryRunEval.MJ1'}, {'choice': 'DryRunEval.MJ1.8.B', 'justification': 'justification', 'probe_id': 'DryRunEval.MJ1.8', 'scenario_id': 'DryRunEval.MJ1'}, {'choice': 'DryRunEval.MJ1.9.A', 'justification': 'justification', 'probe_id': 'DryRunEval.MJ1.9', 'scenario_id': 'DryRunEval.MJ1'}, {'choice': 'DryRunEval.MJ1.10.A', 'justification': 'justification', 'probe_id': 'DryRunEval.MJ1.10', 'scenario_id': 'DryRunEval.MJ1'}, {'choice': 'DryRunEval.MJ1.11.A', 'justification': 'justification', 'probe_id': 'DryRunEval.MJ1.11', 'scenario_id': 'DryRunEval.MJ1'}, {'choice': 'DryRunEval.MJ1.12.A', 'justification': 'justification', 'probe_id': 'DryRunEval.MJ1.12', 'scenario_id': 'DryRunEval.MJ1'}, {'choice': 'DryRunEval.MJ1.13.A', 'justification': 'justification', 'probe_id': 'DryRunEval.MJ1.13', 'scenario_id': 'DryRunEval.MJ1'}, {'choice': 'DryRunEval.IO1.1.A', 'justification': 'justification', 'probe_id': 'DryRunEval.IO1.1', 'scenario_id': 'DryRunEval.IO1'}, {'choice': 'DryRunEval.IO1.5.A', 'justification': 'justification', 'probe_id': 'DryRunEval.IO1.5', 'scenario_id': 'DryRunEval.IO1'}, {'choice': 'DryRunEval.IO1.6.A', 'justification': 'justification', 'probe_id': 'DryRunEval.IO1.6', 'scenario_id': 'DryRunEval.IO1'}, {'choice': 'DryRunEval.IO1.7.A', 'justification': 'justification', 'probe_id': 'DryRunEval.IO1.7', 'scenario_id': 'DryRunEval.IO1'}, {'choice': 'DryRunEval.IO1.11.B', 'justification': 'justification', 'probe_id': 'DryRunEval.IO1.11', 'scenario_id': 'DryRunEval.IO1'}, {'choice': 'DryRunEval.IO1.12.B', 'justification': 'justification', 'probe_id': 'DryRunEval.IO1.12', 'scenario_id': 'DryRunEval.IO1'}, {'choice': 'DryRunEval.IO1.15.B', 'justification': 'justification', 'probe_id': 'DryRunEval.IO1.15', 'scenario_id': 'DryRunEval.IO1'}, {'choice': 'DryRunEval.IO1.16.B', 'justification': 'justification', 'probe_id': 'DryRunEval.IO1.16', 'scenario_id': 'DryRunEval.IO1'})
    _run_choices(server_state, choices)
    return None


def _run_choices(server_state, choices) -> None:
    s = server_state
    sid = s.generate_session_id()

    responses = [
        openapi_server.models.response.Response(
            scenario_id=c["scenario_id"],
            probe_id=c["probe_id"],
            choice=c["choice"]
        )
        for c in choices
    ]

    prb = openapi_server.models.probe_response_batch.ProbeResponseBatch(
        session_id=sid, responses=responses
    )
    s.add_probe_responses(probe_responses=prb)

    kdmas1 = s.get_kdma_profile(session_id=sid)
    assert kdmas1 is not None
    logger.info("kdmas1:")
    for kdma in kdmas1[0]:
        logger.info(f"  {kdma.kdma}: {kdma.value}")

    kdmas2 = s.get_kdma_profile(session_id=sid)
    logger.info("kdmas2:")
    for kdma in kdmas2[0]:
        logger.info(f"  {kdma.kdma}: {kdma.value}")

    assert kdmas1 == kdmas2

    result_mj = s.get_ordered_alignment_and_scores(session_id=sid, kdma_id=openapi_server.models.KDMAName.MORAL_JUDGEMENT)
    logger.info(f"alignment mj: {result_mj}\n")
    assert result_mj is not None
    result_io = s.get_ordered_alignment_and_scores(session_id=sid, kdma_id=openapi_server.models.KDMAName.INGROUP_BIAS)
    logger.info(f"alignment io: {result_io}\n")
    assert result_io is not None
    return None


if __name__ == "__main__":
    # test_simple_call_twice(server_state=openapi_server.server_state.ServerState.get_cached_empty_serverstate())
    test_ta3_example_choices1(server_state=openapi_server.server_state.ServerState.get_cached_empty_serverstate())
    # test_ta3_example_choices2(server_state=openapi_server.server_state.ServerState.get_cached_empty_serverstate())
