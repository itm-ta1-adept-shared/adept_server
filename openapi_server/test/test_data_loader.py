"""Test cases for loading data"""
import typing
import unittest
from pathlib import Path

import openapi_server.data_loader
import openapi_server.portability
from openapi_server.models.probe_response_batch import ProbeResponseBatch
from openapi_server.models.scenario import Scenario


class TestDataLoader(unittest.TestCase):
    """Test cases for data_loader.py"""

    def test_load_object(self) -> None:
        sid = "DryRunEval.IO1"
        s1 = openapi_server.data_loader.load_object(model_type=Scenario, identity=sid)
        self.assertTrue(isinstance(s1, Scenario))
        self.assertEqual(s1.id, sid)
        self.assertEqual(s1.name, "Multi-Patient Tradeoffs ingroup/lives")
        self.assertEqual(s1.state.unstructured[:3], "You")
        self.assertEqual(len(s1.state.characters), 2)
        self.assertEqual(s1.scenes[1].state.unstructured[-3:], "t.\n")  # and so on
        return
    
    def test_get_known_scenario_ids(self) -> None:
        known_sids = openapi_server.data_loader.get_known_scenario_ids()
        self.assertIn("DryRunEval.IO1", known_sids)
        self.assertGreaterEqual(len(known_sids), 12)  # manually counted
        return   
    
    @unittest.skip("MRE behavior, may not run anymore")
    def test_get_known_probe_ids(self) -> None:
        known_pids = openapi_server.data_loader.get_known_probe_ids()
        self.assertEqual(len(known_pids), 8)
        pids_for_scenario = known_pids["MetricsEval.MD5-Desert"]
        self.assertEqual(len(pids_for_scenario), 3)
        self.assertIn("MetricsEval.MD5.1", pids_for_scenario)
        self.assertEqual(sum(len(pids) for pids in known_pids.values()), 64)  # manually counted
        return
    
    def test_load_object_by_filename(self) -> None:
        try:
            openapi_server.portability.cache_and_cwd_to_repo_root()
            prb = typing.cast(ProbeResponseBatch, openapi_server.data_loader.load_object_by_filename("openapi_server/data/probe_response_batch/mvp/kitware/bbn_aligned_responses.json"))
            self.assertEqual(len(prb.responses), 4)
            self.assertEqual(prb.responses[0].scenario_id, "ADEPT1")
        finally:
            openapi_server.portability.restore_cwd()
        return

    def test_load_all_scenarios(self) -> None:
        scenario_dir = Path("openapi_server/data/scenario")
        for file_path in scenario_dir.rglob('*'):
            if file_path.suffix in ['.yaml', '.yml', '.json']:
                loaded = openapi_server.data_loader.load_object_by_filename(filepath=str(file_path))
                self.assertTrue(isinstance(loaded, Scenario))
        return


if __name__ == '__main__':
    unittest.main()
