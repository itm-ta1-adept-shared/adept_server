
import typing

from flask import json

import openapi_server.data_loader
from openapi_server.models.alignment_results import AlignmentResults
from openapi_server.models.alignment_target import AlignmentTarget  # noqa: E501
from openapi_server.models.alignment_target_distribution import AlignmentTargetDistribution  # noqa: E501
from openapi_server.models.get_alignment_target_api_v1_alignment_target_target_id_get200_response import GetAlignmentTargetApiV1AlignmentTargetTargetIdGet200Response  # noqa: E501
from openapi_server.models.http_validation_error import HTTPValidationError
from openapi_server.models.kdma import KDMA
from openapi_server.models.kdma_name import KDMAName  # noqa: E501
from openapi_server.models.probe_response import ProbeResponse  # noqa: E501
from openapi_server.models.probe_response_batch import ProbeResponseBatch
from openapi_server.models.response import Response  # noqa: E501
from openapi_server.models.scenario import Scenario  # noqa: E501
from openapi_server.test.random_walk import random_walk_scenario


class TestDefaultControllerBase():
    """DefaultController integration tests.
    The test logic is here, the client logic will be implemented in child classes."""

    _target_id: typing.Final[str] = "ADEPT-DryRun-Moral judgement-0.6"
    _scenario_id: typing.Final[str] = "DryRunEval-MJ2-eval"
    _probe_id: typing.Final[str] = "Probe 2"
    _choice_id: typing.Final[str] = "Response 2A"

    def __init__(self):
        self.client = None  # Abstract; child classes should set this to something that implements the open method
        self._responses_sent = 0

    def test_get_alignment_target_api_v1_alignment_target_target_id_get(self):
        """Test case for get_alignment_target_api_v1_alignment_target_target_id_get

        Get Alignment Target
        """
        query_string = [('population', False)]
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/api/v1/alignment_target/{target_id}'.format(target_id=self.__class__._target_id),
            method='GET',
            headers=headers,
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_alignment_target_distribution_ids_api_v1_alignment_target_distribution_ids_get(self):
        """Test case for get_alignment_target_distribution_ids_api_v1_alignment_target_distribution_ids_get

        Get alignment targets distribution IDs
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/api/v1/alignment_target_distribution_ids',
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_alignment_target_ids_api_v1_alignment_target_ids_get(self):
        """Test case for get_alignment_target_ids_api_v1_alignment_target_ids_get

        Get alignment targets IDs
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/api/v1/alignment_target_ids',
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_api_api_v1_get(self):
        """Test case for get_api_api_v1_get

        Get Api
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/api/v1',
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))
  
    def test_post_new_session_id_api_v1_new_session_post(self):
        """Test case for post_new_session_id_api_v1_new_session_post

        Post New Session Id
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/api/v1/new_session',
            method='POST',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_post_probe_response_api_v1_response_post(self):
        """Test case for post_probe_response_api_v1_response_post

        Post Probe Response
        """
        probe_response = {"response":{"probe_id":self.__class__._probe_id,"justification":"justification","choice":self.__class__._choice_id,"scenario_id":self.__class__._scenario_id},"session_id":""}
        headers = { 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        response = self.client.open(
            '/api/v1/response',
            method='POST',
            headers=headers,
            data=json.dumps(probe_response),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))
        self._responses_sent += 1

    def test_post_probe_responses_api_v1_responses_post(self):
        """Test case for post_probe_responses_api_v1_responses_post

        Post Probe Responses
        """
        probe_response_batch = {"session_id":"","responses":[{"probe_id":"Probe 1","justification":"justification","choice":"Response 1-A","scenario_id":"DryRunEval-MJ4-eval"},{"probe_id":"Probe 2 kicker","justification":"justification","choice":"Response 2-A","scenario_id":"DryRunEval-MJ4-eval"}]}
        headers = { 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        response = self.client.open(
            '/api/v1/responses',
            method='POST',
            headers=headers,
            data=json.dumps(probe_response_batch),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))
        self._responses_sent += 1

    def test_get_probe_response_alignment_api_v1_alignment_probe_get(self):
        """Test case for get_probe_response_alignment_api_v1_alignment_probe_get

        Get Probe Response Alignment
        """
        if self._responses_sent < 1:
            self.test_post_probe_response_api_v1_response_post()
        if self._responses_sent < 2:
            self.test_post_probe_responses_api_v1_responses_post()

        query_string = [('session_id', ''),
                        ('target_id', self.__class__._target_id),
                        ('scenario_id', self.__class__._scenario_id),
                        ('probe_id', self.__class__._probe_id),
                        ('population', False)]
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/api/v1/alignment/probe',
            method='GET',
            headers=headers,
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_scenario_api_v1_scenario_scenario_id_get(self):
        """Test case for get_scenario_api_v1_scenario_scenario_id_get

        Get Scenario
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/api/v1/scenario/{scenario_id}'.format(scenario_id=self.__class__._scenario_id),
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_session_alignment_api_v1_alignment_session_get(self):
        """Test case for get_session_alignment_api_v1_alignment_session_get

        Get Session Alignment
        """
        if self._responses_sent < 1:
            self.test_post_probe_response_api_v1_response_post()
        if self._responses_sent < 2:
            self.test_post_probe_responses_api_v1_responses_post()
        query_string = [('session_id', ""),
                        ('target_id', self.__class__._target_id),
                        ('population', False)]
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/api/v1/alignment/session',
            method='GET',
            headers=headers,
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    # Custom test cases

    def _get_scenario(self, scenario_id: str) -> None:
        """ GET a scenario """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.get(
            f'/api/v1/scenario/{scenario_id}',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))
        self.assertEqual(Scenario.from_dict(json.loads(response.data.decode('utf-8'))),
                         openapi_server.data_loader.load_object(Scenario, scenario_id))
        return None

    def test_typical_dryrun_use_case(self) -> None:
        """Tests various API on a run of a dryrun scenario"""
        # Start by getting the lay of the land with the first train scenario
        current_scenario = "DryRunEval.IO1"
        self._get_scenario(current_scenario)
        self.test_get_alignment_target_ids_api_v1_alignment_target_ids_get()
        # Get alignment target
        headers = { 
            'Accept': 'application/json',
        }
        target_id: typing.Final[str] = 'ADEPT-DryRun-Ingroup Bias-0.3'
        response = self.client.get(
            f'/api/v1/alignment_target/{target_id}',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))
        self.assertEqual(AlignmentTarget.from_dict(json.loads(response.data.decode('utf-8'))),
                         openapi_server.data_loader.load_object(AlignmentTarget, target_id))
        
        # Start a session
        response = self.client.post(
            '/api/v1/new_session',
            headers=headers)
        self.assert200(response)
        session_id_1 = json.loads(response.data.decode('utf-8'))
        self.assertTrue(isinstance(session_id_1, str))
        # Make response
        more_headers = { 'Accept': 'application/json', 'Content-Type': 'application/json',}
        probe_id = "DryRunEval.IO1.1"
        response_msg = {
            "response": {
                "choice": "DryRunEval.IO1.1.A",
                "justification": "justification",
                "probe_id": probe_id,
                "scenario_id": current_scenario
            },
            "session_id": session_id_1
        }
        response = self.client.post(
            '/api/v1/response',
            headers=more_headers,
            data=json.dumps(response_msg))
        self.assert200(response)
        # Get alignment
        response = self.client.get(
            '/api/v1/alignment/probe',
            query_string={
                'session_id': session_id_1,
                'target_id': target_id,
                'scenario_id': current_scenario,
                'probe_id': probe_id
            },
            headers=headers
        )
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))
        data = json.loads(response.data.decode('utf-8'))
        alignment_result = AlignmentResults.from_dict(data)
        self.assertTrue(0 < alignment_result.score < 1)
        self.assertEqual(len(alignment_result.alignment_source), 1)
        # Get session alignment
        response = self.client.get(
            '/api/v1/alignment/session',
            query_string={
                'session_id': session_id_1,
                'target_id': target_id
            },
            headers=headers
        )
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))
        data = json.loads(response.data.decode('utf-8'))
        alignment_result = AlignmentResults.from_dict(data)
        self.assertTrue(0 < alignment_result.score < 1)
        self.assertEqual(len(alignment_result.alignment_source), 1)
        # Make responses to all the rest of the probes
        response_tuples = tuple((f"{current_scenario}.{i}", f"{current_scenario}.{i}.A") for i in range(2, 17))
        responses = [Response(scenario_id=current_scenario,
                              probe_id=pid,
                              choice=c,
                              justification="") for pid, c in response_tuples]
        prb = ProbeResponseBatch(session_id=session_id_1,
                                 responses=responses)
        response = self.client.post(
            '/api/v1/responses',
            headers=more_headers,
            data=json.dumps(prb))
        self.assert200(response)
        # Next scenario
        current_scenario = "DryRunEval.MJ1"
        self._get_scenario(current_scenario)
        response_tuples = tuple((f"{current_scenario}.{i}", f"{current_scenario}.{i}.A") for i in range(1, 17))
        responses = [Response(scenario_id=current_scenario,
                              probe_id=pid,
                              choice=c,
                              justification="") for pid, c in response_tuples]
        prb = ProbeResponseBatch(session_id=session_id_1,
                                 responses=responses)
        response = self.client.post(
            '/api/v1/responses',
            headers=more_headers,
            data=json.dumps(prb))
        self.assert200(response)
        # Get session alignment
        response = self.client.get(
            '/api/v1/alignment/session',
            query_string={
                'session_id': session_id_1,
                'target_id': target_id
            },
            headers=headers
        )
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))
        data = json.loads(response.data.decode('utf-8'))
        alignment_result = AlignmentResults.from_dict(data)
        self.assertTrue(0 <= alignment_result.score <= 1)
        self.assertEqual(len(alignment_result.alignment_source), 2)

        # Start an eval session
        response = self.client.post(
            '/api/v1/new_session',
            headers=headers)
        self.assert200(response)
        session_id_1 = json.loads(response.data.decode('utf-8'))
        self.assertTrue(isinstance(session_id_1, str))
        current_scenario = "DryRunEval-MJ2-eval"
        self._get_scenario(current_scenario)  # Get an eval scenario
        # Get alignment target
        headers = { 
            'Accept': 'application/json',
        }
        target_id: typing.Final[str] = 'ADEPT-DryRun-Moral judgement-0.2'
        response = self.client.get(
            f'/api/v1/alignment_target/{target_id}',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))
        self.assertEqual(AlignmentTarget.from_dict(json.loads(response.data.decode('utf-8'))),
                         openapi_server.data_loader.load_object(AlignmentTarget, target_id))
        # Make responses
        response_tuples = random_walk_scenario(scenario=current_scenario)
        responses = [Response(scenario_id=current_scenario,
                              probe_id=pid,
                              choice=c,
                              justification="") for pid, c in response_tuples]
        prb = ProbeResponseBatch(session_id=session_id_1,
                                 responses=responses)
        response = self.client.post(
            '/api/v1/responses',
            headers=more_headers,
            data=json.dumps(prb))
        self.assert200(response)
        # Get session alignment
        response = self.client.get(
            '/api/v1/alignment/session',
            query_string={
                'session_id': session_id_1,
                'target_id': target_id
            },
            headers=headers
        )
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))
        data = json.loads(response.data.decode('utf-8'))
        alignment_result = AlignmentResults.from_dict(data)
        self.assertTrue(0 <= alignment_result.score <= 1, f'{alignment_result.score} is not in the range [0, 1] for responses:\n{responses}')
        self.assertEqual(len(alignment_result.alignment_source), 1)
        # Get KDMA profile
        response = self.client.get(
            '/api/v1/computed_kdma_profile',
            query_string={
                'session_id': session_id_1
            },
            headers=headers
        )
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))
        data = json.loads(response.data.decode('utf-8'))
        kdmas = [KDMA.from_dict(d) for d in data]
        for kdma in kdmas:
            self.assertTrue(len(kdma.kdma) > 1)
            self.assertTrue(0 <= kdma.value <= 1)
            self.assertTrue(kdma.kdes is None)
        # New eval sessions
        response = self.client.post(
            '/api/v1/new_session',
            headers=headers)
        self.assert200(response)
        session_id_2 = json.loads(response.data.decode('utf-8'))
        self.assertTrue(isinstance(session_id_1, str))
        # Make responses
        response_tuples = random_walk_scenario(scenario=current_scenario)
        responses = [Response(scenario_id=current_scenario,
                              probe_id=pid,
                              choice=c,
                              justification="") for pid, c in response_tuples]
        prb = ProbeResponseBatch(session_id=session_id_2,
                                 responses=responses)
        response = self.client.post(
            '/api/v1/responses',
            headers=more_headers,
            data=json.dumps(prb))
        self.assert200(response)
        # Get session session alignment
        response = self.client.get(
            '/api/v1/alignment/compare_sessions',
            query_string={
                'session_id_1': session_id_1,
                'session_id_2': session_id_2
            },
            headers=headers
        )
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))
        data = json.loads(response.data.decode('utf-8'))
        alignment_result = AlignmentResults.from_dict(data)
        self.assertTrue(0 <= alignment_result.score <= 1)
        self.assertEqual(len(alignment_result.alignment_source), 2)  # One scenario in each session
        return None
    
    def test_io2_end_scene(self) -> None:
        headers = { 'Accept': 'application/json', }
        more_headers = { 'Accept': 'application/json', 'Content-Type': 'application/json', }
        current_scenario = "DryRunEval.IO2"
        target_id: typing.Final[str] = 'ADEPT-DryRun-Ingroup Bias-0.3'
        probe_id = "P1"
        # Start a session
        response1 = self.client.post(
            '/api/v1/new_session',
            headers=headers)
        self.assert200(response1)
        session_id_1 = json.loads(response1.data.decode('utf-8'))
        self.assertTrue(isinstance(session_id_1, str))

        # Make responses 
        response_tuples = ((probe_id, "P1.A"), (probe_id, "P1.B"), (probe_id, "n/a"))
        responses = [Response(scenario_id=current_scenario,
                              probe_id=pid,
                              choice=c,
                              justification="") for pid, c in response_tuples]
        prb = ProbeResponseBatch(session_id=session_id_1,
                                 responses=responses)
        response2 = self.client.post(
            '/api/v1/responses',
            headers=more_headers,
            data=json.dumps(prb))
        self.assert200(response2)

        # Get KDMA
        response3 = self.client.get(
            '/api/v1/computed_kdma_profile',
            query_string={
                'session_id': session_id_1
            },
            headers=headers
        )
        self.assert200(response3,
                       'Response body is : ' + response3.data.decode('utf-8'))

        # Get alignment
        response4 = self.client.get(
            '/api/v1/alignment/probe',
            query_string={
                'session_id': session_id_1,
                'target_id': target_id,
                'scenario_id': current_scenario,
                'probe_id': probe_id
            },
            headers=headers
        )
        self.assert200(response4,
                       'Response body is : ' + response4.data.decode('utf-8'))
        data = json.loads(response4.data.decode('utf-8'))
        alignment_result = AlignmentResults.from_dict(data)
        self.assertTrue(0 < alignment_result.score < 1)
        self.assertEqual(len(alignment_result.alignment_source), 1)
        # Get session alignment
        response5 = self.client.get(
            '/api/v1/alignment/session',
            query_string={
                'session_id': session_id_1,
                'target_id': target_id
            },
            headers=headers
        )
        self.assert200(response5,
                       'Response body is : ' + response5.data.decode('utf-8'))
        data = json.loads(response5.data.decode('utf-8'))
        alignment_result = AlignmentResults.from_dict(data)
        self.assertTrue(0 < alignment_result.score < 1)
        self.assertEqual(len(alignment_result.alignment_source), 1)

        return None

    def test_population_stuff(self) -> None:
        headers = { 'Accept': 'application/json', }
        more_headers = { 'Accept': 'application/json', 'Content-Type': 'application/json', }

        # Start a session
        response1 = self.client.post(
            '/api/v1/new_session',
            headers=headers)
        self.assert200(response1)
        session_id_1 = json.loads(response1.data.decode('utf-8'))
        self.assertTrue(isinstance(session_id_1, str))

        scenario_id: typing.Final[str] = "DryRunEval-MJ2-eval"
        pop_target_id: typing.Final[str] = "ADEPT-DryRun-Moral judgement-Population-All"

        r1 = Response(scenario_id=scenario_id,
                    probe_id="Probe 2",
                    choice="Response 2B")
        r2 = Response(scenario_id=scenario_id,
                    probe_id="Probe 2B-1",
                    choice="Response 2B-1B")
        r3 = Response(scenario_id=scenario_id,
                    probe_id="Probe 3-B.2",
                    choice="Response 3-B.2-B-gauze-v")
        r4 = Response(scenario_id=scenario_id,
                    probe_id="Probe 3-B.2",
                    choice="Response 3-B.2-B-gauze-v")
        r5 = Response(scenario_id=scenario_id,
                    probe_id="Probe 3-B.2",
                    choice="Response 3-B.2-A-gauze-s")
        r6 = Response(scenario_id=scenario_id,
                    probe_id="Probe 3-B.2",
                    choice="Response 3-B.2-B-gauze-v")
        r7 = Response(scenario_id=scenario_id,
                    probe_id="Probe 3-B.2",
                    choice="Response 3-B.2-B-gauze-v")
        r8 = Response(scenario_id=scenario_id,
                    probe_id="Probe 3-B.2",
                    choice="Response 3-B.2-C")
        r9 = Response(scenario_id=scenario_id,
                    probe_id="Probe 4",
                    choice="Response 4-B")
        r9b = Response(scenario_id=scenario_id,
                    probe_id="Probe 4-B.1",
                    choice="Response 4-B.1-A")
        r10 = Response(scenario_id=scenario_id,
                    probe_id="Probe 5",
                    choice="Response 5-B")
        r11 = Response(scenario_id=scenario_id,
                    probe_id="Probe 5-B.1",
                    choice="Response 5-B.1-B")
        r12 = Response(scenario_id=scenario_id,
                    probe_id="Probe 6",
                    choice="Response 6-B")
        r13 = Response(scenario_id=scenario_id,
                    probe_id="Probe 7",
                    choice="Response 7-B")
        r14 = Response(scenario_id=scenario_id,
                    probe_id="Probe 8",
                    choice="Response 8-B")
        r15 = Response(scenario_id=scenario_id,
                    probe_id="Probe 9",
                    choice="Response 9-A")
        r16 = Response(scenario_id=scenario_id,
                    probe_id="Probe 9-A.1",
                    choice="Response 9-A.1-A")
        r17 = Response(scenario_id=scenario_id,
                    probe_id="Probe 10",
                    choice="Response 10-A")
        prb = ProbeResponseBatch(session_id=session_id_1,
                                responses=[r1, r2, r3, r4, r5, r6, r7, r8,
                                    r9, r9b, r10, r11, r12, r13, r14,
                                    r15, r16, r17])
        response2 = self.client.post(
            '/api/v1/responses',
            headers=more_headers,
            data=json.dumps(prb))
        self.assert200(response2)

        # Get KDMA
        response3 = self.client.get(
            '/api/v1/computed_kdma_profile',
            query_string={
                'session_id': session_id_1
            },
            headers=headers
        )
        self.assert200(response3,
                       'Response body is : ' + response3.data.decode('utf-8'))

        # Get session alignment
        response5 = self.client.get(
            '/api/v1/alignment/session',
            query_string={
                'session_id': session_id_1,
                'target_id': pop_target_id,
                'population': True
            },
            headers=headers
        )
        self.assert200(response5,
                       'Response body is : ' + response5.data.decode('utf-8'))
        data = json.loads(response5.data.decode('utf-8'))
        alignment_result = AlignmentResults.from_dict(data)
        self.assertTrue(0 < alignment_result.score < 1)
        self.assertEqual(len(alignment_result.alignment_source), 1)
        return None
