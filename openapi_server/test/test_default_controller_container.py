import logging
import os
import subprocess
import time
import unittest

import requests

from openapi_server.test import BaseTestCase
from openapi_server.test.test_default_controller_base import TestDefaultControllerBase


class ClientWithOpen:
     def __init__(self, base_url: str):
         self.base_url = base_url
    
     def open(self, method, url, headers=None, json_data=None, query_string=None):
        # Use the requests library to communicate with the Docker container
        if method == 'GET':
            response = requests.get(self.base_url + url, headers=headers, params=query_string)
        elif method == 'POST':
            response = requests.post(self.base_url + url, headers=headers, json=json_data)
        # Add more HTTP methods as needed
        return response   


class TestDefaultControllerContainer(BaseTestCase, TestDefaultControllerBase):
    """
    DefaultController integration test for testing Flask app running as a container.
    
    Note, docker daemon (e.g. Docker Desktop) is required to be running for this test to run.
    """

    @classmethod
    def setUpClass(cls):
        # Check if Docker is available
        try:
            subprocess.run(["docker", "--version"], check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        except (subprocess.CalledProcessError, FileNotFoundError):
            raise unittest.SkipTest("Skipping test because Docker is not available")
 
        BaseTestCase.setUpClass()
        # Step 1: Build and run the Docker container
        cls.image_name = "adept_test_image"
        cls.container_name = "adept_test_container"

        # Stop and remove any existing container/image with the same name
        subprocess.run(["docker", "rm", "-f", cls.container_name], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        subprocess.run(["docker", "image", "rm", "-f", cls.image_name], stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        # Build the Docker image
        build_command = ["docker", "build", "-t", cls.image_name, "."]
        try:
            subprocess.run(build_command, check=True)
        except subprocess.CalledProcessError as e:
            logging.exception("Do you have the Docker daemon running?")
            raise unittest.SkipTest(f"Skipping test due to Docker build failure: {e}")

        # Run the Docker container
        run_command = [
            "docker", "run", "-d",
            "--name", cls.container_name,
            "-p", "8080:8080", cls.image_name
        ]
        subprocess.run(run_command, check=True)

        # Wait for the container to be ready
        time.sleep(5)  # Adjust depending on startup time
        
    @classmethod
    def tearDownClass(cls):
        subprocess.run(["docker", "stop", cls.container_name], check=True)
        subprocess.run(["docker", "rm", cls.container_name], check=True)
        subprocess.run(["docker", "image", "rm", cls.image_name], check=True)
        BaseTestCase.tearDownClass()

    def __init__(self, *args, **kwargs):
        TestDefaultControllerBase.__init__(self)
        BaseTestCase.__init__(self, *args, **kwargs)
        self.client = ClientWithOpen(base_url="http://localhost:8080")


if __name__ == '__main__':
    unittest.main()
