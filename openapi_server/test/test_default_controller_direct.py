import unittest

from openapi_server.test import BaseTestCase
from openapi_server.test.test_default_controller_base import TestDefaultControllerBase

class TestDefaultControllerDirect(BaseTestCase, TestDefaultControllerBase):
    """DefaultController integration test for directly testing Flask app"""

    def __init__(self, *args, **kwargs):
        TestDefaultControllerBase.__init__(self)
        BaseTestCase.__init__(self, *args, **kwargs)


if __name__ == '__main__':
    unittest.main()
