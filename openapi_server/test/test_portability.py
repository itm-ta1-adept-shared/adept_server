"""Test cross-platform helpers"""
import os
import unittest
from pathlib import Path

import openapi_server.portability


class TestPortability(unittest.TestCase):
    """Test cases for portability.py"""

    def test_repo_root(self) -> None:
        mydir = openapi_server.portability.repo_root()
        self.assertIn(mydir.name, ("adept_api", "app", "adept_server"))  # adept_api is local version, app is for Docker
        return

    def test_init_cwd_to_repo_root(self) -> None:
        original_cwd = os.getcwd()
        try:
            root_dir = openapi_server.portability.repo_root()
            openapi_server.portability.init_cwd_to_repo_root()
            wd = Path(os.getcwd())
            self.assertEqual(root_dir, wd)
            openapi_server.portability.init_cwd_to_repo_root()
            self.assertEqual(root_dir, wd)
            os.chdir("openapi_server")
            openapi_server.portability.init_cwd_to_repo_root()
            wd = Path(os.getcwd())
            self.assertEqual(wd, root_dir / "openapi_server")
        finally:
            os.chdir(original_cwd)  # try to leave us in the state we started
        return

    def test_cache_cwd(self) -> None:
        initial_cwd = os.getcwd()
        openapi_server.portability.cache_cwd()
        os.chdir("..")
        changed_wd = os.getcwd()
        assert initial_cwd != changed_wd
        openapi_server.portability.restore_cwd()
        self.assertEqual(initial_cwd, os.getcwd())
        return


if __name__ == "__main__":
    unittest.main()
