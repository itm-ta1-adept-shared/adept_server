"""Test manipulating the state of the server"""
import math
import pathlib
import shutil
import typing
import unittest

from openapi_server import attribute_values, scenario_parsing
from openapi_server.models.alignment_results import AlignmentResults
from openapi_server.models.kdma_name import KDMAName
from openapi_server.models.probe_response import ProbeResponse
from openapi_server.models.probe_response_batch import ProbeResponseBatch
from openapi_server.models.response import Response
from openapi_server.server_state import ServerState


class TestServerState(unittest.TestCase):
    """testing server_state.py"""

    @classmethod
    def setUpClass(cls):
        fast_and_loose = True
        cls.server = ServerState.get_cached_empty_serverstate() if fast_and_loose else ServerState()

    @unittest.skip("MRE behavior, may not run anymore")
    def test_stuff(self) -> None:
        session_id = self.server.generate_session_id()
        scenario_id: typing.Final[str] = "MetricsEval.MD18"
        target_id: typing.Final[str] = "ADEPT-metrics_eval-alignment-target-train-HIGH"

        r1 = Response(scenario_id=scenario_id,
                      probe_id="MetricsEval.MD18.1",
                      choice="MetricsEval.MD18.1.A")
        pr = ProbeResponse(session_id=session_id,
                           response=r1)
        self.server.add_probe_response(pr)

        r2 = Response(scenario_id=scenario_id,
                      probe_id="MetricsEval.MD18.2",
                      choice="MetricsEval.MD18.2.B")
        r3 = Response(scenario_id=scenario_id,
                      probe_id="MetricsEval.MD18.3",
                      choice="MetricsEval.MD18.3.A")
        prb = ProbeResponseBatch(session_id=session_id,
                                 responses=[r2, r3])
        self.server.add_probe_responses(probe_responses=prb)

        ar1 = self.server.get_alignment_result_probe(session_id=session_id,
                                                target_id=target_id,
                                                scenario_id=scenario_id,
                                                probe_id="MetricsEval.MD18.1",
                                                population=False)
        self.assertAlmostEqual(ar1.score, 0.5)  # value .5 vs target 1
        ar1b = self.server.get_alignment_result_probe(session_id=session_id,
                                                 target_id=target_id,
                                                 scenario_id=scenario_id,
                                                 probe_id="MetricsEval.MD18.1",
                                                 population=1)
        self.assertLess(ar1b.score, 0.01)  # value .5 vs target 1 distribution
        self.assertGreaterEqual(ar1b.score, 0.0)  # value .5 vs target 1 distribution
        ar2 = self.server.get_alignment_result_probe(session_id=session_id,
                                                target_id=target_id,
                                                scenario_id=scenario_id,
                                                probe_id="MetricsEval.MD18.2",
                                                population=False)
        self.assertAlmostEqual(ar2.score, 1.0)  # value 1 vs target 1
        ar2b = self.server.get_alignment_result_probe(session_id=session_id,
                                                 target_id=target_id,
                                                 scenario_id=scenario_id,
                                                 probe_id="MetricsEval.MD18.2",
                                                 population=1)
        self.assertGreater(ar2b.score, 0.99)  # value 1 vs target 1 distribution
        self.assertLessEqual(ar2b.score, 1.0)
        ar3 = self.server.get_alignment_result_probe(session_id=session_id,
                                                target_id=target_id,
                                                scenario_id=scenario_id,
                                                probe_id="MetricsEval.MD18.3",
                                                population=False)
        self.assertAlmostEqual(ar3.score, 0)  # score of 0 against alignment of 1
        ar3b = self.server.get_alignment_result_probe(session_id=session_id,
                                                 target_id=target_id,
                                                 scenario_id=scenario_id,
                                                 probe_id="MetricsEval.MD18.3",
                                                 population=1)
        self.assertAlmostEqual(ar3b.score, 0)  # score of 0 against alignment of 1

        ar4 = self.server.get_alignment_result_session(session_id=session_id, target_id=target_id, population=False)
        self.assertAlmostEqual(ar4.score, 0.5)  # 0.5, 1, 0

        # Again
        new_session = self.server.generate_session_id()
        r1 = Response(scenario_id=scenario_id,
                      probe_id="MetricsEval.MD18.1",
                      choice="MetricsEval.MD18.1.A")
        pr = ProbeResponse(session_id=new_session,
                           response=r1)
        self.server.add_probe_response(pr)
        ar1 = self.server.get_alignment_result_probe(session_id=new_session,
                                                target_id=target_id,
                                                scenario_id=scenario_id,
                                                probe_id="MetricsEval.MD18.1",
                                                population=False)
        self.assertAlmostEqual(ar1.score, 0.5)
        ar2 = self.server.get_alignment_result_session(session_id=new_session,
                                                  target_id=target_id,
                                                  population=False)
        self.assertAlmostEqual(ar2.score, 0.5)
        return

    def test_gauze_mj2(self) -> None:
        session_id = self.server.generate_session_id()
        scenario_id: typing.Final[str] = "DryRunEval-MJ2-eval"
        responses = [Response(scenario_id=scenario_id, probe_id="Probe 3-B.2", choice="Response 3-B.2-A-gauze-s"),
                     Response(scenario_id=scenario_id, probe_id="Probe 3-B.2", choice="Response 3-B.2-A-gauze-s"),
                     Response(scenario_id=scenario_id, probe_id="Probe 3-B.2", choice="Response 3-B.2-A-gauze-s"),
                     Response(scenario_id=scenario_id, probe_id="Probe 3-B.2", choice="Response 3-B.2-C")]
        prb = ProbeResponseBatch(session_id=session_id, responses=responses)
        self.server.add_probe_responses(probe_responses=prb)
        kdmas, _ = self.server.get_kdma_profile(session_id=session_id)

        # A lot of asserts here, mostly testing intermediate logic, which actually may change.

        self.assertEqual(len(kdmas), 1)
        self.assertEqual(kdmas[0].kdma, "Moral judgement")

        expected: dict[str, set[float]] = scenario_parsing.get_kmda_values_for_probe(scenario_id, "Probe 3-B.2")
        self.assertEqual(len(expected), 1)
        self.assertIn("Moral judgement", expected.keys())
        self.assertEqual(expected["Moral judgement"], {0.2, 0.4, 0.7})
        
        a3, b3, c3 = attribute_values._estimate_expected_value_three_choices(0.2, 0.4, 0.7)
        self.assertAlmostEqual(a3, 0.156, 2)
        self.assertAlmostEqual(b3, 0.433, 2)
        self.assertAlmostEqual(c3, 0.767, 2)

        self.assertAlmostEqual(kdmas[0].value, a3, 2)

    def test_gauze_mj4(self) -> None:
        session_id = self.server.generate_session_id()
        scenario_id: typing.Final[str] = "DryRunEval-MJ5-eval"
        responses = [Response(scenario_id=scenario_id, probe_id="Probe 2", choice="Response 2-A"),  # Springer is better
                     Response(scenario_id=scenario_id, probe_id="Probe 2-A.1", choice="Response 2-A.1-B-gauze-sp"),
                     Response(scenario_id=scenario_id, probe_id="Probe 3", choice="Response 3-C")]
        prb = ProbeResponseBatch(session_id=session_id, responses=responses)
        self.server.add_probe_responses(probe_responses=prb)
        kdmas, _ = self.server.get_kdma_profile(session_id=session_id)

        self.assertEqual(len(kdmas), 1)
        self.assertEqual(kdmas[0].kdma, "Moral judgement")

        expected: dict[str, set[float]] = scenario_parsing.get_kmda_values_for_probe(scenario_id, "Probe 3")
        self.assertEqual(len(expected), 1)
        self.assertIn("Moral judgement", expected.keys())
        self.assertEqual(expected["Moral judgement"], {0.1, 0.2, 0.4, 0.6, 0.7, 1.0})

        a2, b2 = attribute_values._estimate_expected_value(0.2, 0.7)  # due to Response 2-A
        self.assertAlmostEqual(a2, 0.248, 3)
        self.assertAlmostEqual(b2, 0.706, 3)

        # Response 2-A.1-B-gauze-sp doesn't have a KDMA value (it was for stay/search)

        expected_vals = attribute_values._estimate_expected_value_generic((0.1, 0.2, 0.4, 0.6, 0.7, 1.0))
        self.assertEqual(len(expected_vals), 6)
        calculated_elsewhere_vals = (0.0778, 0.2332, 0.4001, 0.5666, 0.7667, 0.9003)
        for exp_val, calc_val in zip(expected_vals, calculated_elsewhere_vals):
            self.assertAlmostEqual(exp_val, calc_val, places=3)

        self.assertAlmostEqual(kdmas[0].value, (a2+expected_vals[0])/2, 2)

    def test_get_least_most_aligned_targets(self) -> None:
        session_id = self.server.generate_session_id()
        scenario_id: typing.Final[str] = "DryRunEval.IO1"

        r1 = Response(scenario_id=scenario_id,
                    probe_id="DryRunEval.IO1.1",
                    choice="DryRunEval.IO1.1.A")
        pr = ProbeResponse(session_id=session_id,
                        response=r1)
        self.server.add_probe_response(pr)

        r2 = Response(scenario_id=scenario_id,
                    probe_id="DryRunEval.IO1.2",
                    choice="DryRunEval.IO1.2.A")
        r3 = Response(scenario_id=scenario_id,
                    probe_id="DryRunEval.IO1.3",
                    choice="DryRunEval.IO1.3.A")
        prb = ProbeResponseBatch(session_id=session_id,
                                responses=[r2, r3])
        self.server.add_probe_responses(probe_responses=prb)
        
        m_l_at = self.server.get_least_most_aligned_targets(session_id=session_id)
        self.assertTrue(isinstance(m_l_at, dict))
        self.assertEqual(len(m_l_at), 2)
        # In general, the least aligned should always be a 0 or 1.  Because we picked IO1 training, we know it will be one of the Ingroup Bias targets.
        self.assertIn(m_l_at["least_aligned"], ("ADEPT-DryRun-Ingroup Bias-0.2", "ADEPT-DryRun-Ingroup Bias-0.8"))
        # Now we'll make some asserts that are based on observed results rather than well-reasoned.  So these are more regressions tests.
        self.assertEqual(m_l_at["least_aligned"], 'ADEPT-DryRun-Ingroup Bias-0.2')
        self.assertEqual(m_l_at["most_aligned"], 'ADEPT-DryRun-Ingroup Bias-0.7')
        return None
    
    def test_get_alignment_sessions(self) -> None:
        server = ServerState()
        session_id = server.generate_session_id()
        scenario_id: typing.Final[str] = "DryRunEval.IO1"

        r1 = Response(scenario_id=scenario_id,
                    probe_id="DryRunEval.IO1.1",
                    choice="DryRunEval.IO1.1.A")
        pr = ProbeResponse(session_id=session_id,
                        response=r1)
        server.add_probe_response(pr)

        r2 = Response(scenario_id=scenario_id,
                    probe_id="DryRunEval.IO1.2",
                    choice="DryRunEval.IO1.2.A")
        r3 = Response(scenario_id=scenario_id,
                    probe_id="DryRunEval.IO1.3",
                    choice="DryRunEval.IO1.3.A")
        prb = ProbeResponseBatch(session_id=session_id,
                                responses=[r2, r3])
        server.add_probe_responses(probe_responses=prb)
        
        # New session, same results
        session_idB = server.generate_session_id()
        prbB = ProbeResponseBatch(session_id=session_idB,
                                responses=[r1, r2, r3])
        server.add_probe_responses(probe_responses=prbB)
        ss_alignment = server.get_alignment_sessions(session_id1=session_id, session_id2=session_idB)
        assert math.isclose(ss_alignment.score, 1.0, abs_tol=0.0001)

        # New session, opposite results (I didn't actually check that B answer was opposite)
        session_idC = server.generate_session_id()
        r1C = Response(scenario_id=scenario_id,
                    probe_id="DryRunEval.IO1.1",
                    choice="DryRunEval.IO1.1.B")
        r2C = Response(scenario_id=scenario_id,
                    probe_id="DryRunEval.IO1.2",
                    choice="DryRunEval.IO1.2.B")
        r3C = Response(scenario_id=scenario_id,
                    probe_id="DryRunEval.IO1.3",
                    choice="DryRunEval.IO1.3.B")
        prbC = ProbeResponseBatch(session_id=session_idC,
                                responses=[r1C, r2C, r3C])
        server.add_probe_responses(probe_responses=prbC)
        ssC_alignment = server.get_alignment_sessions(session_id1=session_id, session_id2=session_idC)
        assert ssC_alignment.score < ss_alignment.score

        return None

    def test_io2(self) -> None:
        session_id = self.server.generate_session_id()
        target_id = "ADEPT-DryRun-Ingroup Bias-0.2"
        scenario_id: typing.Final[str] = "DryRunEval.IO2"
        probe_id = "P1"
        responses = [Response(scenario_id=scenario_id, probe_id=probe_id, choice="P1.A"),
                     Response(scenario_id=scenario_id, probe_id=probe_id, choice="P1.A"),
                     Response(scenario_id=scenario_id, probe_id=probe_id, choice="P1.B")]
        prb = ProbeResponseBatch(session_id=session_id, responses=responses)
        self.server.add_probe_responses(probe_responses=prb)

        kdmas, _ = self.server.get_kdma_profile(session_id=session_id)
        self.assertGreaterEqual(len(kdmas), 1)
        self.assertLessEqual(len(kdmas), 2)
        ar1 = self.server.get_alignment_result_probe(session_id=session_id, target_id=target_id, scenario_id=scenario_id, probe_id=probe_id, population=False)
        self.assertGreaterEqual(ar1.score, 0)
        self.assertLessEqual(ar1.score, 1)
        ar2 = self.server.get_alignment_result_session(session_id=session_id, target_id=target_id, population=False)
        self.assertGreaterEqual(ar2.score, 0)
        self.assertLessEqual(ar2.score, 1)
        self.assertAlmostEqual(ar1.score, ar2.score)

        # The following is more stringent value based on what was seen (e.g. more of a regression test than an independently verified unit test)
        self.assertAlmostEqual(ar1.score, 0.4167, delta=0.001)

        # A session that should align closer to the target
        session_id = self.server.generate_session_id()
        responses = [Response(scenario_id=scenario_id, probe_id=probe_id, choice="P1.A"),
                     Response(scenario_id=scenario_id, probe_id=probe_id, choice="P1.B"),
                     Response(scenario_id=scenario_id, probe_id=probe_id, choice="P1.B")]
        prb = ProbeResponseBatch(session_id=session_id, responses=responses)
        self.server.add_probe_responses(probe_responses=prb)
        ar3 = self.server.get_alignment_result_session(session_id=session_id, target_id=target_id, population=False)
        self.assertGreater(ar3.score, ar1.score)

        # Now a session with equal
        session_id = self.server.generate_session_id()
        responses = [Response(scenario_id=scenario_id, probe_id=probe_id, choice="P1.A"),
                     Response(scenario_id=scenario_id, probe_id=probe_id, choice="P1.B")]
        prb = ProbeResponseBatch(session_id=session_id, responses=responses)
        self.server.add_probe_responses(probe_responses=prb)
        kmdas2, _ = self.server.get_kdma_profile(session_id=session_id)
        kdma_vals = scenario_parsing.get_kmda_values_for_probe(scenario_id=scenario_id, probe_id=probe_id)
        middle_index = len(kdma_vals["Ingroup Bias"]) // 2
        self.assertEqual(middle_index, 1)
        self.assertAlmostEqual(sorted(kdma_vals["Ingroup Bias"])[middle_index], 0.4, delta=0.001)
        self.assertAlmostEqual(kmdas2[0].value, 0.4, delta=0.001)

        # Now do testing with the end_scene (as represented by choice "n/a")
        session_id = self.server.generate_session_id()
        responses = [Response(scenario_id=scenario_id, probe_id=probe_id, choice="P1.A"),
                     Response(scenario_id=scenario_id, probe_id=probe_id, choice="n/a")]
        prb = ProbeResponseBatch(session_id=session_id, responses=responses)
        self.server.add_probe_responses(probe_responses=prb)
        kmdas3, _ = self.server.get_kdma_profile(session_id=session_id)
        self.assertAlmostEqual(sorted(kdma_vals["Ingroup Bias"])[2], 0.8, delta=0.001)
        self.assertAlmostEqual(kmdas3[0].value, 0.8, delta=0.1)
        ar4 = self.server.get_alignment_result_probe(session_id=session_id, target_id=target_id, scenario_id=scenario_id, probe_id="P1", population=False)
        self.assertAlmostEqual(ar4.score, 0.4167, delta=0.001)  # observered value
        ar5 = self.server.get_alignment_result_session(session_id=session_id, target_id=target_id, population=False)
        self.assertAlmostEqual(ar4.score, ar5.score)

        return None
    
    def test_session_alignment_is_not_per_scenario(self) -> None:
        """This was created when it was reported that alignment scores TA2 were getting is per scenario.
        (It ended up being TA3 was creating a new session per scenario.)
        """
        session_id = self.server.generate_session_id()
        scenario_id: typing.Final[str] = "DryRunEval.IO1"
        target_id = "ADEPT-DryRun-Ingroup Bias-0.2"
        pairs = ((f"DryRunEval.IO1.{i}", f"DryRunEval.IO1.{i}.A") for i in range(1, 17))
        responses = [Response(scenario_id=scenario_id, probe_id=pid, choice=c) for pid, c in pairs]
        prb = ProbeResponseBatch(session_id=session_id, responses=responses)
        self.server.add_probe_responses(probe_responses=prb)
        kdmas1 = self.server.get_kdma_profile(session_id=session_id)
        self.assertEqual(kdmas1[0][0].kdma, "Ingroup Bias")
        self.assertGreater(kdmas1[0][0].value, 0)
        self.assertLess(kdmas1[0][0].value, 1)
        ar1 = self.server.get_alignment_result_session(session_id=session_id, target_id=target_id, population=False)
        self.assertGreater(ar1.score, 0)
        self.assertLess(ar1.score, 1)
        # Now adding a new response from a different scenario
        scenario_id = "DryRunEval.IO3"
        probe_id = "DryRunEval.IO3.2"
        choice = "DryRunEval.IO3.2.A"
        pr = ProbeResponse(session_id=session_id, response=Response(scenario_id=scenario_id, probe_id=probe_id, choice=choice))
        self.server.add_probe_response(probe_response=pr)
        kdmas2 = self.server.get_kdma_profile(session_id=session_id)
        self.assertNotAlmostEqual(kdmas2[0][0].value, kdmas1[0][0].value)
        ar2 = self.server.get_alignment_result_session(session_id=session_id, target_id=target_id, population=False)
        self.assertGreater(ar2.score, 0)
        self.assertLess(ar2.score, 1)
        self.assertNotAlmostEqual(ar2.score, ar1.score)
        ar2b = self.server.get_alignment_result_probe(session_id=session_id, target_id=target_id, scenario_id=scenario_id, probe_id=probe_id, population=False)
        self.assertGreater(ar2b.score, 0)
        self.assertLess(ar2b.score, 1)
        # This is the main point, and checks that the session after the new scenario was started is not just the result from that scenario
        self.assertNotAlmostEqual(ar2b.score, ar2.score)

        # A new session with just the one probe from the second scenario should produce same results as previous result_probe call
        session_id = self.server.generate_session_id()
        pr = ProbeResponse(session_id=session_id, response=Response(scenario_id=scenario_id, probe_id=probe_id, choice=choice))
        self.server.add_probe_response(probe_response=pr)
        kdmas3 = self.server.get_kdma_profile(session_id=session_id)
        self.assertGreater(kdmas3[0][0].value, 0)
        self.assertLess(kdmas3[0][0].value, 1)
        self.assertNotAlmostEqual(kdmas3[0][0].value, kdmas1[0][0].value)
        self.assertNotAlmostEqual(kdmas3[0][0].value, kdmas2[0][0].value)
        ar3 = self.server.get_alignment_result_session(session_id=session_id, target_id=target_id, population=False)
        self.assertAlmostEqual(ar3.score, ar2b.score)
        ar3b = self.server.get_alignment_result_probe(session_id=session_id, target_id=target_id, scenario_id=scenario_id, probe_id=probe_id, population=False)
        self.assertAlmostEqual(ar3b.score, ar3.score)

        return None
    
    def test_sesssion_alignment_with_kdma_filter(self) -> None:
        session1_id = self.server.generate_session_id()
        pr1 = ProbeResponse(session_id=session1_id, response=Response(scenario_id="DryRunEval-MJ4-eval", probe_id="Probe 7", choice="Response 7-B"))
        self.server.add_probe_response(pr1)
        session2_id = self.server.generate_session_id()
        pr2 = ProbeResponse(session_id=session2_id, response=Response(scenario_id="DryRunEval-MJ4-eval", probe_id="Probe 7", choice="Response 7-C"))
        self.server.add_probe_response(pr2)
        ar1 = self.server.get_alignment_sessions(session1_id, session2_id)
        self.assertGreater(ar1.score, 0)
        self.assertLess(ar1.score, 1)
        ar2 = self.server.get_alignment_sessions(session1_id, session2_id, KDMAName.INGROUP_BIAS)
        self.assertAlmostEqual(ar2.score, 1)
        self.assertLess(ar1.score, ar2.score)
        return None
    
    def test_session_alignment_with_kdma_filter_using_ta3_problem_example(self) -> None:
        """
        TA3 reported these sessions were giving the same alignment using the two different filters.
        This test shows otherwise...
        """
        session1_id = "0a76cce4-27e3-4a51-8525-d0da45bcaf71"
        responses1 = (
            ("Probe 1", "Response 1-A"),
            ("Probe 1-A.1", "Response 1-A.1-A"),
            ("Probe 2", "Response 2-B"),
            ("Probe 2-B.1", "Response 2-B.1-B-gauze-u"),
            ("Probe 2-B.1-B.1", "Response 2-B.1-B.1-B"),
            ("Probe 3", "Response 3-B-gauze-u"),
            ("Probe 3", "Response 3-B-gauze-u"),
            ("Probe 3", "Response 3-A-gauze-sp"),
            ("Probe 3", "Response 3-A-gauze-sp"),
            ("Probe 3", "Response 3-A-gauze-sp"),
            ("Probe 3", "Response 3-C"),
            ("Probe 4", "Response 4-A"),
            ("Probe 4.5", "Response 4.5-A"),
            ("Probe 7", "Response 7-B"),
            ("Probe 8", "Response 8-B"),
            ("Probe 9", "Response 9-C"),
            ("Probe 9-C.1", "Response 9-C.1-B"),
        )
        responses1 = [Response(scenario_id="DryRunEval-MJ5-eval", probe_id=pid, choice=c) for pid, c in responses1]
        prb1 = ProbeResponseBatch(session_id=session1_id, responses=responses1)
        self.server.add_probe_responses(prb1)

        session2_id = "88d02547-3925-4037-ad99-ab2bbf092222"
        responses2 = (
            ("Probe 1", "Response 1-A"),
            ("Probe 2 kicker", "Response 2-A"),
            ("Probe 2-A.1", "Response 2-A.1-B"),
            ("Probe 3", "Response 3-B"),
            ("Probe 3-B.1", "Response 3-B.1-A"),
            ("Probe 6", "Response 6-A"),
            ("Probe 7", "Response 7-B"),
            ("Probe 8", "Response 8-B"),
            ("Probe 9", "Response 9-A"),
            ("Probe 10", "Response 10-C"),
            ("Probe 10-C.1", "Response 10-C.1-A"),
        )
        responses2 = [Response(scenario_id="DryRunEval-MJ4-eval", probe_id=pid, choice=c) for pid, c in responses2]
        prb2 = ProbeResponseBatch(session_id=session2_id, responses=responses2)
        self.server.add_probe_responses(prb2)

        ar1 = self.server.get_alignment_sessions(session1_id, session2_id)
        self.assertGreater(ar1.score, 0)
        self.assertLess(ar1.score, 1)
        ar2 = self.server.get_alignment_sessions(session1_id, session2_id, KDMAName.INGROUP_BIAS)
        self.assertGreater(ar2.score, 0)
        self.assertLess(ar2.score, 1)
        ar3 = self.server.get_alignment_sessions(session1_id, session2_id, KDMAName.MORAL_JUDGEMENT)
        self.assertGreater(ar3.score, 0)
        self.assertLess(ar3.score, 1)

        # In theory some of those alignment scores could happen to be the same, but that would be a coincidence
        self.assertNotEqual(ar1.score, ar2.score)
        self.assertNotEqual(ar1.score, ar3.score)
        self.assertNotEqual(ar2.score, ar3.score)

        return None
    
    def test_persist(self) -> None:
        """
        Check if the persist mechanism works.
        """
        shutil.rmtree(path=ServerState._SAVE_DIR, ignore_errors=True)
        self.assertFalse(self.server._autosave)  # autosave off by default
        self.assertEqual(self.server.load_state(), 204)  # loading when nothing saved
        session_id = self.server.generate_session_id()
        response = Response(scenario_id="DryRunEval-MJ2-eval", probe_id="Probe 2", choice="Response 2A")
        self.server.add_probe_response(ProbeResponse(session_id=session_id, response=response))
        self.assertEqual(self.server.load_state(), 204)  # adding a probe without save didn't save
        self.server.save_state()
        self.assertEqual(self.server.load_state(), 200)  # loading saved when already loaded
        self.assertListEqual(self.server._probe_responses_by_session[session_id], [response])  # make sure its still there
        self.server.reset()  # make sure clear works
        self.assertFalse(self.server._probe_responses_by_session)  # No sessions
        self.assertEqual(self.server.load_state(), 200)  # loading saved from empty
        self.assertListEqual(self.server._probe_responses_by_session[session_id], [response])  # make sure it came back
        self.server.reset()
        self.server.set_autosave(enabled=False)  # make sure off to off doesn't do much
        self.assertFalse(self.server._autosave)
        self.assertEqual(self.server.load_state(), 200)
        self.assertListEqual(self.server._probe_responses_by_session[session_id], [response])  # make sure it came back
        # Check autosave works
        self.server.reset()
        self.server.set_autosave(enabled=True)
        self.assertTrue(self.server._autosave)
        self.assertEqual(self.server.load_state(), 200)
        self.assertFalse(self.server._probe_responses_by_session)  # No sessions (because we did an implicit save when we enabled autosave while empty)
        self.server.add_probe_response(ProbeResponse(session_id=session_id, response=response))
        self.assertEqual(self.server.load_state(), 200)
        self.assertListEqual(self.server._probe_responses_by_session[session_id], [response])  # make sure it was saved because autosave and came back
        self.server.reset()
        self.assertFalse(self.server._probe_responses_by_session)  # No sessions
        self.assertEqual(self.server.load_state(), 200)
        self.assertListEqual(self.server._probe_responses_by_session[session_id], [response])
        # Saving an empty works
        self.server.reset()
        self.server.save_state()
        self.assertEqual(self.server.load_state(), 200)
        self.assertFalse(self.server._probe_responses_by_session)  # No sessions
        self.server.add_probe_response(ProbeResponse(session_id=session_id, response=response))
        self.server.save_state()
        # Corrupt save is detected
        pathlib.Path(ServerState._RESPONSES_SAVE_NAME).unlink()  # corrupt the save
        self.assertEqual(self.server.load_state(), 500)  # error when trying to restore
        self.assertListEqual(self.server._probe_responses_by_session[session_id], [response])  # our state is unchanged
        shutil.rmtree(path=ServerState._SAVE_DIR, ignore_errors=True)  # clean up after ourselves
        return None
    
    def test_population_based_alignment_new_style(self) -> None:
        def is_float_or_int(obj: object) -> bool:
            return isinstance(obj, float) or isinstance(obj, int)
        
        session_id = self.server.generate_session_id()
        scenario_id: typing.Final[str] = "DryRunEval-MJ2-eval"
        target_id: typing.Final[str] = "ADEPT-DryRun-Moral judgement-Group-All"
        pop_target_id: typing.Final[str] = "ADEPT-DryRun-Moral judgement-Population-All"

        r1 = Response(scenario_id=scenario_id,
                    probe_id="Probe 2",
                    choice="Response 2B")
        r2 = Response(scenario_id=scenario_id,
                    probe_id="Probe 2B-1",
                    choice="Response 2B-1B")
        r3 = Response(scenario_id=scenario_id,
                    probe_id="Probe 3-B.2",
                    choice="Response 3-B.2-B-gauze-v")
        r4 = Response(scenario_id=scenario_id,
                    probe_id="Probe 3-B.2",
                    choice="Response 3-B.2-B-gauze-v")
        r5 = Response(scenario_id=scenario_id,
                    probe_id="Probe 3-B.2",
                    choice="Response 3-B.2-A-gauze-s")
        r6 = Response(scenario_id=scenario_id,
                    probe_id="Probe 3-B.2",
                    choice="Response 3-B.2-B-gauze-v")
        r7 = Response(scenario_id=scenario_id,
                    probe_id="Probe 3-B.2",
                    choice="Response 3-B.2-B-gauze-v")
        r8 = Response(scenario_id=scenario_id,
                    probe_id="Probe 3-B.2",
                    choice="Response 3-B.2-C")
        r9 = Response(scenario_id=scenario_id,
                    probe_id="Probe 4",
                    choice="Response 4-B")
        r9b = Response(scenario_id=scenario_id,
                    probe_id="Probe 4-B.1",
                    choice="Response 4-B.1-A")
        r10 = Response(scenario_id=scenario_id,
                    probe_id="Probe 5",
                    choice="Response 5-B")
        r11 = Response(scenario_id=scenario_id,
                    probe_id="Probe 5-B.1",
                    choice="Response 5-B.1-B")
        r12 = Response(scenario_id=scenario_id,
                    probe_id="Probe 6",
                    choice="Response 6-B")
        r13 = Response(scenario_id=scenario_id,
                    probe_id="Probe 7",
                    choice="Response 7-B")
        r14 = Response(scenario_id=scenario_id,
                    probe_id="Probe 8",
                    choice="Response 8-B")
        r15 = Response(scenario_id=scenario_id,
                    probe_id="Probe 9",
                    choice="Response 9-A")
        r16 = Response(scenario_id=scenario_id,
                    probe_id="Probe 9-A.1",
                    choice="Response 9-A.1-A")
        r17 = Response(scenario_id=scenario_id,
                    probe_id="Probe 10",
                    choice="Response 10-A")

        prb = ProbeResponseBatch(session_id=session_id,
                                responses=[r1, r2, r3, r4, r5, r6, r7, r8,
                                    r9, r9b, r10, r11, r12, r13, r14,
                                    r15, r16, r17])
        self.server.add_probe_responses(probe_responses=prb)

        # Kitware aligned MJ low
        session_id_2 = self.server.generate_session_id()

        r1 = Response(scenario_id=scenario_id,
                    probe_id="Probe 2",
                    choice="Response 2A")
        r2 = Response(scenario_id=scenario_id,
                    probe_id="Probe 2A-1",
                    choice="Response 2A-1A")
        r3 = Response(scenario_id=scenario_id,
                    probe_id="Probe 3-B.2",
                    choice="Response 3-B.2-A-gauze-s")
        r4 = Response(scenario_id=scenario_id,
                    probe_id="Probe 3-B.2",
                    choice="Response 3-B.2-A-gauze-s")
        r5 = Response(scenario_id=scenario_id,
                    probe_id="Probe 3-B.2",
                    choice="Response 3-B.2-A-gauze-s")
        r8 = Response(scenario_id=scenario_id,
                    probe_id="Probe 3-B.2",
                    choice="Response 3-B.2-C")
        r9 = Response(scenario_id=scenario_id,
                    probe_id="Probe 4",
                    choice="Response 4-B")
        r9b = Response(scenario_id=scenario_id,
                    probe_id="Probe 4-B.1",
                    choice="Response 4-B.1-B")
        r9bb = Response(scenario_id=scenario_id,
                    probe_id="Probe 4-B.1-B.1",
                    choice="Response 4-B.1-B.1-B")
        r10 = Response(scenario_id=scenario_id,
                    probe_id="Probe 5",
                    choice="Response 5-A")
        r11 = Response(scenario_id=scenario_id,
                    probe_id="Probe 5-A.1",
                    choice="Response 5-A.1-A")
        r12 = Response(scenario_id=scenario_id,
                    probe_id="Probe 6",
                    choice="Response 6-A")
        r13 = Response(scenario_id=scenario_id,
                    probe_id="Probe 7",
                    choice="Response 7-C")
        r14 = Response(scenario_id=scenario_id,
                    probe_id="Probe 8",
                    choice="Response 8-A")
        r15 = Response(scenario_id=scenario_id,
                    probe_id="Probe 9",
                    choice="Response 9-A")
        r16 = Response(scenario_id=scenario_id,
                    probe_id="Probe 9-A.1",
                    choice="Response 9-A.1-A")
        r17 = Response(scenario_id=scenario_id,
                    probe_id="Probe 10",
                    choice="Response 10-B")

        prb_2 = ProbeResponseBatch(session_id=session_id_2,
                                responses=[r1, r2, r3, r4, r5, r8, r9,
                                    r9b, r9bb, r10, r11, r12, r13, r14,
                                    r15, r16, r17])
        self.server.add_probe_responses(probe_responses=prb_2)

        ar = self.server.get_alignment_result_session(session_id=session_id,
                                                target_id=target_id,
                                                population=False)
        self.assertTrue(is_float_or_int(ar.score))
        self.assertGreaterEqual(ar.score, 0)
        self.assertLessEqual(ar.score, 1)

        ar_pop = self.server.get_alignment_result_session(session_id=session_id,
                                                    target_id=pop_target_id,
                                                    population=True)
        self.assertTrue(is_float_or_int(ar_pop.score))
        self.assertGreaterEqual(ar_pop.score, 0)
        self.assertLessEqual(ar_pop.score, 1)

        ar_2 = self.server.get_alignment_result_session(session_id=session_id_2,
                                                target_id=target_id,
                                                population=False)
        self.assertTrue(is_float_or_int(ar_2.score))
        self.assertGreaterEqual(ar_2.score, 0)
        self.assertLessEqual(ar_2.score, 1)

        ar_pop_2 = self.server.get_alignment_result_session(session_id=session_id_2,
                                                    target_id=pop_target_id,
                                                    population=True)
        self.assertTrue(is_float_or_int(ar_pop_2.score))
        self.assertGreaterEqual(ar_pop_2.score, 0)
        self.assertLessEqual(ar_pop_2.score, 1)

        # New alignment method
        alignment_btwn_profiles_s_s: AlignmentResults = self.server.get_alignment_sessions_population(session_id1_or_target_id=session_id, session_id2_or_target_id=session_id_2, target_pop_id=pop_target_id)
        self.assertTrue(isinstance(alignment_btwn_profiles_s_s.score, float) or isinstance(alignment_btwn_profiles_s_s.score, int))
        self.assertGreaterEqual(alignment_btwn_profiles_s_s.score, 0)
        self.assertLessEqual(alignment_btwn_profiles_s_s.score, 1)
        alignment_btwn_profiles_s_at = self.server.get_alignment_sessions_population(session_id1_or_target_id=session_id, session_id2_or_target_id="ADEPT-DryRun-Moral judgement-0.8", target_pop_id=pop_target_id)
        self.assertTrue(isinstance(alignment_btwn_profiles_s_at.score, float) or isinstance(alignment_btwn_profiles_s_at.score, int))
        self.assertGreaterEqual(alignment_btwn_profiles_s_at.score, 0)
        self.assertLessEqual(alignment_btwn_profiles_s_at.score, 1)
        alignment_btwn_profiles_at_at = self.server.get_alignment_sessions_population(session_id1_or_target_id="ADEPT-DryRun-Moral judgement-0.6", session_id2_or_target_id="ADEPT-DryRun-Moral judgement-0.8", target_pop_id=pop_target_id)
        self.assertTrue(isinstance(alignment_btwn_profiles_at_at.score, float) or isinstance(alignment_btwn_profiles_at_at.score, int))
        self.assertGreaterEqual(alignment_btwn_profiles_at_at.score, 0)
        self.assertLessEqual(alignment_btwn_profiles_at_at.score, 1)

        return None
    
    def test_get_ordered_alignment_and_scores(self) -> None:
        session_id = self.server.generate_session_id()
        scenario_id: typing.Final[str] = "DryRunEval-MJ2-eval"

        r1 = Response(scenario_id=scenario_id,
                    probe_id="Probe 2",
                    choice="Response 2B")
        r2 = Response(scenario_id=scenario_id,
                    probe_id="Probe 2B-1",
                    choice="Response 2B-1B")
        r3 = Response(scenario_id=scenario_id,
                    probe_id="Probe 3-B.2",
                    choice="Response 3-B.2-B-gauze-v")
        r4 = Response(scenario_id=scenario_id,
                    probe_id="Probe 3-B.2",
                    choice="Response 3-B.2-B-gauze-v")
        r5 = Response(scenario_id=scenario_id,
                    probe_id="Probe 3-B.2",
                    choice="Response 3-B.2-A-gauze-s")
        r6 = Response(scenario_id=scenario_id,
                    probe_id="Probe 3-B.2",
                    choice="Response 3-B.2-B-gauze-v")
        r7 = Response(scenario_id=scenario_id,
                    probe_id="Probe 3-B.2",
                    choice="Response 3-B.2-B-gauze-v")
        r8 = Response(scenario_id=scenario_id,
                    probe_id="Probe 3-B.2",
                    choice="Response 3-B.2-C")
        r9 = Response(scenario_id=scenario_id,
                    probe_id="Probe 4",
                    choice="Response 4-B")
        r9b = Response(scenario_id=scenario_id,
                    probe_id="Probe 4-B.1",
                    choice="Response 4-B.1-A")
        r10 = Response(scenario_id=scenario_id,
                    probe_id="Probe 5",
                    choice="Response 5-B")
        r11 = Response(scenario_id=scenario_id,
                    probe_id="Probe 5-B.1",
                    choice="Response 5-B.1-B")
        r12 = Response(scenario_id=scenario_id,
                    probe_id="Probe 6",
                    choice="Response 6-B")
        r13 = Response(scenario_id=scenario_id,
                    probe_id="Probe 7",
                    choice="Response 7-B")
        r14 = Response(scenario_id=scenario_id,
                    probe_id="Probe 8",
                    choice="Response 8-B")
        r15 = Response(scenario_id=scenario_id,
                    probe_id="Probe 9",
                    choice="Response 9-A")
        r16 = Response(scenario_id=scenario_id,
                    probe_id="Probe 9-A.1",
                    choice="Response 9-A.1-A")
        r17 = Response(scenario_id=scenario_id,
                    probe_id="Probe 10",
                    choice="Response 10-A")
        prb = ProbeResponseBatch(session_id=session_id,
                                responses=[r1, r2, r3, r4, r5, r6, r7, r8,
                                    r9, r9b, r10, r11, r12, r13, r14,
                                    r15, r16, r17])
        self.server.add_probe_responses(probe_responses=prb)
        alignments = self.server.get_ordered_alignment_and_scores(session_id=session_id, kdma_id=KDMAName.MORAL_JUDGEMENT)
        for alignment in alignments:
            self.assertEqual(len(alignment), 1)
            target_id, alignment_score = next(iter(alignment.items()))
            self.assertIn(target_id, self.server._known_target_ids)
            self.assertGreaterEqual(alignment_score, 0.0)
            self.assertLessEqual(alignment_score, 1.0)
        for idx in range(1, len(alignments)):
            prev_score = next(iter(alignments[idx-1].values()))
            curr_score = next(iter(alignments[idx].values()))
            self.assertGreaterEqual(prev_score, curr_score)
        print(alignments)
        return None

if __name__ == "__main__":
    unittest.main()
