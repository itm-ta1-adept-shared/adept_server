connexion[swagger-ui] >= 2.6.0; python_version>="3.6"
# 2.3 is the last version that supports python 3.4-3.5
connexion[swagger-ui] <= 2.3.0; python_version=="3.5" or python_version=="3.4"
# prevent breaking dependencies from advent of connexion>=3.0
connexion[swagger-ui] <= 2.14.2; python_version>"3.4"
# connexion requires werkzeug but connexion < 2.4.0 does not install werkzeug
# we must peg werkzeug versions below to fix connexion
# https://github.com/zalando/connexion/pull/1044
werkzeug == 0.16.1; python_version=="3.5" or python_version=="3.4"
swagger-ui-bundle >= 0.0.2
python_dateutil >= 2.6.0
setuptools >= 21.0.0
Flask == 2.1.1
# Above are added by the autogenerator
# Below are added for business logic
Flask-Cors>=3.0  # requested by one of the other teams
matplotlib
numpy
pandas
pyarrow  # not directly used, but suppress a warning from pandas
scikit-learn
scipy
