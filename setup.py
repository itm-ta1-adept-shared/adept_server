import sys
from setuptools import setup, find_packages

NAME = "openapi_server"
VERSION = "3.9.9"

# To install the library, run the following
#
# python setup.py install
#
# prerequisite: setuptools
# http://pypi.python.org/pypi/setuptools

REQUIRES = [
    "connexion>=2.0.2",
    "swagger-ui-bundle>=0.0.2",
    "python_dateutil>=2.6.0"
]

setup(
    name=NAME,
    version=VERSION,
    description="ITM TA1 API",
    author_email="",
    url="",
    keywords=["OpenAPI", "ITM TA1 API"],
    install_requires=REQUIRES,
    packages=find_packages(),
    package_data={'': ['openapi/openapi.yaml']},
    include_package_data=True,
    python_requires='>=3.10',
    entry_points={
        'console_scripts': ['openapi_server=openapi_server.__main__:main']},
    long_description="""\
    This is the specification of the TA1 API for In The Moment (ITM) for the ADEPT server. This is created in support of the Phase 1 Eval. It is used to characterize and determine alignment of a decision maker&#39;s choices.
    """
)
